#!/usr/bin/python
# Modul sys wird importiert:
import sys
import requests
import json
import time
from bs4 import BeautifulSoup
from bs4 import SoupStrainer

status = 'online' # local or online

if status == 'local':
    oxids = requests.get('http://bodynova.local/modules/bfox/multichannel/scripts/getArticlesAsJsonWithAttribute.php',timeout=10)
    url = 'http://bodynova.local/index.php?cl=details&anid='
else:
	oxids = requests.get('https://bodynova.de/modules/bfox/multichannel/scripts/getArticlesAsJsonWithAttribute.php',timeout=10)
	url = 'https://bodynova.de/index.php?cl=details&anid='

data = json.loads(oxids.content)
oxids.close()

# print current date and time
print('beginn')
beginn = time.strftime("%d.%m.%Y %H:%M:%S")
print(beginn)

nodesc = []
lang = ['de','en','fr']

for l in lang:
	print('Sprache:')
	print(l)
	for i in data:
	    oxid = ''.join(i)
	    print(oxid)

	    try:
	        only = SoupStrainer(id="description")

	        if l == 'de':
	            response = requests.get(url+oxid+'&lang=0', timeout=10)
	        elif l == 'en':
	            response = requests.get(url+oxid+'&lang=1', timeout=10)
	        elif l == 'fr':
	            response = requests.get(url+oxid+'&lang=2', timeout=10)

	        soup = BeautifulSoup(response.content, 'html.parser', parse_only=only)
	        response.close()

	        [x.extract() for x in soup.findAll('iframe')]
	        [x.extract() for x in soup.findAll("div", {"class":"noEbay"})]
	        soup.find('div', id='description').unwrap()

	        f = open("../cache/"+l+"/"+oxid+'.html', "w")
	        f.write(soup.prettify().encode('utf-8'))
	        f.close()
	    except:
	        print(oxid+" hat keine Detailbeschreibung")
	        nodesc.append(oxid)

print('begonnen: ')
print(beginn)
print('ende')
ende = time.strftime("%d.%m.%Y %H:%M:%S")
print(ende)
print('keine Detailbeschreibung: ')
print(nodesc)
