<?php

use OxidEsales\Eshop\Core\DatabaseProvider;

include '../../../../bootstrap.php';

header('Content-type: application/json; charset=utf-8');

$debug = 0;
/*
 <div class="noEbay">   </div>
 * */
if($debug === 1){
    /*
    Artikel mit nicht für ebay geeignetem Inhalt.
    */
    $articles = array(
        '47c190f6255ce23e86792590445605c7',
        'b1bd8ce3a9fd7c34601fb1121247e80a'
    );
    $contents = array(
        'maharajaback',
    );
    die(json_encode($articles));
}

$odb = DatabaseProvider::getDb();
$query = "SELECT a.OXID from oxarticles a LEFT JOIN oxobject2attribute as o ON( a.OXID = o.OXOBJECTID) WHERE o.OXATTRID = 'marketplace_3' AND o.OXVALUE = 1 AND a.OXACTIVE = 1";
$articles = $odb->select($query)->fetchAll();
echo json_encode($articles);
