<?php

use OxidEsales\Eshop\Core\DatabaseProvider;

include '../../../../bootstrap.php';

$odb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);

$query = "SELECT * from oxartextends WHERE OXLONGDESC LIKE '%<font%' OR OXLONGDESC_1 LIKE '%<font%' OR OXLONGDESC_2 LIKE '%<font%'";
$articles = $odb->select($query)->fetchAll();


echo count($articles).' Artikel gefunden.';
/*
echo '<code>';
echo $articles[0]['OXLONGDESC'];


echo strip_only($articles[0]['OXLONGDESC'],'font');

echo '</code>';
*/
$savequery = 'Update oxartextends set OXLONGDESC = ?, OXLONGDESC_1 = ?, OXLONGDESC_2 = ? WHERE OXID = ?';

foreach($articles as $article){
    $odb->execute($savequery,array(strip_only($article['OXLONGDESC'], 'font'), strip_only($article['OXLONGDESC_1'], 'font'), strip_only($article['OXLONGDESC_2'], 'font'),$article['OXID']));
}

echo 'Bereinigt.';



function strip_only($str, $tags, $stripContent = false)
{
    $content = '';
    if (!is_array($tags)) {
        $tags = (strpos($str, '>') !== false ? explode('>', str_replace('<', '', $tags)) : array($tags));
        if (end($tags) == '') array_pop($tags);
    }
    foreach ($tags as $tag) {
        if ($stripContent)
            $content = '(.+</' . $tag . '[^>]*>|)';
        $str = preg_replace('#</?' . $tag . '[^>]*>' . $content . '#is', '', $str);
    }
    return $str;
}
/*
$str = '<font color="red">red</font> text';
$tags = 'font';
$a = strip_only($str, $tags); // red text
$b = strip_only($str, $tags, true); // text
*/