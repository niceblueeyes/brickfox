<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 13.09.18
 * Time: 09:20
 *
 * autoconnect.php - automated calls using intervals
 *
 */

namespace bfox\multichannel\scripts;

use bfox\multichannel\classes\util as Utils;

    $dir = dirname(__FILE__);
    $pathArray = explode("modules", $dir);
    $bootstrapPath = array_shift($pathArray);
    require_once $bootstrapPath . 'bootstrap.php';

    /**
     * Parameter sent by Querystring with requested action to perform
     */
    $action			= getRequestParameter('action');

    /**
     * Array of valid actions to call
     */
    $validActions	= array(
        'showVersion','exportProductsAuto'
    );

    start($action, $validActions);




function getRequestParameter($parameterName){
    if(isset($_GET[$parameterName])){
        return $_GET[$parameterName];
    }
    return "";
}




/**
 * @param string $action Taken from query string request
 * @param array $validActions
 */
function start($action, $validActions){

    $testCurlMessage = "Brickfox plugin for Oxid e-sales 6.0";

    if(true === isset($action) && '' != $action && true === in_array($action, $validActions))
    {
        switch($action)
        {
            case 'showVersion':
                echo $testCurlMessage;
                break;


            case 'exportProductsAuto':

                $htuser = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : "";
                $htpass = isset($_SERVER['PHP_AUTH_PW'])   ? $_SERVER['PHP_AUTH_PW'] : "";

                ignore_user_abort(true);
                #set_time_limit(60 * 60 * 6);

                $lang	= getRequestParameter('lang');
                $start	= getRequestParameter('startPage');

                $pageURL = 'http';

                if ($_SERVER["HTTPS"] == "on") {
                    $pageURL .= "s";
                }
                $pageURL .= "://";
                $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["SCRIPT_NAME"];

                $testURL = $pageURL;
                $testURL = $testURL . '?action=showVersion';
                $test = getCurlContent($testURL, $htuser, $htpass);

                if ($test !== $testCurlMessage)
                {
                    echo "<p>" . Utils\ConfigurationKeys::AUTOCONNECT_KEY_ERROR_TEST .  "</p>";
                    echo $test;
                    exit;
                }

                $pageURL = str_ireplace('autoconnect.php', 'connect.php', $pageURL);
                $start = strlen($start) > 0 ? $start : 1;
                $limit = 250;
                $pages = 4;
                $data = Utils\ConfigurationKeys::AUTOCONNECT_KEY_MORE_FILES;
                $language = "";

                if (strlen($lang) > 0){
                    $language = '&lang=' . $lang;
                }

                while ($data === Utils\ConfigurationKeys::AUTOCONNECT_KEY_MORE_FILES)
                {
                    $fullUrl = $pageURL . '?action=exportProducts&startPage=' . $start . '&pages=' . $pages . '&limit=' . $limit . $language;

                    $data = getCurlContent($fullUrl, $htuser, $htpass);
                    $start = $start + $pages;

                    echo "<p>" . $data . "</p>";
                    echo "<p>" . $fullUrl . "</p>";
                }

                break;
        }

    }
}

function getCurlContent($url, $htuser, $htpass)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1200);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    if (strlen($htuser) > 0)
    {
        curl_setopt($ch, CURLOPT_USERPWD, "$htuser:$htpass");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    }

    $data = callWithRetry($ch);

    curl_close($ch);

    return $data;
}


function callWithRetry($ch)
{
    $data = curl_exec($ch);
    $count = 0;

    while (!$data && $count < 5)
    {
        Utils\LogManager::getInstance()->debug("cUrl empty! Retry called. Attempt #" . ($count + 1) . ".");
        echo "<p>" . Utils\ConfigurationKeys::AUTOCONNECT_KEY_ERROR_CURL .  "</p>";
        echo "<p>" . curl_error ( $ch ) .  "</p>";
        $count++;
        echo "Retry in 15 seconds. Attempt #" . $count . "<br>";
        sleep(15);
        $data = curl_exec($ch);
    }
    return $data;
}



