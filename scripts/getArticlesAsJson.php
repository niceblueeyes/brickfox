<?php

use OxidEsales\Eshop\Core\DatabaseProvider;

include '../../../../bootstrap.php';

$odb = DatabaseProvider::getDb();

$query = "SELECT OXID from oxarticles WHERE OXACTIVE = 1 AND OXPARENTID=''";
$articles = $odb->select($query)->fetchAll();

echo json_encode($articles);