<?php
/**
 * connect.php - the entry point
 *
 */

namespace bfox\multichannel\scripts;

use bfox\multichannel\classes\controller as Controllers;
use bfox\multichannel\classes\util as Utils;
use bfox\multichannel\classes\exception as Exceptions;


callOxidBootstrap();

checkOutputDebug();

/**
 * Parameter sent by Querystring with requested action to perform
 */
$action = getRequestParameter('action');

/**
 * Array of valid actions to call
 */
$validActions = array(
    'importOrders',
    'exportOrdersStatus',
    'showVersion',
    'exportProducts',
    'exportCategories',
    'exportManufacturers',
    'exportProductsUpdates',
    'customExportProducts',
    'exportNewProducts',
    'exportProductsDeletes'
);

start($action, $validActions);


function getRequestParameter($parameterName)
{
    if (isset($_GET[$parameterName])) {
        return $_GET[$parameterName];
    }
}

/**
 * @param string $action Taken from query string request
 * @param array $validActions
 */
function start($action, $validActions)
{
    if (true === isset($action) && '' != $action && true === in_array($action, $validActions)) {
        $importExportAction = '';
        switch ($action) {
            case 'importOrders':
                $importExportAction = 'importBrickfoxOrders';
                break;

            case 'exportProducts':
                $importExportAction = 'collectOxidArticles';
                break;

            case 'exportNewProducts':
                $importExportAction = 'collectOxidArticlesCreatedSince';
                break;

            case 'exportProductsUpdates':
                $importExportAction = 'collectOxidArticlesStockAndPrices';
                break;

            case 'customExportProducts':
                $importExportAction = 'collectOxidArticlesCustomized';
                break;

            case 'exportCategories':
                $importExportAction = 'collectOxidCategories';
                break;

            case 'exportManufacturers':
                $importExportAction = 'collectOxidManufacturers';
                break;

            case 'exportOrdersStatus':
                $importExportAction = 'collectOxidOrdersStatus';
                break;

            case 'exportProductsDeletes':
                $importExportAction = 'collectOxidProductsDeletes';
                break;

            case 'showVersion':
                outputVersionInfo();
                break;
        }

        callRequestedAction($importExportAction);
    }
}


/**
 * Calls the Oxid bootstrap file to load all the Oxid framework and its contents
 */
function callOxidBootstrap()
{
    $dir           = dirname($_SERVER['SCRIPT_FILENAME']);
    $pathArray     = explode("modules", $dir);
    $bootstrapPath = array_shift($pathArray);

    if (substr($bootstrapPath, -1) !== '/') {
        $bootstrapPath .= '/';
    }

    require_once $bootstrapPath . 'bootstrap.php';

}

function changeMemoryLimits()
{
    $memoryLimit = '2048M';
    ini_set('memory_limit', $memoryLimit);
    Utils\LogManager::getInstance()->debug("Try Memory Limit change to " . $memoryLimit);
}

function changeTimeLimits()
{
    #set_time_limit(60 * 60 * 6);
}


/**
 *  Use the debug_mode for Exceptions and Errors description.
 *  Here we change how to handle exceptions as for PHP 7 they are Errors instances.
 */
function checkOutputDebug()
{
    //$debug_mode = getRequestParameter('debug_mode');
    $debug_mode = 'j';


    if (true === isset($debug_mode) && $debug_mode === 'j') {
        Utils\SystemManager::getInstance()->setDebugMode(true);


        set_exception_handler(
            [
                new Exceptions\OxidExceptionHandler(),
                'handleUncaughtException'
            ]
        );

        set_error_handler(
            [
                new Exceptions\OxidExceptionHandler(),
                'handleUncaughtError'
            ]
        );
    }
}


/**
 * @param $importExportAction string Name of the method that should be called by the controller
 */
function callRequestedAction($importExportAction)
{
    if ('' != $importExportAction) {
        $controller = new Controllers\ImportExportController;
        $controller->{$importExportAction}();
    }
}


/**
 * Return information about the Oxid version Installed and some of its features
 */
function outputVersionInfo()
{
    echo "<h2>Brickfox multichannel plugin for Oxid e-sales 6.0</h2>";
    echo "<h3>Oxid details:</h3>";

    $config = new Utils\OxidCompatibility;

    echo "Version: " . $config->getVersionDescription();
    echo "<br>";
    echo "Multiple Shops support: ";
    echo $config->getSupportShops() ? "true" : "false";
    echo "<br>";
    echo "Singleton (getInstance method) support: ";
    echo $config->getSupportGetInstance() ? "true" : "false";
    echo "<br>";
    echo "<h3>Server max_execution_time: " . ini_get('max_execution_time') . "</h3>";
    echo "<h3>Server max_input_time: " . ini_get('max_input_time') . "</h3>";
    echo "<h3>Server memory_limit: " . ini_get('memory_limit') . "</h3>";
    echo "<h3>Server safe_mode: " . var_export(ini_get('safe_mode'), true) . "</h3>";

    echo "<hr>";
    #phpinfo();
}
