<?php
/**
 * Module configuration
 *
 * Pay attention:
 * settings name values should not have more than 32 characters.
 */

// Metadata version
$sMetadataVersion = '2.1';

// Module information
$aModule = array(
    'id'          => 'multichannel',
    'title'       => 'Brickfox Multichannel (PIM)',
    'description'  => array(
        'de' => 'Brickfox Multichannel e-commerce. Module for Oxid 6.',
        'en' => 'Brickfox Multichannel e-commerce. Module for Oxid 6.',
    ),
    'thumbnail'   => 'images/logo.jpg',
    'version'     => '6.1.0',
    'author'      => 'brickfox GmbH',
    'email'       => 'support@brickfox.de',
    'url'         => 'http://www.brickfox.de',

    'extend'      => array(

        \OxidEsales\Eshop\Application\Model\AmountPriceList::class  => \bfox\multichannel\wrapper\Application\Model\OxidAmountPriceList::class,
        \OxidEsales\Eshop\Application\Model\Article::class          => \bfox\multichannel\wrapper\Application\Model\OxidArticle::class,
        \OxidEsales\Eshop\Application\Model\ArticleList::class      => \bfox\multichannel\wrapper\Application\Model\OxidArticleList::class,
        \OxidEsales\Eshop\Application\Model\Attribute::class        => \bfox\multichannel\wrapper\Application\Model\OxidAttribute::class,
        \OxidEsales\Eshop\Application\Model\AttributeList::class    => \bfox\multichannel\wrapper\Application\Model\OxidAttributeList::class,
        \OxidEsales\Eshop\Application\Model\Category::class         => \bfox\multichannel\wrapper\Application\Model\OxidCategory::class,
        \OxidEsales\Eshop\Application\Model\CategoryList::class     => \bfox\multichannel\wrapper\Application\Model\OxidCategoryList::class,
        \OxidEsales\Eshop\Application\Model\Manufacturer::class     => \bfox\multichannel\wrapper\Application\Model\OxidManufacturer::class,
        \OxidEsales\Eshop\Application\Model\ManufacturerList::class => \bfox\multichannel\wrapper\Application\Model\OxidManufacturerList::class,
        \OxidEsales\Eshop\Application\Model\Order::class            => \bfox\multichannel\wrapper\Application\Model\OxidOrder::class,
        \OxidEsales\Eshop\Application\Model\OrderArticle::class     => \bfox\multichannel\wrapper\Application\Model\OxidOrderArticle::class,
        \OxidEsales\Eshop\Application\Model\Shop::class             => \bfox\multichannel\wrapper\Application\Model\OxidShop::class,
        \OxidEsales\Eshop\Application\Model\ShopList::class         => \bfox\multichannel\wrapper\Application\Model\OxidShopList::class,
        \OxidEsales\Eshop\Application\Model\User::class             => \bfox\multichannel\wrapper\Application\Model\OxidUser::class,
        \OxidEsales\Eshop\Application\Model\Vendor::class           => \bfox\multichannel\wrapper\Application\Model\OxidVendor::class,
        \OxidEsales\Eshop\Application\Model\VendorList::class       => \bfox\multichannel\wrapper\Application\Model\OxidVendorList::class,

        \OxidEsales\Eshop\Core\Config::class                        => \bfox\multichannel\wrapper\Core\OxidConfig::class,
        \OxidEsales\Eshop\Core\Registry::class                      => \bfox\multichannel\wrapper\Core\OxidRegistry::class,
        \OxidEsales\Eshop\Core\Model\BaseModel::class               => \bfox\multichannel\wrapper\Core\Model\OxidBaseModel::class,
        \OxidEsales\Eshop\Core\Model\ListModel::class               => \bfox\multichannel\wrapper\Core\Model\OxidListModel::class,
        \OxidEsales\Eshop\Core\Model\MultiLanguageModel::class      => \bfox\multichannel\wrapper\Core\Model\OxidMultiLanguageModel::class,

        \OxidEsales\Eshop\Core\Exception\ExceptionHandler::class    => \bfox\multichannel\classes\exception\OxidExceptionHandler::class,
    ),


    'events' 	=> array(
	),
	'templates' => array(
	),
    'blocks' => array(
    ),

	'settings'		=> array(
        array(
            'group' => 'bfox_multichannel_connection',
            'name' => 'strbrickfoxFtpHost',
            'type' => 'str',
            'value' => '',
            'position' => 1
        ),
        array(
            'group' => 'bfox_multichannel_connection',
            'name' => 'strbrickfoxFtpUsername',
            'type' => 'str',
            'value' => '',
            'position' => 2
        ),
        array(
            'group' => 'bfox_multichannel_connection',
            'name' => 'strbrickfoxFtpPassword',
            'type' => 'str',
            'value' => '',
            'position' => 3
        ),
        array(
            'group' => 'bfox_multichannel_connection',
            'name' => 'intbrickfoxFtpPort',
            'type' => 'str',
            'value' => '',
            'position' => 4
        ),
        array(
            'group' => 'bfox_multichannel_connection',
            'name' => 'strbrickfoxFtpFolder',
            'type' => 'str',
            'value' => 'incoming/',
            'position' => 5
        ),
        array(
            'group' => 'bfox_multichannel_products',
            'name' => 'aTaxClassMapping',
            'type' => 'aarr',
            'value' => array(
                '19' => 'standart',
                '7'  => 'reduced'
            ),
            'position' => 1
        ),
        array(
            'group' => 'bfox_multichannel_products',
            'name' => 'aArticleAdditionalWhere',
            'type' => 'aarr',
            'value' => array(
                "OXPARENTID" => " = '' " ,
            ),
            'position' => 2
        ),
        array(
            'group' => 'bfox_multichannel_products',
            'name' => 'slctImageType',
            'type' => 'str',
            /*
            'type' => 'select',
            'constrains' => 'Icons|Pics|ZoomPics|Master',
            */
            'value' => 'ZoomPics',
            'position' => 3
        ),
        array(
            'group' => 'bfox_multichannel_products',
            'name' => 'strLastProductsUpdate',
            'type' => 'str',
            'value' => '',
            'position' => 4
        ),
        array(
            'group' => 'bfox_multichannel_customexport',
            'name' => 'aGroupsToExport',
            'type' => 'arr',
            'value' => array(
                "Attributes", "Categories", "Images", "Descriptions", "Currencies", "Options"
            ),
            'position' => 1
        ),
        array(
            'group' => 'bfox_multichannel_customexport',
            'name' => 'aLastUpdateCheck',
            'type' => 'arr',
            'value' => array(
                "oxartextends__oxid", "oxaccessoire2article__oxarticlenid", "oxobject2article__oxarticlenid",
                "oxobject2attribute__oxobjectid", "oxobject2category__oxobjectid", "oxobject2seodata__oxobjectid", "oxprice2article__oxartid"
            ),
            'position' => 2
        ),
        array(
            'group' => 'bfox_multichannel_customexport',
            'name' => 'strCustomExportLastChange',
            'type' => 'str',
            'value' => '',
            'position' => 3
        ),
        array(
            'group' => 'bfox_multichannel_thirdpartystock',
            'name' => 'blnThirdPartyMap',
            'type' => 'bool',
            'value' => 'false',
            'position' => 1
        ),
        array(
            'group' => 'bfox_multichannel_thirdpartystock',
            'name' => 'strThirdPartyMapFrom',
            'type' => 'str',
            'value' => '',
            'position' => 2
        ),
        array(
            'group' => 'bfox_multichannel_thirdpartystock',
            'name' => 'slcThirdPartyMapAs',
            'type' => 'str',
            /*
            'type' => 'select',
            'constrains' => 'blnText|blnNumber|fieldValue',
            */
            'value' => 'fieldValue',
            'position' => 3
        ),
        array(
            'group' => 'bfox_multichannel_attributes',
            'name' => 'arrAttributesArticlesTable',
            'type' => 'aarr',
            'value' => '',
            'position' => 1
        ),
        array(
            'group' => 'bfox_multichannel_orders',
            'name' => 'slcOrderNumberType',
            /*
            'type' => 'select',
            */
            'type' => 'str',
            'value' => 'bfId',
            'position' => 1
            /*,
            'constrains' => 'externId|bfId|oxId'
            */
        ),
         array(
            'group' => 'bfox_multichannel_orders',
            'name' => 'sOrderfolderNew',
            'type' => 'str',
            'value' => 'ORDERFOLDER_NEW',
            'position' => 2
        ),
        array(
            'group' => 'bfox_multichannel_orders',
            'name' => 'sOrderfolderShipped',
            'type' => 'str',
            'value' => 'ORDERFOLDER_FINISHED',
            'position' => 3
        ),
        array(
            'group' => 'bfox_multichannel_orders',
            'name' => 'sOrderfolderFba',
            'type' => 'str',
            'value' => 'ORDERFOLDER_FINISHED',
            'position' => 4
        ),
        array(
            'group' => 'bfox_multichannel_orders',
            'name' => 'slcOrderStatusUpdateType',
            'type' => 'str',
            /*
            'type' => 'select',
            */
            'value' => 'excludeLines',
            'position' => 5,
            /*
            'constrains' => 'excludeLines|includeLines'
            */
        ),
        array(
            'group'     => 'bfox_multichannel_orders',
            'name'      => 'aaOrdersStatusFolderMapping',
            'type'      => 'aarr',
            'value'     => [
                'ORDERFOLDER_NEW'           => 'Pending',
                'ORDERFOLDER_FINISHED'      => 'Shipped',
                'ORDERFOLDER_CANCELLED'     => 'Cancelled',
                'ORDERFOLDER_RETURNED'      => 'Returned',
                'ORDERFOLDER_PARTRETURNED'  => 'PartlyReturned',
            ],
            'position'  => 6,
        ),
        array(
            'group'     => 'bfox_multichannel_orders',
            'name'      => 'sOrderLastRun',
            'type'      => 'str',
            'value'     => '',
            'position'  => 7,
        ),
        array(
            'group'     => 'bfox_multichannel_orders',
            'name'      => 'sCustomerOrderNumberColumnName',
            'type'      => 'str',
            'value'     => '',
            'position'  => 8,
        ),
        array(
            'group'    => 'shops',
            'name'     => 'aBfShopIdToShopIdMapping',
            'type'     => 'aarr',
            'value'    => '',
            'position' => 1,
        ),
        array(
            'group'    => 'shops',
            'name'     => 'blnOverrideShopsLoopDefault',
            'type'     => 'bool',
            'value'    => 'false',
            'position' => 2,
        ),
        array(
            'group'    => 'shops',
            'name'     => 'aShopsToLoop',
            'type'     => 'arr',
            'value'    => '',
            'position' => 3,
        ),
        array(
            'group'    => 'bfox_multichannel_logging',
            'name'     => 'sLogLevel',
            'type'     => 'str',
            'value'    => 'DEBUG,WARN,ERROR',
            'position' => 1,
        ),
        array(
            'group'    => 'bfox_multichannel_logging',
            'name'     => 'sLogFilename',
            'type'     => 'str',
            'value'    => 'multichannel',
            'position' => 2,
        ),
        array(
            'group'    => 'bfox_multichannel_logging',
            'name'     => 'sLogCleanupInDays',
            'type'     => 'str',
            'value'    => '24',
            'position' => 3,
        ),
        array(
            'group'    => 'bfox_multichannel_storage',
            'name'     => 'strStorageDirectory',
            'type'     => 'str',
            'value'    => '/modules/bfox/multichannel/exchange',
            'position' => 1,
        ),
        array(
            'group'    => 'bfox_multichannel_storage',
            'name'     => 'boolStorageShopDirectory',
            'type'     => 'bool',
            'value'    => 'true',
            'position' => 2,
        ),
        array(
            'group'    => 'bfox_multichannel_storage',
            'name'     => 'sStorageFilenameOrders',
            'type'     => 'str',
            'value'    => 'Orders',
            'position' => 3,
        ),
        array(
            'group'    => 'bfox_multichannel_storage',
            'name'     => 'sStorageFilenameOrdersStatus',
            'type'     => 'str',
            'value'    => 'Orderstatus',
            'position' => 4,
        ),
        array(
            'group'    => 'bfox_multichannel_storage',
            'name'     => 'sStorageFilenameManufacturers',
            'type'     => 'str',
            'value'    => 'Manufacturers',
            'position' => 5,
        ),
        array(
            'group'    => 'bfox_multichannel_storage',
            'name'     => 'sStorageFilenameCategories',
            'type'     => 'str',
            'value'    => 'Categories',
            'position' => 6,
        ),
        array(
            'group'    => 'bfox_multichannel_storage',
            'name'     => 'sStorageFilenameProducts',
            'type'     => 'str',
            'value'    => 'Products',
            'position' => 7,
        ),
        array(
            'group'    => 'bfox_multichannel_storage',
            'name'     => 'sStorageFilenameProductsUpdate',
            'type'     => 'str',
            'value'    => 'ProductUpdate',
            'position' => 8,
        ),
        array(
            'group'    => 'bfox_multichannel_storage',
            'name'     => 'sStorageFilenameProductDelete',
            'type'     => 'str',
            'value'    => 'ProductDelete',
            'position' => 9,
        ),
        array(
            'group'    => 'bfox_multichannel_scriptLogger',
            'name'     => 'sScriptLoggerCleanupDBHanging',
            'type'     => 'str',
            'value'    => '18000',
            'position' => 1,
        ),
        array(
            'group'    => 'bfox_multichannel_scriptLogger',
            'name'     => 'sScriptLoggerCleanupDBRemoval',
            'type'     => 'str',
            'value'    => '100',
            'position' => 2,
        ),


	),
);
