CREATE TABLE IF NOT EXISTS brickfox_import (
  id varchar(32) NOT NULL,
  oxorderid char(32) default NULL COLLATE 'latin1_general_ci',
  externorderid varchar(40) default NULL,
  import_importdate datetime  DEFAULT NULL,
  orderid int(11) default NULL,
  shopid varchar(255) NOT NULL,
  import_oxfolder varchar(40) NOT NULL default '0',
  import_senddate datetime DEFAULT NULL,
  import_updatedate datetime DEFAULT NULL,
  saleschannelid int(11) NOT NULL default '0',
  PRIMARY KEY  (id),
  UNIQUE KEY orderid (orderid,shopid),
  KEY oxorderid (oxorderid),
  KEY shopid (shopid),
  KEY import_oxfolder (import_oxfolder)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS brickfox_import_articles (
  id varchar(32) NOT NULL,
  import_id varchar(32) NOT NULL,
  orderline_id varchar(32) NOT NULL,
  extern_product_id varchar(32) NOT NULL,
  import_article_oxfolder varchar(40) NOT NULL,
  import_article_senddate datetime DEFAULT NULL,
  import_article_updatedate datetime DEFAULT NULL,
  import_article_importdate datetime DEFAULT NULL,
  oxorderid varchar(32) NOT NULL COLLATE 'latin1_general_ci',
  quantity_ordered int(11) NOT NULL,
  PRIMARY KEY  (id),
  KEY import_id (import_id)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS brickfox_articles_exports (
  id varchar(32) NOT NULL,
  oxid_id varchar(32) NOT NULL,
  is_deleted int(1) NOT NULL DEFAULT 0,
  last_export datetime DEFAULT NULL,
  export_hash varchar(32) DEFAULT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY oxid_id (oxid_id)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bfscriptlogger` (
	`OXID` char(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
	`OXSHOPID` int(11) NOT NULL DEFAULT '0',
	`SCRIPTNAME` VARCHAR(50) NOT NULL,
	`SCRIPTSTATUS` VARCHAR(50) NOT NULL,
	`DATESTART` datetime DEFAULT NULL,
	`DATEEND` datetime DEFAULT NULL,
	PRIMARY KEY(`OXID`),
	KEY `KEY_SCRIPTSTATUS` (`SCRIPTSTATUS`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
