<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 24.04.19
 * Time: 10:21
 */

namespace bfox\multichannel\classes\model;

use bfox\multichannel\classes\util as Utils;


class OrderStatusListModel extends ListModel
{
    const TYPE_ONLY_ORDER_CHANGES    = 'excludeLines';
    const TYPE_ORDER_ARTICLE_CHANGES = 'includeLines';



    public function __construct($oOrder = null)
    {
        if ($oOrder === null)
        {
            $oOrder = oxNew(OrderModel::class);
        }
        $this->setBaseObject($oOrder);

    }


    public function getOrderStatusList()
    {
        $type = Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_ORDERS_ORDER_STATUS_TYPE);

        $oxShopId = Utils\OxidRegistry::getActiveShopId();

        if ($type === self::TYPE_ONLY_ORDER_CHANGES)
        {
            return $this->getOrderListOnlyOrderChanges($oxShopId);
        }
        else
        {
            return $this->getOrderListIncludeOrderLinesChanges($oxShopId);
        }
    }


    protected function getOrderListOnlyOrderChanges($oxShopId)
    {
        $sSelect = "SELECT *, oo.oxsenddate AS rawSendDate FROM brickfox_import AS bi
                    LEFT JOIN oxorder AS oo ON bi.oxorderid = oo.oxid 
                    WHERE  bi.import_oxfolder != oo.oxfolder
                        AND bi.shopid = '" . $oxShopId . "'";

        return $this->selectString($sSelect);
    }


    protected function getOrderListIncludeOrderLinesChanges($oxShopId)
    {
        $sSelect = "SELECT * FROM oxorder 
                    LEFT JOIN brickfox_import ON brickfox_import.oxorderid = oxorder.oxid
                    WHERE oxid IN (
                        SELECT oo.OXID FROM brickfox_import AS bi
                        LEFT JOIN oxorder AS oo ON bi.oxorderid = oo.oxid 
                        WHERE  bi.import_oxfolder <> oo.oxfolder AND bi.shopid = '" . $oxShopId . "'
                        UNION 
                        SELECT ooa.oxorderid FROM brickfox_import_articles AS bia
                        LEFT JOIN oxorderarticles AS ooa ON bia.import_id = ooa.oxid 
                        WHERE  bia.import_article_oxfolder <> ooa.oxfolder 
                    )";

        return $this->selectString($sSelect);
    }
}