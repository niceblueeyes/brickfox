<?php
/**
 * ProductDeleteListModel
 * This file is part of brickfox.
 *
 * @author brickfox GmbH <support@brickfox.de>
 * @copyright Copyright (c) 2011-2019 brickfox GmbH http://www.brickfox.de
 */

namespace bfox\multichannel\classes\model;

class ProductDeleteListModel extends ListModel
{
    /**
     */
    public function __construct()
    {
        $this->setBaseObject(oxNew(ProductDeleteModel::class));
    }

    /**
     * @return mixed
     */
    public function getProductDeleteList()
    {
        $sSelect = "SELECT bae.oxid_id AS productExternId FROM brickfox_articles_exports bae" .
                   " LEFT JOIN oxarticles oa ON oa.OXID = bae.oxid_id" .
                   " WHERE oa.OXID IS NULL AND bae.is_deleted = 0";

        return $this->selectString($sSelect);
    }
}