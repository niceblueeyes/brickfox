<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 26.04.19
 * Time: 13:13
 */

namespace bfox\multichannel\classes\model;


class OrderArticleListModel extends ListModel
{

    public function getOrderArticlesFull($oxOrderId, $blExcludeCanceled = false)
    {
        $sSelect = "SELECT  oo.OXID AS orderoxid, oo.OXFOLDER AS orderoxfolder, oo.OXSTORNO as orderoxstorno,
                          ooa.OXID, ooa.OXARTID, ooa.OXAMOUNT, ooa.OXARTNUM, ooa.OXDELIVERY, ooa.OXTIMESTAMP, ooa.OXFOLDER, ooa.OXSTORNO,
                          bia.orderline_id, bia.import_id, bia.id, bia.quantity_ordered
                        FROM brickfox_import_articles AS bia
                        INNER JOIN oxorderarticles AS ooa ON bia.import_id = ooa.OXID 
                        INNER JOIN oxorder AS oo ON ooa.OXORDERID = oo.OXID
                        WHERE ooa.oxorderid = '" . $oxOrderId . "'" .
                        ($blExcludeCanceled ? " AND ooa.oxstorno != 1 " : " ") ;

        $this->setBaseObject(oxNew(OrderArticleModel::class));
        $this->selectString($sSelect);

    }


}