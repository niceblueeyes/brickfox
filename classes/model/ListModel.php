<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 15.08.18
 * Time: 09:56
 */

namespace bfox\multichannel\classes\model;

use bfox\multichannel\wrapper\Core\Model as OxidCoreModels;

class ListModel extends OxidCoreModels\OxidListModel
{
    public function getListAll()
    {
        parent::getListAll();
    }

}