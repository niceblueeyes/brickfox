<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 15.08.18
 * Time: 10:31
 */

namespace bfox\multichannel\classes\model;

use bfox\multichannel\classes\util as Utils;
use bfox\multichannel\wrapper\Application\Model as OxidApplicationModels;


class ProductModel extends OxidApplicationModels\OxidArticle
{

    public function resetLanguageFields()
    {
        $this->_oLongDesc = null;
    }

    public function getAmountPriceList()
    {
        return $this->_oAmountPriceList;
    }


    public function getAdminVariants($sLanguage = null)
    {
        $oBase = oxNew(ProductModel::class);

        $oVariants = oxNew(ProductListModel::class);
        $oVariants->setBaseObject($oBase);

        if (($sId = $this->getId())) {
            $oBaseObj = $oVariants->getBaseObject();

            if (is_null($sLanguage)) {
                $oBaseObj->setLanguage(Utils\OxidRegistry::getBaseLanguage());
            } else {
                $oBaseObj->setLanguage($sLanguage);
            }

            $sSql = "select * from " . $oBaseObj->getViewName() . " where oxparentid = '{$sId}' order by oxsort ";
            $oVariants->selectString($sSql);

            //if we have variants then depending on config option the parent may be non buyable
            if (!$this->getConfig()->getConfigParam('blVariantParentBuyable') && ($oVariants->count() > 0)) {
                //$this->blNotBuyable = true;
                $this->_blNotBuyableParent = true;
            }
        }

        return $oVariants;
    }

    /**
     * @param $taxClassRate
     * @return mixed
     */
    public function getTaxClassValue($taxClassRate)
    {
        $taxClassMapping    = $this->getTaxClassMapping();

        if(true === array_key_exists($taxClassRate, $taxClassMapping))
        {
            return $taxClassMapping[$taxClassRate];
        }
    }

    /**
     */
    public function getLength() {
        return $this->oxarticles__oxlength->value;
    }

    /**
     */
    public function getWidth()
    {
        return $this->oxarticles__oxwidth->value;
    }

    /**
     */
    public function getHeight()
    {
        return $this->oxarticles__oxheight->value;
    }

    /**
     * @return array bf shop id to shop id mapping
     */
    protected function getTaxClassMapping()
    {
        return Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_TAXCLASS_MAPPING);
    }

}