<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 07.06.19
 * Time: 15:51
 */

namespace bfox\multichannel\classes\model;

use bfox\multichannel\wrapper\Core\Model\OxidBaseModel;
use bfox\multichannel\classes\util as Utils;
use OxidEsales\Eshop as Oxid;


class ScriptLoggerModel extends OxidBaseModel
{
    const STATUS_RUNNING  = 'running';
    const STATUS_FINISHED = 'finished';

    /**
     */
    public function __construct()
    {
        parent::__construct();

        $this->init('bfscriptlogger');
    }

    /**
     * @param array $scriptDependencies script dependencies
     */
    public function loadByScriptDependencies($scriptDependencies)
    {
        $query = sprintf(
            'SELECT * FROM %s WHERE OXSHOPID = %s AND SCRIPTSTATUS = \'%s\' AND SCRIPTNAME IN (\'%s\');',
            $this->getCoreTableName(),
            Utils\OxidRegistry::getActiveShopId(),
            self::STATUS_RUNNING,
            implode('\', \'', $scriptDependencies));

        $result = $this->assignRecord($query);

        if ($result != false) {
            $this->_isLoaded = true;
        }
    }

    /**
     */
    public function start()
    {
        if (false === is_null($this->getScriptName())) {
            $this->setOxid(Oxid\Core\UtilsObject::getInstance()->generateUID());
            $this->setOxShopId(Utils\OxidRegistry::getActiveShopId());
            $this->setScriptStatus(self::STATUS_RUNNING);
            $this->setDateStart(date('Y-m-d H:i:s'));
            $this->save();
        }
    }

    /**
     */
    public function end()
    {
        if (false === is_null($this->getOxid())) {
            $this->setScriptStatus(self::STATUS_FINISHED);
            $this->setDateEnd(date('Y-m-d H:i:s', time()));
            $this->save();
        }
    }

    /**
     * Clean up hanging script logger database entries
     *
     * @param integer $cleanupTime cleanup time
     */
    public function cleanupHangingRecords($cleanupTime)
    {
        $updateQuery = sprintf(
            'UPDATE %s SET SCRIPTSTATUS = \'%s\' WHERE SCRIPTSTATUS = \'%s\' AND DATESTART < \'%s\';',
            $this->getCoreTableName(),
            self::STATUS_FINISHED,
            self::STATUS_RUNNING,
            date('Y-m-d H:i:s', $cleanupTime)
        );

        Oxid\Core\DatabaseProvider::getDb()->execute($updateQuery);
    }

    /**
     * Removes old script logger database records
     *
     * @param integer $removalTime removal time
     */
    public function removeOldRecords($removalTime)
    {
        $deleteQuery = sprintf(
            'DELETE FROM %s WHERE SCRIPTSTATUS = \'%s\' AND DATESTART < \'%s\';',
            $this->getCoreTableName(),
            self::STATUS_FINISHED,
            date('Y-m-d H:i:s', $removalTime)
        );

        Oxid\Core\DatabaseProvider::getDb()->execute($deleteQuery);
    }

    /**
     * @return integer oxid
     */
    public function getOxid()
    {
        return $this->getFieldData('oxid');
    }

    /**
     * @param integer $oxid oxid
     * @return ScriptLoggerModel script logger model
     */
    public function setOxid($oxid)
    {
        $this->bfscriptlogger__oxid = oxNew(
            Oxid\Core\Field::class,
            $oxid,
            Oxid\Core\Field::T_RAW
        );

        return $this;
    }

    /**
     * @return integer
     */
    public function getOxShopId()
    {
        return $this->getFieldData('oxshopid');
    }

    /**
     * @param integer $oxShopId
     * @return ScriptLoggerModel
     */
    public function setOxShopId($oxShopId)
    {
        $this->bfscriptlogger__oxshopid = oxNew(
            Oxid\Core\Field::class,
            $oxShopId,
            Oxid\Core\Field::T_RAW
        );

        //we must set the _iShopId in the BaseModel again because the ScriptLogger Model
        //calls its constructor only once in the whole process if importing
        $this->setShopId($oxShopId);

        return $this;
    }

    /**
     * getScriptName.
     *
     * @return string script name
     */
    public function getScriptName()
    {
        return $this->getFieldData('scriptname');
    }

    /**
     * @param string $scriptName script name
     * @return ScriptLoggerModel script logger model
     */
    public function setScriptName($scriptName)
    {
        $this->bfscriptlogger__scriptname = oxNew(
            Oxid\Core\Field::class,
            $scriptName,
            Oxid\Core\Field::T_RAW
        );

        return $this;
    }

    /**
     * @return string script status
     */
    public function getScriptStatus()
    {
        return $this->getFieldData('scriptstatus');
    }

    /**
     * @param string $scriptStatus script status
     * @return ScriptLoggerModel script logger model
     */
    public function setScriptStatus($scriptStatus)
    {
        $this->bfscriptlogger__scriptstatus = oxNew(
            Oxid\Core\Field::class,
            $scriptStatus,
            Oxid\Core\Field::T_RAW
        );

        return $this;
    }

    /**
     * @return string date start
     */
    public function getDateStart()
    {
        return $this->getFieldData('datestart');
    }

    /**
     * @param string $dateStart date start
     * @return ScriptLoggerModel script logger model
     */
    public function setDateStart($dateStart)
    {
        $this->bfscriptlogger__datestart = oxNew(
            Oxid\Core\Field::class,
            $dateStart,
            Oxid\Core\Field::T_RAW
        );

        return $this;
    }

    /**
     * @return string date end
     */
    public function getDateEnd()
    {
        return $this->getFieldData('dateend');
    }

    /**
     * @param string $dateEnd date end
     * @return ScriptLoggerModel script logger model
     */
    public function setDateEnd($dateEnd)
    {
        $this->bfscriptlogger__dateend = oxNew(
            Oxid\Core\Field::class,
            $dateEnd,
            Oxid\Core\Field::T_RAW
        );;

        return $this;
    }
}