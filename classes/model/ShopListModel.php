<?php

/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 01.08.18
 * Time: 11:42
 */

    namespace bfox\multichannel\classes\model;

    use bfox\multichannel\wrapper\Application\Model as OxidApplicationModels;

    class ShopListModel extends OxidApplicationModels\OxidShopList
    {
        /**
         */
        public function loadShopList()
        {
            $sqlStatement = sprintf(
                'SELECT * FROM %s;',
                $this->getBaseObject()->getViewName()
            );

            $this->selectString($sqlStatement);
        }
    }