<?php
/**
 * ProductDeleteModel
 * This file is part of brickfox.
 *
 * @author brickfox GmbH <support@brickfox.de>
 * @copyright Copyright (c) 2011-2019 brickfox GmbH http://www.brickfox.de
 */

namespace bfox\multichannel\classes\model;


use bfox\multichannel\wrapper\Core\Model\OxidBaseModel;

class ProductDeleteModel extends OxidBaseModel
{

}