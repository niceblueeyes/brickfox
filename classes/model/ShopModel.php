<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 01.08.18
 * Time: 11:59
 */

namespace bfox\multichannel\classes\model;

use bfox\multichannel\wrapper\Application\Model as OxidApplicationModels;


class ShopModel extends OxidApplicationModels\OxidShop
{

    /**
     * @return bool return if the shop is super shop
     */
    public function isSuperShop()
    {
        return (bool) $this->getFieldData('oxissupershop');
    }


    /**
     * @return int ox parent shop id
     */
    public function getParentShopId()
    {
        return $this->getFieldData('oxparentid');
    }

}