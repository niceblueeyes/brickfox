<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 11.06.19
 * Time: 16:31
 */

namespace bfox\multichannel\classes\exception;


class ImportExportException extends \Exception
{
    /**
     * exception codes
     */
    const GENERAL						= 1,
        NO_XML_ELEMENTS					= 2,
        INVALID_XML						= 3,
        SCRIPT_RUNNING					= 4;

}