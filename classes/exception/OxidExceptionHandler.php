<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 31.07.18
 * Time: 18:47
 */

namespace bfox\multichannel\classes\exception;


class OxidExceptionHandler extends OxidExceptionHandler_parent
{
    /**
     * exception codes
     */
    const   GENERAL							= 1,
            NO_XML_ELEMENTS					= 2,
            INVALID_XML						= 3,
            SCRIPT_RUNNING					= 4;


    public function __construct($iDebug = 0)
    {
        parent::__construct($iDebug);
    }


    public function handleUncaughtException(\Throwable $exception)
    {
        echo "<hr>";
        $errorMessage = "<p>EXCEPTION: code {$exception->getCode()}</p>";
        $errorMessage .= $exception->getMessage();

        $errorMessage .=  "<h2>file: " . $exception->getFile() . " line: " . $exception->getLine() . "</h2>";
        $errorMessage .=  "<h4>stack: " . str_replace("#", "<br>#",  $exception->getTraceAsString()) . "</h4>";

        echo $errorMessage;
        echo "<hr>";

        $this->exitApplication();
    }


    public function handleUncaughtError($arg1, $arg2 = 0, $arg3 = 0, $arg4 = 0, $arg5 = 0)
    {
        if ($arg1 === E_WARNING || $arg1 === E_NOTICE)
        {
            # echo "Warning or Notice<br>";
            return;
        }

        echo "<hr>";
        $errorMessage = "<p>ERROR: #{$arg1}</p>";
        $errorMessage .= $arg2;

        $errorMessage .=  "<h2>file: " . $arg3 . " line: " . $arg4 . "</h2>";
        echo $errorMessage;

        if( in_array($arg1, array(E_USER_ERROR, E_RECOVERABLE_ERROR))  ) {
            echo "<h4>stack: <pre>";
            var_dump($arg5);
            echo "</pre></h4>";
            echo "<hr>";
            ($arg1 == E_USER_ERROR) ? $this->exitApplication() : "nix" ;
        }

        echo "<hr>";
    }


    public function internalExceptionCatch (\Exception $exception){

        echo "<hr>";
        $errorMessage = "<p>Internal EXCEPTION</p>";
        $errorMessage .= $exception->getMessage();

        $errorMessage .=  "<h2>file: " . $exception->getFile() . " line: " . $exception->getLine() . "</h2>";
        $errorMessage .=  "<h4>stack: " . str_replace("#", "<br>#",  $exception->getTraceAsString()) . "</h4>";

        echo $errorMessage;

        echo "<h2>Trace details </h2>>>>>>>>";

        echo "<pre>";
        var_dump($exception->getTrace());
        echo "</pre>";

        echo "<hr>";

    }


    private function showAutoloadArrayFiles(){
        if ($_SESSION['debug_composer_autoload_files']) {
            echo "<h3>Debug Composer Autoload</h3>";
            echo "<pre>";
            var_dump($_SESSION['debug_composer_autoload_files']);
            echo "</pre>";
        }

    }



}