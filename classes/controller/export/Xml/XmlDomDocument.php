<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 15.08.18
 * Time: 17:25
 */

namespace bfox\multichannel\classes\controller\export\Xml;

use bfox\multichannel\classes\util as Utils;

class XmlDomDocument extends \DOMDocument
{
    /**
     * createElement()
     *
     * Create a DOMNode
     * Encode to UTF-8 if shop is not configured to use utf-8
     * Replace XML-invalid chars
     * Check for invalid strings
     *
     * @param String $name Name of the Node
     * @param String $value Value to attach to the Node
     * @return \DOMNode
     */
    public function createElement($name, $value = null)
    {
        if($value)
        {
            $value = Utils\SystemManager::localToUtf8($value);

            $value = str_replace('&', '&amp;', $value);
            $value = str_replace('<', '&lt;', $value);
            $value = str_replace('>', '&gt;', $value);
            if(!@simplexml_load_string('<data>' . $value . '</data>'))
            {
                $value = '';
            }
        }
        $domElement       = oxNew(XmlDomElement::class, $name, $value);
        $documentFragment = $this->createDocumentFragment();
        $documentFragment->appendChild($domElement);

        return $documentFragment->removeChild($domElement);
    }

    /**
     * createCDATASection()
     *
     * Create a DOMCDATASection
     * Encode to UTF-8 if shop is not configured to use utf-8
     *
     * @param String $data data to write to cdata-section
     * @return \DOMCDATASection
     */
    public function createCDATASection($data)
    {
        if($data)
        {
            $data = Utils\SystemManager::localToUtf8($data);

            if(!@simplexml_load_string('<data><![CDATA[' . $data . ']]></data>', null, LIBXML_NOCDATA))
            {
                $data = '';
            }
        }

        return parent::createCDATASection($data);
    }

    /**
     * createTextNode()
     *
     * Create a DOMTextNode
     * Encode to UTF-8 if shop is not configured to use utf-8
     *
     * @param String $data data to write to text-node
     * @return \DOMText
     */
    public function createTextNode($data)
    {
        $data = Utils\SystemManager::localToUtf8($data);

        $data = str_replace('&', '&amp;', $data);
        $data = str_replace('<', '&lt;', $data);
        $data = str_replace('>', '&gt;', $data);
        if(!@simplexml_load_string('<data>' . $data . '</data>'))
        {
            $data = '';
        }

        return parent::createTextNode($data);
    }
}
