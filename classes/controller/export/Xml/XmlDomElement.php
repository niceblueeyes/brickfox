<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 15.08.18
 * Time: 17:30
 */

namespace bfox\multichannel\classes\controller\export\Xml;


class XmlDomElement extends \DOMElement
{
    /**
     * __construct()
     *
     * Does nothing but constructing DOMElement
     *
     * @param String $name
     * @param String $value
     * @param String $uri
     */
    public function __construct($name, $value = '', $uri = null)
    {
        parent::__construct($name, $value, $uri);
    }
}