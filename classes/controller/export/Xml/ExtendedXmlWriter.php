<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 15.08.18
 * Time: 17:32
 */

namespace bfox\multichannel\classes\controller\export\Xml;

use bfox\multichannel\classes\util as Utils;

class ExtendedXmlWriter extends \XMLWriter
{
    /**
     * @var string
     */
    protected $fileName;

    /**
     * @var string
     */
    protected $fileIsOpen;

    /**
     * @param string $type
     * @param int $shopsId
     * @param string $timeOfExport
     * @param int $fileNumber
     */
    public function __construct($type, $shopsId = 1, $timeOfExport = null, $fileNumber = 0)
    {
        if (is_null($timeOfExport) || $timeOfExport == '')
        {
            $timeOfExport = date("Ymd_His");
        }

        $strFileNumber = '';
        if ($fileNumber > 0)
        {
            $strFileNumber = '_'.$fileNumber;
        }

        $exchangePath = Utils\SystemManager::getInstance()->getShopExchangePath();
        $name = $type . '_' . $shopsId . '_' . $timeOfExport . $strFileNumber . '.xml';
        $this->fileName = $exchangePath . $name ;

        $dir = '';
        foreach (explode('/', $exchangePath) as $part) {

            $dir .= '/' . $part;

            if(is_dir($dir) === false) {
                mkdir($dir);
            }
        }

        #if($this->openUri('php://output'))
        if($this->openUri($this->fileName))
        {
            Utils\LogManager::getInstance()->debug("File {$name} created");
            $this->setFileIsOpen(true);
            $this->startDocument('1.0', 'utf-8');
            $this->setIndent(true);
            $this->setIndentString('	');
        }
        else{
            Utils\LogManager::getInstance()->error('Could not create XML file!');
            $this->setFileIsOpen(false);
        }

    }

    /**
     * @param array $data
     * @return ExtendedXmlWriter
     */
    public function writeArray($data)
    {
        foreach($data as $key => $value)
        {
            if(substr_count($key, ':') > 0)
            {
                $arr_key = explode(':', $key);
                if(substr_count($arr_key[0], '_') > 0)
                {
                    $arr_key_items = explode('_', $arr_key[0]);
                    $arr_key[0]    = $arr_key_items[0];
                }
                $this->startElement($arr_key[0]);

                for ($i = 1; $i < count($arr_key); $i = $i + 2)
                {
                    @$this->writeAttribute($arr_key[$i], $arr_key[$i + 1]);
                }
            }
            elseif(substr_count($key, '_') > 0)
            {
                $arr_key = explode('_', $key);
                $this->startElement($arr_key[0]);
            }
            else
            {
                $this->startElement($key);
            }

            if(is_array($value))
            {
                $this->writeArray($value);
            }
            else
            {
                if(preg_match('/<|>/', $value))
                {
                    $this->writeCdata($value);
                }
                else
                {
                    $this->text($value);
                }
            }

            $this->endElement();
            $this->flush();
        }
        unset($data);

        return $this;
    }

    /**
     * @param string $value
     * @return bool|void
     */
    public function writeCdata($value)
    {
        $value = Utils\SystemManager::localToUtf8($value);
        $value = preg_replace('/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]/u', '', $value);
        parent::writeCdata($value);
    }

    /**
     * @param string $value
     * @return bool|void
     */
    public function text($value)
    {
        $value = Utils\SystemManager::localToUtf8($value);
        parent::text($value);
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @return bool
     */
    public function getFileIsOpen()
    {
        return $this->fileIsOpen;
    }

    /**
     * @param bool $openFlag
     *
     */
    public function setFileIsOpen($openFlag)
    {
        $this->fileIsOpen = $openFlag;
    }

}