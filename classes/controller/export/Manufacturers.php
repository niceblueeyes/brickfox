<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 22.08.18
 * Time: 12:59
 */

namespace bfox\multichannel\classes\controller\export;

use bfox\multichannel\classes\model as Models;

class Manufacturers extends Wrapper
{
    private $translations;

    public function __construct(Models\ManufacturerModel $manufacturerModel, $exportOptions, $languages)
    {
        $this->setExportOptions($exportOptions);
        $this->languages = $languages;

        $this->output['ManufacturerId']             = $manufacturerModel->oxmanufacturers__oxid;

        //Get MultiLanguage Fields.
        $this->getMultilanguageFields($manufacturerModel);

    }


    /**
     * get Multilanguage Fields
     * @param Models\ManufacturerModel $manufacturerModel
     *
     */
    private function getMultilanguageFields(Models\ManufacturerModel $manufacturerModel)
    {
        foreach ($this->getLanguages() as $language)
        {

            $languageAbbreviation = $language['abbr'];
            $manufacturerModel->loadInLang($language['id'], $manufacturerModel->oxmanufacturers__oxid);

            // get Titles
            $title = $manufacturerModel->getTitle();
            if (count($title) > 0)
            {
                $this->translations['Translation:lang:'.$languageAbbreviation] =  ["Name" => $title];
            }

        }

        //Set collected values

        if(count($this->translations) > 0)
        {
            $this->output['Translations'] = $this->translations;
        }


    }
}