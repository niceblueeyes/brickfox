<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 16.08.18
 * Time: 14:29
 */

namespace bfox\multichannel\classes\controller\export;

use bfox\multichannel\classes\model as Models;
use OxidEsales\Eshop\Core\Registry;

class Products extends ProductsWrapper
{
    const EXPORT_INCLUDE_ATTRIBUTES = 'Attributes';

    const EXPORT_INCLUDE_CATEGORIES = 'Categories';

    const EXPORT_INCLUDE_IMAGES = 'Images';

    const EXPORT_INCLUDE_DESCRIPTIONS = 'Descriptions';

    const EXPORT_INCLUDE_CURRENCIES = 'Currencies';

    const EXPORT_INCLUDE_PACKAGE = 'Package';

    const EXPORT_INCLUDE_OPTIONS = 'Options';



    /** @var array */
    protected $descriptions;

    /** @var  array */
    protected $varOptionNames;

    /** @var  array */
    protected $varOptionValues;

    /** @var ProductsVariations $productsVariations */
    private $productsVariations;

    /** @var Models\AttributeModel $attributeModel */
    protected $attributeModel;

    /** @var array $attributesTypes */
    protected $attributesTypes;

    /** @var array $copyFieldsToVariant; */
    private $copyFieldsToVariant;


        /**
     * __construct
     * @param Models\ProductModel $articleModel
     * @param array $exportOptions
     * @param ProductsVariations $productsVariations
     * @param Models\AttributeModel $attributeModel
     * @param \stdClass $oCurrency
     * @param array $languages
     */
    public function __construct(Models\ProductModel $articleModel,
                                array $exportOptions,
                                ProductsVariations $productsVariations,
                                Models\AttributeModel $attributeModel,
                                \stdClass $oCurrency,
                                array $languages
    )
    {
        $this->attributesTypes = [];
        $this->copyFieldsToVariant = [];
        $this->varOptionValues = [];
        $this->varOptionNames = [];
        $this->descriptions = [];

        $this->setExportOptions($exportOptions);
        $productsVariations->setExportOptions($exportOptions);
        $this->setProductsVariations($productsVariations);
        $this->setAttributeModel($attributeModel);
        $this->configCurrency = $oCurrency;
        $this->languages = $languages;


        /**
         * Begin of the output
         */
        $this->output['ProductExternId']        = $articleModel->oxarticles__oxid;
        $this->output['ItemNumber']             = $articleModel->oxarticles__oxartnum;
        $this->output['Active']                 = $articleModel->oxarticles__oxactive;
        $this->output['TaxClass']               = $articleModel->getTaxClassValue($articleModel->getArticleVat());

        $manufacturer = $articleModel->getManufacturer();
        $this->output['Manufacturer']           = is_null($manufacturer) ? "" : $manufacturer->getTitle();
        $this->output['ExternManufacturerId']   = $articleModel->oxarticles__oxmanufacturerid;



        $mappedAttributesFromArticle    = $this->getMappedAttributes($articleModel);
        $articleAttributesTypes         = $this->getAttributesTypes($articleModel->getAttributes()->getArray());
        if(($articleAttributesTypes > 0 || $mappedAttributesFromArticle > 0) && $this->exportInclude(self::EXPORT_INCLUDE_ATTRIBUTES) === true)
        {
            $this->output['Attributes'] = $this->attributesTypes;
        }

        $categories = $this->getCategories($articleModel->getCategoryIds(false,false));
        if(count($categories) > 0 && $this->exportInclude(self::EXPORT_INCLUDE_CATEGORIES) === true)
        {
            $this->output['Categories'] = $categories;
        }

        $images = $this->getImages($articleModel);
        if(count($images) > 0 && $this->exportInclude(self::EXPORT_INCLUDE_IMAGES) === true)
        {
            $this->output['Images'] = $images;
        }

        //Get MultiLanguage Fields in ProductModel.
        $this->getMultilanguageFields($articleModel);

        //Variations call MUST be done AFTER Multilanguage fields
        $variations = $this->getVariations($articleModel);
        if(count($variations) > 0)
        {
            $this->output['Variations'] = $variations;
        }
    }




    /**
     * get Multilanguage Fields
     * @param Models\ProductModel $oxidArticleModel
     */
    private function getMultilanguageFields($oxidArticleModel)
    {
        $newModel = new Models\ProductModel();

        if(count($this->getLanguages()) == 0){
            $this->languages = array(0);
        }

        foreach ($this->getLanguages() as $language)
        {
            $articleModel = null;

            if ((int)$language['id'] === $oxidArticleModel->getLanguage())
            {
                $articleModel = $oxidArticleModel;
            }
            else
            {
                $articleModel = $newModel;
                $articleModel->loadInLang($language['id'], $oxidArticleModel->oxarticles__oxid);
            }

            $languageAbbreviation = $language['abbr'];

            $articleModel->resetLanguageFields();

            // get descriptions
            $fields = $this->getDescriptionFields($articleModel, false,null,null, $languageAbbreviation);
            if (count($fields) > 0)
            {
                $this->descriptions['Description:lang:'.$languageAbbreviation] = $fields;
            }

            //Get  Options Names in parent, the values are in the variants
            if (!is_null($articleModel->oxarticles__oxvarname))
            {
                $this->varOptionNames[$languageAbbreviation] = $articleModel->oxarticles__oxvarname->getRawValue();
            }
        }

        //Set collected values
        if(count($this->descriptions) > 0 && $this->exportInclude(self::EXPORT_INCLUDE_DESCRIPTIONS) === true)
        {
            $this->output['Descriptions'] = $this->descriptions;
        }

        $this->getProductsVariations()->setVarOptionNames($this->varOptionNames);
    }

    /**
     * @param Models\ProductModel $articleModel
     * @return int
     */
    protected function getMappedAttributes(Models\ProductModel $articleModel)
    {
        $count = count($this->attributesTypes);
        $attributesTypes = [];
        $aFields = $this->exportOptions['articleFieldsToAttribute'];

        foreach ($aFields as $fieldName => $outputName)
        {
            $fieldName = strtolower(trim($fieldName));
            $outputName = trim($outputName);

            $attributeTranslations = [];
            $attributeField = $articleModel->{'oxarticles__'.$fieldName};

            foreach ($this->getLanguages() as $language)
            {
                $attributeTranslation = [];

                if ((int)$language['id'] === $articleModel->getLanguage())
                {
                    $attributeTranslation['Name'] = $outputName;
                    $attributeTranslation['Value'] = is_null($attributeField) ? "" : $attributeField->getRawValue();
                    $attributeTranslations['Translation:lang:' . $language['abbr']] = $attributeTranslation;
                }
            }
            $attributesTypes['String:'.$count]['Translations'] = $attributeTranslations;
            $count++;
        }

        $this->addCollectedAttributes($attributesTypes);
        return $count;
    }


    /**
     * get Attributes Types
     * @param array $attributeList
     * @return int
     */
    protected function getAttributesTypes($attributeList)
    {
        $count = count($this->attributesTypes);
        $attributesTypes = [];

        foreach ($attributeList as $key => $oxidAttributeModel)
        {
            $attributeTranslations = [];

            foreach ($this->getLanguages() as $language)
            {
                $attributeTranslation = [];

                if ((int)$language['id'] === $oxidAttributeModel->getLanguage())
                {
                    $attributeModel = $oxidAttributeModel;
                }
                else{
                    /** @var Models\AttributeModel $attributeModel */
                    $attributeModel = $this->getAttributeModel();
                    $attributeModel->loadInLang($language['id'], $oxidAttributeModel->oxattribute__oxid );
                }

                $attributeTitle                 = $attributeModel->oxattribute__oxtitle;
                $attributeTranslation['Name']   = is_null($attributeTitle) ? "" : $attributeTitle->getRawValue();

                $attributeField                 = $attributeModel->oxattribute__oxvalue;
                $attributeTranslation['Value']  = is_null($attributeField) ? "" : $attributeField->getRawValue();

                $attributeTranslations['Translation:lang:'.$language['abbr']] = $attributeTranslation;
            }

            $attributesTypes['String:'.$count]['Translations'] = $attributeTranslations;
            $count++;
        }

        $this->addCollectedAttributes($attributesTypes);
        return $count;
    }

    /**
     * @param array $attributesCollected
     */
    private function addCollectedAttributes($attributesCollected)
    {
        foreach ($attributesCollected as $key => $attributeTypeTag)
        {
            $this->attributesTypes[$key] = $attributeTypeTag;
        }
    }


    /**
     * get Categories
     * @param array $articleCategoryIds
     * @return array
     */
    protected function getCategories($articleCategoryIds)
    {
        $categories = [];
        $counter = 1;

        foreach ($articleCategoryIds as $articleCategoryId)
        {
            $categories['Category:'.$counter] = array(
                'ExternCategoryId' => $articleCategoryId
            );

            $counter++;
        }

        return $categories;
    }



    /**
     * get Descriptions Fields
     * @param Models\ProductModel $articleModel
     * @param string $tagName optional, if the tags names have additional prefix
     * @param bool $isVariation
     * @param array $copyFields
     * @param string $lang which language used for cache
     * @return array
     */
    protected function getDescriptionFields(Models\ProductModel $articleModel, $isVariation, $copyFields = [], $tagName = '', $lang = null)
    {
        $fields = [];

        $unitField   = $articleModel->oxarticles__oxunitname;
        $contentUnit = is_null($unitField) ? "" : $unitField->getRawValue();

        $titleField = $articleModel->oxarticles__oxtitle;
        $title      = is_null($titleField) ? "" : $titleField->getRawValue();

        $shortDescField = $articleModel->oxarticles__oxshortdesc;
        $shortDesc      = is_null($shortDescField) ? "" : $titleField->getRawValue();

        /* Beginn Bodynova änderung */

        if($lang == null){
            $lang = 'de';
        }
        if($isVariation){
            $oxid = $articleModel->oxarticles__oxparentid->value;
        }else{
            $oxid = $articleModel->oxarticles__oxid->value;
        }
        #$LongDesc   = $articleModel->getLongDescription()->getRawValue();//$articleModel->getLongDesc();

        $filename = dirname(__DIR__, 3) . "/cache/".$lang."/".$oxid.".html";
        if (file_exists($filename)) {
            $LongDesc = file_get_contents($filename);
        } else {
            $LongDesc = $articleModel->getLongDescription()->getRawValue();
        }

        /* Ende Bodynova Änderung */
        $productUrl = $articleModel->getMainLink();

        if ($isVariation === true){
            isset($copyFields['ContentUnit']) ? $fields[$tagName.'ContentUnit'] = $copyFields['ContentUnit'] : null;
            (strlen($title) > 0)        ? $fields[$tagName.'TitleShort'] = $title : null;
        }
        else{
            (strlen($contentUnit) > 0)  ? $this->copyFieldsToVariant['ContentUnit'] = $contentUnit : null;
            (strlen($title) > 0)        ? $fields[$tagName.'Title'] = $title : null;
        }

        (strlen($shortDesc) > 0)    ? $fields[$tagName.'ShortDescription'] = $shortDesc : null;
        (strlen($LongDesc) > 0)     ? $fields[$tagName.'LongDescription']  = $LongDesc : null;
        (strlen($productUrl) > 0)   ? $fields[$tagName.'ProductUrl']  = $productUrl : null;

        return $fields;
    }


    /**
     * get Images
     * @param Models\ProductModel $articleModel
     * @param string $tagName
     * @return array
     */
    protected function getImages(Models\ProductModel $articleModel, $tagName = 'Image')
    {
        $images = [];
        $imagesInfo = $articleModel->getExtendedPictureGallery();
        $counter = 1;

        $galleryToExport = $this->exportOptions['exportImageSizes'];

        if ($galleryToExport === "ZoomPics" && $imagesInfo['ZoomPic'] === false)
        {
            $galleryToExport = "Pics";
        }

        foreach($imagesInfo[$galleryToExport] as $key => $articleImage)
        {
            $filePath = ($galleryToExport === "ZoomPics") ? $articleImage["file"] : $articleImage;

            $images[$tagName.':sort:'.$counter] = array(
                'Path' => $filePath
            );
            $counter++;
        }

        return $images;
    }


    /**
     * get Variations
     * @param Models\ProductModel $articleModel
     * @return array
     */
    private function getVariations(Models\ProductModel $articleModel)
    {
        $adminVariations = $articleModel->getAdminVariants();
        $count = 1;
        $variations = [];

        $this->copyParentFields();

        if (count($adminVariations) === 0)
        {
            $this->getProductsVariations()->setArticleHasNoVariant(true);
            $variations['Variation'] = $this->getProductsVariations()->getArray($articleModel);

            return $variations;
        }

        foreach($adminVariations as $articleVariation)
        {
            $variation = $this->getProductsVariations()->getArray($articleVariation);

            $variations['Variation:'.$count] = $variation;
            $count++;
        }

        $this->resetAttributeModel();

        return $variations;
    }

    /**
     * copyParentFields -
     * transfer fields captured in the Parent that will be used in the variants
     */
    private function copyParentFields()
    {
        foreach ($this->copyFieldsToVariant as $key => $value)
        {
            $this->getProductsVariations()->setParentField($key, $value);
        }
    }

    /**
     * setProductsVariations
     * @param ProductsVariations $productsVariations
     */
    private function setProductsVariations(ProductsVariations $productsVariations)
    {
        $this->productsVariations = $productsVariations;
    }

    /**
     * getProductsVariations
     * @return ProductsVariations
     */
    private function getProductsVariations()
    {
        return $this->productsVariations;
    }

    /**
     * resetAttributeModel
     */
    protected function resetAttributeModel()
    {
        $this->attributeModel = null;
    }

    /**
     * setAttributeModel
     * @param Models\AttributeModel $attributeModel
     */
    protected function setAttributeModel(Models\AttributeModel $attributeModel)
    {
        $this->attributeModel = $attributeModel;
    }

    /**
     * getAttributeModel
     * @return Models\AttributeModel
     */
    protected function getAttributeModel()
    {
        return $this->attributeModel;
    }


    /**
     * setVarOptionNames
     * @param array $varOptionNames
     */
    protected function setVarOptionNames($varOptionNames)
    {
        $this->varOptionNames = $varOptionNames;
    }

    /**
     * getVarOptionNames
     * @return array
     */
    protected function getVarOptionNames()
    {
        return $this->varOptionNames;
    }



}