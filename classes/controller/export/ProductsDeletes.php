<?php
/**
 * ProductsDeletes
 * This file is part of brickfox.
 *
 * @author brickfox GmbH <support@brickfox.de>
 * @copyright Copyright (c) 2011-2019 brickfox GmbH http://www.brickfox.de
 */

namespace bfox\multichannel\classes\controller\export;

use bfox\multichannel\classes\model as Models;
use \OxidEsales\Eshop as Oxid;

class ProductsDeletes extends Wrapper
{
    /**
     * @param Models\ProductDeleteModel $productDeleteModel
     */
    public function __construct(Models\ProductDeleteModel $productDeleteModel)
    {
        $productExternId = $productDeleteModel->__productexternid;

        $this->output["ProductExternId:id:$productExternId"] = $productExternId;

        $sQuery = "UPDATE brickfox_articles_exports" .
                  " SET is_deleted = 1" .
                  " WHERE oxid_id = '$productExternId'";

        Oxid\Core\DatabaseProvider::getDb()->execute($sQuery);
    }
}