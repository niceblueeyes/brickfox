<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 17.08.18
 * Time: 10:23
 */

namespace bfox\multichannel\classes\controller\export;

use bfox\multichannel\classes\model as Models;
use bfox\multichannel\classes\util as Utils;

class ProductsVariations extends Products
{
    private $variation = [];

    private $options = [];

    private $optionStrings = [];

    private $articleHasNoVariant = false;

    private $parentFields = [];

    public function __construct(
        Models\AttributeModel $attributeModel,
        array $exportOptions,
        array $languages,
        \stdClass $oCurrency
    ) {
        $this->setExportOptions($exportOptions);
        $this->setAttributeModel($attributeModel);
        $this->languages      = $languages;
        $this->configCurrency = $oCurrency;
    }

    /**
     * @param Models\ProductModel $articleVariations
     * @return array
     */
    public function getArray(Models\ProductModel $articleVariations)
    {
        $this->variation['VariationExternId']      = $articleVariations->oxarticles__oxid;
        $this->variation['Weight']                 = $articleVariations->getWeight();
        $this->variation['VariationItemNumber']    = $articleVariations->oxarticles__oxartnum;
        $this->variation['ManufacturerItemNumber'] = $articleVariations->oxarticles__oxmpn;
        $this->variation['EAN']                    = $articleVariations->oxarticles__oxean;
        $this->variation['VariationActive']        = $articleVariations->oxarticles__oxactive;
        $this->variation['Available']              = $this->getAvailabilityByStock($articleVariations->oxarticles__oxstock->getRawValue());
        $this->variation['Stock']                  = $articleVariations->oxarticles__oxstock;
        $this->variation['ContentQuantity']        = $articleVariations->oxarticles__oxunitquantity;

        if ($this->exportOptions["thirdPartyStockConfig"]["ThirdPartyStock"] === true) {
            $fieldToMap = $this->exportOptions["thirdPartyStockConfig"]["ThirdPartyMapFrom"];
            $fieldValue = $articleVariations->{'oxarticles__' . $fieldToMap};

            $this->variation['ThirdPartyStock'] = $this->getThirdPartyStockValue($fieldValue);
        }

        $currencies = $this->getCurrencies($articleVariations);
        if (count($currencies) > 0 && $this->exportInclude(self::EXPORT_INCLUDE_CURRENCIES) === true) {
            $this->variation['Currencies'] = $currencies;
        } else {
            $msg = "No Prices found for Variation Id: " . $this->variation['VariationExternId'];
            Utils\LogManager::getInstance()->debug($msg);
        }

        $package = $this->getPackage($articleVariations);
        if (count($package) > 0 && $this->exportInclude(self::EXPORT_INCLUDE_PACKAGE) === true) {
            $this->variation['Package'] = $package;
        }

        $articleAttributesTypes = $this->getVariationAttributesTypes($articleVariations);
        if ($articleAttributesTypes > 0 && $this->exportInclude(self::EXPORT_INCLUDE_ATTRIBUTES) === true) {
            $this->variation['Attributes'] = $this->attributesTypes;
        }

        $variationImages = $this->getVariationImages($articleVariations);
        if (count($variationImages) > 0 && $this->exportInclude(self::EXPORT_INCLUDE_IMAGES) === true) {
            $this->variation['VariationImages'] = $variationImages;
        }

        //Get MultiLanguage Fields in ProductModel.
        $this->getVariationsMultilanguageFields($articleVariations);

        return $this->variation;
    }

    /**
     * get Multilanguage Fields
     *
     * @param Models\ProductModel $oxidArticleModel
     */
    private function getVariationsMultilanguageFields($oxidArticleModel)
    {
        $copyDescriptionFields = $this->checkCopyDescriptionFields();

        if ($this->articleHasNoVariant === true && $copyDescriptionFields === false) {
            return;
        }

        $newModel = new Models\ProductModel();

        foreach ($this->getLanguages() as $language) {
            $articleVariations = null;

            if ((int)$language['id'] === $oxidArticleModel->getLanguage()) {
                $articleVariations = $oxidArticleModel;
            } else {
                $articleVariations = $newModel;
                $articleVariations->loadInLang($language['id'], $oxidArticleModel->oxarticles__oxid);
            }

            $languageAbbreviation = $language['abbr'];
            $articleVariations->resetLanguageFields();

            // get descriptions
            $fields = $this->getVariationsDescriptionFields($articleVariations, $languageAbbreviation);
            if (count($fields) > 0) {
                $this->descriptions['Description:lang:' . $languageAbbreviation] = $fields;
            }

            // get Options full string
            $this->getOptionString($articleVariations, $languageAbbreviation);
        }

        if (count($this->descriptions) > 0 && $this->exportInclude(self::EXPORT_INCLUDE_DESCRIPTIONS) === true) {
            $this->variation['Descriptions'] = $this->descriptions;
        }

        $this->getOptions();
        if (count($this->options) > 0
            && $this->exportInclude(self::EXPORT_INCLUDE_OPTIONS) === true
            && $this->articleHasNoVariant === false
        ) {
            $this->variation['Options'] = $this->options;
        }
    }

    private function checkCopyDescriptionFields()
    {
        $multilanguageFields = ['ContentUnit', 'LongDescription', 'ShortDescription', 'Title', 'TitleShort'];

        foreach ($this->getParentFields() as $key => $value) {
            if (in_array($key, $multilanguageFields)) {
                return true;
            }
        }

        return false;
    }

    /**
     * get Variations Descriptions
     *
     * @param Models\ProductModel $articleVariations
     * @param string $lang Language used for Cache
     * @return array
     */
    private function getVariationsDescriptionFields(Models\ProductModel $articleVariations, $lang)
    {
        return parent::getDescriptionFields($articleVariations, true, $this->getParentFields(), null, $lang);
    }

    /**
     * get Options
     * Here is really hard logic, because we must invert the relational order
     * between Options and Translations.
     * So first we collect the translation data, build a new structure and
     * if necessary, we split the translation in more options.
     *
     * @return array
     */
    private function getOptions()
    {
        $option            = [];
        $optionTranslation = [];

        foreach ($this->getLanguages() as $language) {
            $optionsString = $this->optionStrings["lang:" . $language['abbr']];

            $arrNames  = explode("|", $optionsString["OptionName"]);
            $arrValues = explode("|", $optionsString["OptionValue"]);

            if (count($arrNames) === count($arrValues)) {
                for ($count = 0; $count < count($arrNames); $count++) {
                    $name  = trim($arrNames[$count]);
                    $value = trim($arrValues[$count]);

                    if (isset($optionTranslation[$name]["Translation:lang:" . $language['abbr']])) {
                        $optionTranslation[$name]["Translation:lang:" . $language['abbr']]["OptionValue"] .= "-$value";
                    } else {
                        $optionTranslation[$name]["Translation:lang:" . $language['abbr']]["OptionName"]  = $name;
                        $optionTranslation[$name]["Translation:lang:" . $language['abbr']]["OptionValue"] = $value;
                    }
                }
            } else {
                $msg = "Number of Option Names amount doesn't match with total Option Values. Variation ID: " . $this->variation['VariationExternId'];
                Utils\LogManager::getInstance()->debug($msg);
            }
        }

        $counter = 0;

        foreach ($optionTranslation as $value) {
            $option['Option:' . $counter++] = [
                "Translations" => $value,
            ];
        }

        $this->options = $option;
    }

    /**
     * @param Models\ProductModel $articleVariations
     * @param $languageAbbreviation
     */
    private function getOptionString(Models\ProductModel $articleVariations, $languageAbbreviation)
    {
        if (!is_null($articleVariations->oxarticles__oxvarselect)) {
            $this->optionStrings["lang:" . $languageAbbreviation]
                = [
                "OptionName"  => $this->varOptionNames[$languageAbbreviation],
                "OptionValue" => $articleVariations->oxarticles__oxvarselect->getRawValue()
            ];
        }
    }

    /**
     * get Variation Images
     *
     * @param Models\ProductModel $articleVariations
     * @return int
     */
    private function getVariationAttributesTypes(Models\ProductModel $articleVariations)
    {
        $this->attributesTypes = [];

        if ($this->articleHasNoVariant === true) {
            return 0;
        }

        return parent::getAttributesTypes($articleVariations->getAttributes()->getArray());
    }

    /**
     * get Variation Images
     *
     * @param Models\ProductModel $articleVariations
     * @return array
     */
    private function getVariationImages(Models\ProductModel $articleVariations)
    {
        if ($this->articleHasNoVariant === true) {
            return [];
        }

        return parent::getImages($articleVariations);
    }

    private function getThirdPartyStockValue($fieldValue)
    {
        $outputMode = $this->exportOptions["thirdPartyStockConfig"]["ThirdPartyValue"];

        if (is_a($fieldValue, 'OxidEsales\Eshop\Core\Field')) {
            $value = $fieldValue->getRawValue();
        } else {
            $value = $fieldValue;
        }

        //when field value is selected - return
        if ($outputMode === 'fieldValue') {
            return $value;
        }

        $value     = $this->checkForStringExceptions($value);
        $boolValue = boolval($value);

        if ($outputMode === 'blnText') {
            $boolValue = ($boolValue) ? 'true' : 'false';
        }

        if ($outputMode === 'blnNumber') {
            $boolValue = ($boolValue) ? 1 : 0;
        }

        return $boolValue;
    }

    private function checkForStringExceptions($value)
    {
        if (gettype($value) === "string") {
            $exceptions = ['0000-00-00', '0000-00-00 00:00:00', 'null', 'NULL'];

            in_array(trim($value), $exceptions) ? $value = '' : null;
        }

        return $value;
    }

    /**
     * @param $boolFlag
     */
    public function setArticleHasNoVariant($boolFlag)
    {
        $this->articleHasNoVariant = $boolFlag;
    }

    /**
     * @return bool
     */
    public function getArticleHasNoVariant()
    {
        return $this->articleHasNoVariant;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function setParentField($key, $value)
    {
        $this->parentFields[$key] = $value;
    }

    /**
     * @param string $key
     * @return string
     */
    public function getParentField($key)
    {
        if (isset($this->parentFields[$key])) {
            return $this->parentFields[$key];
        }

        return "";
    }

    /**
     * @return array
     */
    public function getParentFields()
    {
        return $this->parentFields;
    }
}