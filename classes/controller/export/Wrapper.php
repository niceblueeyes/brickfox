<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 16.08.18
 * Time: 15:19
 */

namespace bfox\multichannel\classes\controller\export;


class Wrapper
{
    /** @var array output  */
    protected $output = array();

    /** @var array */
    protected $languages;

    /** @var array $exportOptions */
    protected $exportOptions;


    /**
     * toArray.
     *
     * @return array output
     */
    public function toArray()
    {
        return $this->output;
    }


    /**
     * setExportOptions
     * @param array $exportOptions
     */
    public function setExportOptions($exportOptions)
    {
        $this->exportOptions = $exportOptions;
    }

    /**
     * getExportOptions
     * @return array
     */
    public function getExportOptions()
    {
        return $this->exportOptions;
    }


    /**
     * exportInclude
     * @param string $exportInclude
     * @return bool
     */
    public function exportInclude($exportInclude)
    {
        if ($this->exportOptions["customExport"] === false){
            return true;
        }

        if (in_array($exportInclude, $this->exportOptions["groupsToExport"])){
            return true;
        }

        return false;
    }


    /**
     * get Languages
     * @return array
     */
    public function getLanguages()
    {
        return $this->languages;
    }
}