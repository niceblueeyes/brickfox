<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 11.09.18
 * Time: 16:14
 */

namespace bfox\multichannel\classes\controller\export;

use bfox\multichannel\classes\model as Models;

class ProductsUpdates extends ProductsWrapper
{

    /** @var array $configCurrency; */
    protected $configCurrency;


    public function __construct(Models\ProductModel $articleModel,
                                \stdClass $oCurrency)
    {
        $this->configCurrency = $oCurrency;


        /**
         * Begin of the output
         */
        $this->output['ProductExternId']        = $articleModel->oxarticles__oxid;
        $this->output['Active']                 = $articleModel->oxarticles__oxactive;

        $variations = $this->getVariations($articleModel);
        if(count($variations) > 0)
        {
            $this->output['Variations'] = $variations;
        }


    }


    /**
     * get Variations
     * @param Models\ProductModel $articleModel
     * @return array
     */
    private function getVariations(Models\ProductModel $articleModel)
    {
        $adminVariations = $articleModel->getAdminVariants();
        $count = 1;
        $variations = [];

        if (count($variations) === 0)
        {
            $variation = $this->getVariationArray($articleModel);
            $variations['Variation:'.$count] = $variation;
        }

        foreach($adminVariations as $articleVariation)
        {
            $variation = $this->getVariationArray($articleVariation);

            $variations['Variation:'.$count] = $variation;
            $count++;
        }
        return $variations;
    }


    public function getVariationArray(Models\ProductModel $articleVariations)
    {
        $array = [];
        $isNet = false;

        $price = $articleVariations->getPrice();
        if ($price)
        {
            $isNet = $price->isNettoMode();
        }

        $array['VariationExternId'] = $articleVariations->oxarticles__oxid;
        $array['VariationActive']   = $articleVariations->oxarticles__oxactive;
        $array['Available']         = $this->getAvailabilityByStock($articleVariations->oxarticles__oxstock->getRawValue());

        /*
        if($articleVariations->oxarticles__oxstock->value > 20){
            $array['Stock'] = $articleVariations->oxarticles__oxstock->value;
        }else{
            $array['Stock'] = 0;
        }
        */

        $array['Stock'] = $articleVariations->oxarticles__oxstock;

        $currencies = $this->getCurrencies($articleVariations);
        if(count($currencies) > 0)
        {
            $array["Currencies:net:".var_export($isNet, true)] = $currencies;
        }

        return $array;
    }

}