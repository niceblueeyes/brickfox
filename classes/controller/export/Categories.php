<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 21.08.18
 * Time: 15:31
 */

namespace bfox\multichannel\classes\controller\export;

use bfox\multichannel\classes\model as Models;

class Categories extends Wrapper
{
    private $translations;

    public function __construct(Models\CategoryModel $categoryModel, $exportOptions, $languages)
    {
        $this->setExportOptions($exportOptions);
        /*
        d($exportOptions);
        d($categoryModel->oxcategories__oxtitle->value);
        d($languages);
        die();
        */
        $this->languages = $languages;

        $this->output['CategoryId']             = $categoryModel->oxcategories__oxid;
        $this->output['ParentId']               = $categoryModel->oxcategories__oxparentid;
        $this->output['SortOrder']              = $categoryModel->oxcategories__oxsort;

        //Get MultiLanguage Fields.
        $this->getMultilanguageFields($categoryModel);

    }


    /**
     * get Multilanguage Fields
     * @param Models\CategoryModel $categoryModel
     *
     */
    private function getMultilanguageFields(Models\CategoryModel $categoryModel)
    {
        foreach ($this->getLanguages() as $language)
        {

            $languageAbbreviation = $language['abbr'];
            $categoryModel->loadInLang($language['id'], $categoryModel->oxcategories__oxid);

            // get Titles
            $title = $categoryModel->getTitle();
            if (count($title) > 0)
            {
                $this->translations['Translation:lang:'.$languageAbbreviation] = ["Name" => $title];
            }

        }

        //Set collected values

        if(count($this->translations) > 0)
        {
            $this->output['Translations'] = $this->translations;
        }


    }

}