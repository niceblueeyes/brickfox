<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 04.09.18
 * Time: 10:11
 */

namespace bfox\multichannel\classes\controller\export;

use bfox\multichannel\classes\model as Models;
use bfox\multichannel\classes\util as Utils;


class Collect
{
    const CLASS_NAME_PRODUCT      = "ProductModel";
    const CLASS_NAME_CATEGORY     = "CategoryModel";
    const CLASS_NAME_MANUFACTURER = "ManufacturerModel";

    const MODEL_NAME             = "Model";
    const OBJECT_DEFAULT_COUNTER = "num";
    const OBJECT_PRODUCT_COUNTER = "id";


    private $listBaseObject;
    private $objectShortName;
    private $objectCounterAttribute;
    private $classToExport;
    private $listModel;

    public function __construct($exporterSettings, Models\ListModel $listModel, $shp = null)
    {
        $this->exporterSettings       = $exporterSettings;
        $this->listBaseObject         = $exporterSettings["object"];
        $this->classToExport          = $this->getListBaseObjectName();
        $this->objectShortName        = str_replace(self::MODEL_NAME, "", $this->classToExport);
        $this->objectCounterAttribute = self::OBJECT_DEFAULT_COUNTER;

        if ($this->classToExport === self::CLASS_NAME_PRODUCT && $exporterSettings["xmlTagObjectName"] === "Product") {
            $this->objectCounterAttribute = self::OBJECT_PRODUCT_COUNTER;
        }
        $this->listModel = $listModel;
    }

    /**
     * @return Models\ListModel | Models\OrderStatusListModel
     */
    public function getObjectsList()
    {
        $this->setRulesForList();

        if ($this->exporterSettings["onlyUpdates"] === true) {
            if ($this->exporterSettings["customExport"] === true) {
                $configKey      = Utils\ConfigurationKeys::CONFIG_KEY_CUSTOM_EXPORT_LAST_CHANGES;
                $aTablesToCheck = Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_CUSTOM_EXPORT_TABLES_TO_CHECK);
            } else {
                $configKey      = Utils\ConfigurationKeys::CONFIG_KEY_LAST_RUN_PRODUCTS_UPDATE;
                $aTablesToCheck = [];
            }

            $lastUpdateDatetime = $this->getLastRunDate($configKey);
            $this->listModel->getArticleListByLastUpdate($lastUpdateDatetime, $aTablesToCheck);
        } elseif ($this->exporterSettings["onlyStatus"] === true) {
            /** remember: here we have a different type of List Object (Models\OrderStatusListModel) */
            $this->listModel->getOrderStatusList();

        } elseif ($this->exporterSettings["xmlTagObjectName"] === "ProductDelete") {
            $this->listModel->getProductDeleteList();
        } elseif ($this->exporterSettings["fullExport"] === true) {
            #$my = $this->listModel;
            #d($my);
            $this->listModel->getListWithClause(true);
            #d($this);
            #die();
        } else {
            if ($this->exporterSettings["onlyNew"] === true) {
                $today = date_format(date_create(), 'Y-m-d');
                $this->listModel->setAdditionalClauses(Export::OXFIELD_CREATEDATE_NAME, " = '{$today}' ");
            }
            $this->listModel->getListWithClause();
        }
        #d($this->exporterSettings);
        #die();
        #Utils\LogManager::getInstance()->debug(implode(' ', $this->exporterSettings));
        return $this->listModel;
    }


    private function setRulesForList()
    {
        $this->listModel->setBaseObject($this->exporterSettings["object"]);

        $this->listModel->setSqlLimit($this->offset, $this->limit);

        $currentShopId = Utils\OxidRegistry::getActiveShopId();
        $this->listModel->setAdditionalClauses(Export::OXFIELD_SHOPID_NAME, " = " . $currentShopId . " ");

        if ($this->classToExport === self::CLASS_NAME_PRODUCT) {
            $this->listModel->setAdditionalClauses(Export::OXFIELD_PARENTID_NAME, " = '' ");
            $this->setAdditionalWhereFromConfig($this->listModel);
        }

        // AB add Category limits
        if ($this->classToExport === self::CLASS_NAME_CATEGORY) {
            #$this->listModel->setAdditionalClauses('OXHIDDEN', " = 0 ");
            $this->listModel->setAdditionalClauses('OXACTIVE', " = 1 ");
            $this->listModel->setAdditionalClauses('OXTITLE', ' LIKE "Ebay%" ');
        }
    }


    private function setAdditionalWhereFromConfig(Models\ListModel $ListModel)
    {
        $addWhere = Utils\OxidRegistry::getModuleConfig('aArticleAdditionalWhere');

        foreach ($addWhere as $key => $value) {
            $ListModel->setAdditionalClauses($key, $value);
        }
    }

    public function setLimitsForList($offset, $limit)
    {
        $this->offset = $offset;
        $this->limit  = $limit;

    }


    public function getObjectShortName()
    {
        return $this->objectShortName;
    }

    public function getObjectCounterAttribute()
    {
        return $this->objectCounterAttribute;
    }

    public function getClassToExport()
    {
        return $this->classToExport;
    }

    public function getListBaseObjectName()
    {
        $reflection = new \ReflectionClass($this->listBaseObject);
        return $reflection->getShortName();
    }

    public function getExportSettings()
    {
        return $this->exporterSettings;
    }

    private function getLastRunDate($configKey)
    {
        $lastRun = Utils\OxidRegistry::getModuleConfig($configKey);

        if ($lastRun == "" || !$this->validateDate($lastRun, 'Y-m-d H:i:s')) {
            $lastRun = date_create();
            date_add($lastRun, date_interval_create_from_date_string("-12 hour"));
            $lastRun = date_format($lastRun, 'Y-m-d H:i:s');
        }
        return $lastRun;
    }

    private function validateDate($date, $format)
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

}