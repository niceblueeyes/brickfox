<?php

/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 09.08.18
 * Time: 15:21
 */

namespace bfox\multichannel\classes\controller\export;

use bfox\multichannel\classes\controller\export\Xml\ExtendedXmlWriter;
use bfox\multichannel\classes\model as Models;
use bfox\multichannel\classes\util as Utils;
use \OxidEsales\Eshop as Oxid;


class Export extends AbstractExport
{
    const CLASS_NAME_PRODUCT      = "ProductModel";
    const CLASS_NAME_CATEGORY     = "CategoryModel";
    const CLASS_NAME_MANUFACTURER = "ManufacturerModel";

    const MODEL_NAME             = "Model";
    const OBJECT_DEFAULT_COUNTER = "num";
    const OBJECT_PRODUCT_COUNTER = "id";

    const OXFIELD_PARENTID_NAME   = "OXPARENTID";
    const OXFIELD_SHOPID_NAME     = "OXSHOPID";
    const OXFIELD_CREATEDATE_NAME = "OXINSERT";


    /** @var array $listModel */
    private $listModel;

    private $timeOfExport;
    private $fileNumber;

    /** @var array $exporterSettings */
    private $exporterSettings;
    private $languagesArray;


    public function __construct($collectedData, $exporterSettings, $fileDate, $fileNumber = 0, $languagesToExport = null)
    {
        $this->listModel        = $collectedData;
        $this->exporterSettings = $exporterSettings;
        $this->timeOfExport     = $fileDate;
        $this->fileNumber       = $fileNumber;

        $this->languagesArray = $this->setExportLanguages($languagesToExport);
    }


    protected function setExportLanguages($languagesToExport)
    {
        if (isset($languagesToExport)) {
            $languagesToExport = explode("|", $languagesToExport);
        }
        $languages         = [];
        $oxidLanguagesList = Utils\OxidRegistry::getOxidLanguageArray();

        foreach ($oxidLanguagesList as $language) {
            if (in_array($language->oxid, $languagesToExport) || count($languagesToExport) === 0) {
                $languages[] = (array)$language;
            }
        }
        return $languages;
    }


    protected function preExport()
    {
        #d($this->exporterSettings);
        #die();
    }

    protected function mainExport()
    {

        $count = 1;

        $countList = count($this->listModel);

        if ($countList === 0) {
            return false;
        }

        $xml = $this->startXml($this->exporterSettings["storageKey"], $countList, $this->exporterSettings["xmlStartingElement"]);

        foreach ($this->listModel as $key => $ObjectModel) {
            Utils\LogManager::getInstance()->debug($ObjectModel->_sOXID);

            $this->writeToXml($xml,
                array(
                    $this->exporterSettings["xmlTagObjectName"] . ':' . $this->exporterSettings["objectCounterAttribute"] . ':' . $count
                    => $this->{Utils\ConfigurationKeys::CONFIG_KEY_EXPORT_ACTION_NAME . $this->exporterSettings["classToExport"]}($ObjectModel)
                )
            );

            $count++;

            unset($this->listModel[$key]);
        }
        $this->endXml($xml);

        $msg = "Memory state for " . ($count - 1) . " Objects being exported. ";
        Utils\LogManager::getInstance()->debug($msg);

    }

    protected function posExport()
    {

    }


    /**
     * @param $articleModel
     * @return array
     *
     * Attention: some exportSettings are added here
     */
    protected function exportProductModel($articleModel)
    {
        $attributeModel     = new Models\AttributeModel;
        $oCurrency          = Utils\OxidRegistry::getActShopCurrencyObject();
        $languages          = $this->languagesArray;
        $productsVariations = new ProductsVariations($attributeModel, [], $languages, $oCurrency);

        $this->exporterSettings["articleFieldsToAttribute"] = Utils\OxidRegistry::getModuleConfig('arrAttributesArticlesTable');
        $this->exporterSettings["exportImageSizes"]         = Utils\OxidRegistry::getModuleConfig('slctImageType');

        if ($this->exporterSettings["onlyUpdates"] === true && $this->exporterSettings["customExport"] === false) {
            $product = new ProductsUpdates($articleModel, $oCurrency);
        } else {
            $product = new Products($articleModel, $this->exporterSettings, $productsVariations, $attributeModel, $oCurrency, $languages);
        }

        $productArray = $product->toArray();

        $this->writeBrickfoxArticlesExports($productArray);

        $product = null;

        return $productArray;
    }

    protected function exportCategoryModel($categoryModel)
    {
        $category = new Categories($categoryModel, [], $this->languagesArray);

        return $category->toArray();
    }


    protected function exportManufacturerModel($manufacturerModel)
    {
        $manufacturer = new Manufacturers($manufacturerModel, [], $this->languagesArray);

        return $manufacturer->toArray();
    }


    protected function exportOrderStatusModel($orderStatusModel)
    {
        $orderStatus = new OrdersStatus($orderStatusModel, [], $this->languagesArray);

        return $orderStatus->toArray();
    }

    protected function exportProductDeleteModel($productsDeletesModel)
    {
        $productsDeletes = new ProductsDeletes($productsDeletesModel);

        return $productsDeletes->toArray();
    }


    protected function startXml($storageFileName, $countList = 0, $alternateStartElement = "")
    {
        $shopId = Utils\SystemManager::getInstance()->getActiveShopId();

        $xml = new ExtendedXmlWriter($storageFileName, $shopId, $this->timeOfExport, $this->fileNumber);

        if ($xml->getFileIsOpen()) {
            $this->setFileCreated($xml->getFileName());

            $xmlStartingElement = strlen($alternateStartElement) > 0 ? $alternateStartElement : $storageFileName;

            $xml->startElement($xmlStartingElement);
            $xml->writeAttribute('count', $countList);
        }

        return $xml;
    }

    protected function writeToXml(ExtendedXmlWriter $xml, $object)
    {
        if ($xml->getFileIsOpen()) {
            $xml->writeArray($object);
        }
    }

    protected function endXml(ExtendedXmlWriter $xml)
    {
        $xml->endElement();
        $xml->endDocument();
        $xml->flush();
    }

    /**
     * @param array $productArray
     */
    private function writeBrickfoxArticlesExports(array $productArray)
    {
        $query = "INSERT INTO brickfox_articles_exports (id, oxid_id, is_deleted, last_export, export_hash)" .
            " VALUES ('" . Utils\SystemManager::generateUId() . "', '" . $productArray['ProductExternId'] . "', 0, NOW(), '" . md5(json_encode($productArray)) . "')" .
            " ON DUPLICATE KEY UPDATE is_deleted = 0, last_export = NOW(), export_hash = '" . md5(json_encode($productArray)) . "'";

        Oxid\Core\DatabaseProvider::getDb()->execute($query);
    }
}