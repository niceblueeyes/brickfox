<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 24.08.18
 * Time: 12:42
 */

namespace bfox\multichannel\classes\controller\export;


abstract class AbstractExport
{
    private $fileCreated;

    protected abstract function preExport();

    protected abstract function mainExport();

    protected abstract function posExport();

    public function export()
    {
        $this->preExport();
        $this->mainExport();
        $this->posExport();
    }


    public function getFileCreated()
    {
        return $this->fileCreated;
    }

    protected function setFileCreated($filePath)
    {
        $this->fileCreated = $filePath;
    }

}