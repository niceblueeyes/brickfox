<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 24.04.19
 * Time: 12:26
 */

namespace bfox\multichannel\classes\controller\export;

use bfox\multichannel\classes\model as Models;
use bfox\multichannel\classes\util as Utils;
use \OxidEsales\Eshop as Oxid;


class OrdersStatus extends Wrapper
{
    private $statusMapping;

    protected $orderShipped = false;
    protected $orderStorno = false;


    public function __construct(Models\OrderStatusModel $orderStatusModel, $exportOptions, $languages)
    {
        /** @var array $statusMappings */
        $this->statusMapping = Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_ORDERS_ORDER_STATUS_MAPPING);

        $this->setExportOptions($exportOptions);
        $this->languages = $languages;

        $orderId                                    = $this->_getBrickfoxOrderIdByOxOrderId($orderStatusModel->oxorder__oxid);
        $this->output['OrderId']                    = $orderId;
        $this->output['OrderStatusChanged']         = $orderStatusModel->oxorder__oxtimestamp;
        $this->output['OrderStatus']                = $this->getBrickfoxMappedStatus((string)$orderStatusModel->oxorder__oxfolder, "", false);

        $oxSendDate = (string) $orderStatusModel->oxorder__rawsenddate;

        if (($oxSendDate === "" || $oxSendDate === "-" || $oxSendDate === "0000-00-00 00:00:00") === false && strlen($orderStatusModel->oxorder__oxtrackcode) > 0)
        {
            $this->orderShipped = true;

            $this->output['ShippingTrackingId'] = $orderStatusModel->oxorder__oxtrackcode;
            $this->output['CarrierName']        = $orderStatusModel->oxorder__oxdeltype;
        }



        $this->orderStorno = (bool)(string)$orderStatusModel->oxorder__oxstorno;

        $this->updateProcessedOrder($oxSendDate, (string)$orderStatusModel->oxorder__oxfolder, $orderStatusModel->oxorder__id);

        $orderLines = $this->getOrderLinesStatus((string)$orderStatusModel->oxorder__oxid);
        $orderLinesTotal = count($orderLines);
        if($orderLinesTotal > 0)
        {
            $this->output['OrderLines:count:' . $orderLinesTotal] = $orderLines;
        }

    }


    protected function getOrderLinesStatus($orderOxId)
    {
        $orderArticleList = oxNew(Models\OrderArticleListModel::class);
        $orderArticleList->getOrderArticlesFull($orderOxId);

        $orderLines = [];
        $counter = 1;

        $cancelled = 0; $shipped = 0; $returned = 0;


        /** @var $orderArticle Models\OrderArticleModel */
        foreach($orderArticleList as $orderArticle)
        {
            $qttyOrdered = (int) $orderArticle->oxorderarticles__quantity_ordered->rawValue;
            $oxAmount    = (int) $orderArticle->oxorderarticles__oxamount->rawValue;

            if ($this->orderShipped === true)
            {
                $shipped = $qttyOrdered;
            }

            if ($this->orderStorno === true)
            {
                $shipped = 0;
                $cancelled = $qttyOrdered;
            }

            if ($qttyOrdered > $oxAmount)
            {
                $returned = $qttyOrdered - $oxAmount;
            }

            $orderLines['OrderLine:num:' . $counter++] = [
                'OrderLineId'       => $orderArticle->oxorderarticles__orderline_id,
                'OrderLineStatus'   => $this->getBrickfoxMappedStatus((string)$orderArticle->oxorderarticles__orderoxfolder,
                    (string)$orderArticle->oxorderarticles__oxfolder, true),
                'QuantityOrdered'   => $qttyOrdered,
                'QuantityCancelled' => $cancelled,
                'QuantityShipped'   => $shipped,
                'QuantityReturned'  => $returned
            ];

            $this->updateProcessedOrderLine((string)$orderArticle->oxorderarticles__oxfolder, $orderArticle->oxorderarticles__id);
        }

        return $orderLines;
    }


    protected function getBrickfoxMappedStatus($oxOrderFolder, $oxArticleFolder, $isOrderLine)
    {
        $checkArticleFolder = Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_ORDERS_ORDER_STATUS_TYPE);
        $optionArticleLine = Models\OrderStatusListModel::TYPE_ORDER_ARTICLE_CHANGES;

        $oxFolder = ($checkArticleFolder === $optionArticleLine && $isOrderLine === true) ? $oxArticleFolder : $oxOrderFolder;

        if (array_key_exists($oxFolder, $this->statusMapping) === true)
        {
            return $this->statusMapping[$oxFolder];
        }

        return "status not found: " . $oxFolder;
    }


    private function updateProcessedOrder($sendDate, $oxFolder, $bfImportId)
    {
        $sQuery = "UPDATE brickfox_import 
                    SET 
                    import_updatedate = NOW(), 
                    import_senddate = '" . $sendDate . "', 
                    import_oxfolder = '" . $oxFolder . "' 
                    WHERE id = '" . $bfImportId . "'";

        Oxid\Core\DatabaseProvider::getDb()->execute($sQuery);
    }


    private function updateProcessedOrderLine( $oxFolder = "", $bfImportArticleId)
    {
        $sQuery = "UPDATE brickfox_import_articles 
                    SET 
                    import_article_updatedate = NOW(), 
                    import_article_oxfolder = '" . $oxFolder . "' 
                    WHERE id = '" . $bfImportArticleId . "'";

        Oxid\Core\DatabaseProvider::getDb()->execute($sQuery);
    }

    /**
     * returns bf order with given oxorderid
     *
     * @param string oxOrderId
     * @return string
     */
    private function _getBrickfoxOrderIdByOxOrderId($oxOrderId)
    {
        $orderId = '';
        $sQuery  = "SELECT orderid FROM brickfox_import WHERE oxorderid='" . $oxOrderId . "'";
        $oResult = Oxid\Core\DatabaseProvider::getDb()->select($sQuery);

        if ($oResult != false && $oResult->count() == 1)
        {
            $row = $oResult->getFields();

            if ($row[0] != "") {
                $orderId = $row[0];
            }
        }
        else {
            Utils\LogManager::getInstance()->debug("Could not fetch bf orderId for oxorderid: " . $oxOrderId);
        }
        return $orderId;
    }

}