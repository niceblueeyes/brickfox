<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 11.09.18
 * Time: 17:25
 */

namespace bfox\multichannel\classes\controller\export;

use bfox\multichannel\classes\model as Models;

class ProductsWrapper extends Wrapper
{
    /** @var array $configCurrency ; */
    protected $configCurrency;

    /**
     * get Currencies
     *
     * @param Models\ProductModel $articleModel
     * @return array
     */
    protected function getCurrencies(Models\ProductModel $articleModel)
    {
        $currencies = [];
        $currency   = [];
        $isNet      = false;

        $price = $articleModel->getPrice();
        if ($price) {
            $currency["Price"] = $price->getPrice();
            $isNet             = $price->isNettoMode();
        }

        $TPrice = $articleModel->getTPrice();
        if ($TPrice) {
            $currency["Rrp"] = $TPrice->getPrice();
        }

        //Scale Prices (for the moment only gets Scale prices in Variations)
        $scalePrices = $this->getScalePrices($articleModel->getAmountPriceList(), $price->getPrice());
        if (count($scalePrices) > 0) {
            $currency["ScalePrices"] = $scalePrices;
        }

        $currencies["Currency:code:" . $this->configCurrency->name . ":net:" . var_export($isNet, true)] = $currency;

        return $currencies;
    }

    /**
     * @param \bfox\multichannel\classes\model\ProductModel $articleModel
     * @return array
     */
    protected function getPackage(Models\ProductModel $articleModel)
    {
        return [
            "Weight:unit:g"  => $articleModel->getWeight() * 1000,
            "Width:unit:mm"  => $articleModel->getWidth() * 1000,
            "Length:unit:mm" => $articleModel->getLength() * 1000,
            "Height:unit:mm" => $articleModel->getHeight() * 1000
        ];
    }

    protected function getSpecialPrices(Models\ProductModel $articleModel)
    {

    }

    /**
     * @param $amountPrices
     * @param int $unitPrice
     * @return array
     *
     * The calls here for getRawValue() dont need to be checked because
     * the table fields used here have default values in the database
     */
    protected function getScalePrices($amountPrices, $unitPrice = 0)
    {
        $scalePrices = [];
        $count       = 0;

        foreach ($amountPrices as $amountPrice) {
            $scalePrice = [];

            $absolute = $amountPrice->oxprice2article__oxaddabs->getRawValue();
            $percent  = $amountPrice->oxprice2article__oxaddperc->getRawValue();

            if ((float)$absolute > 0) {
                $price = $absolute;
            } else {
                $price = $unitPrice - ($unitPrice * $percent / 100);
            }

            $scalePrice["Quantity"]              = $amountPrice->oxprice2article__oxamount->getRawValue();
            $scalePrice["Price"]                 = $price;
            $scalePrices["ScalePrice:" . $count] = $scalePrice;
            $count++;
        }

        return $scalePrices;
    }

    /**
     * @param $oxStock
     * @return string
     */
    protected function getAvailabilityByStock($oxStock)
    {
        if ($oxStock > 0) {
            return "1";
        }

        return "0";
    }
}