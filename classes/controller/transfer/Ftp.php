<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 27.08.18
 * Time: 10:30
 */

namespace bfox\multichannel\classes\controller\transfer;



class Ftp
{

    /**
     * host name
     *
     * @var string
     */
    private $host;

    /**
     * user name
     *
     * @var string
     */
    private $user;

    /**
     * password
     *
     * @var string
     */
    private $password;

    /**
     * directory
     *
     * @var string
     */
    private $directory;

    /**
     * passive mode
     *
     * @var boolean
     */
    private $passive = true;

    /**
     * port
     *
     * @var integer
     */
    private $port = 21;

    /**
     * ssl mode
     *
     * @var boolean
     */
    private $ssl = false;

    /**
     * error
     *
     * @var string
     */
    public $error;

    /**
     * connection id
     *
     * @var resource
     */
    private $connectionId;

    /**
     * login success
     *
     * @var bool
     */
    private $loginSuccess = false;




    /**
     * Constructor
     * sets params
     *
     * @param string $host
     * @param string $user
     * @param string $password
     * @param string $directory
     * @param integer $port
     * @param boolean $passive
     * @param boolean $ssl
     */
    public function __construct($host, $user, $password, $directory = null, $port = null, $passive = null, $ssl = false)
    {
        $this->host     = $host;
        $this->user     = $user;
        $this->password = $password;

        if(true === $ssl || 'true' === $ssl)
        {
            $this->ssl = true;
        }
        else
        {
            $this->ssl = false;
        }

        if(!empty($directory))
        {
            $this->directory = $directory;
        }

        if(null != $port && (int) $port > 0)
        {
            $this->port = (int) $port;
        }

        if(true === $passive || 'true' === $passive)
        {
            $this->passive= true;
        }
        else
        {
            $this->passive= false;
        }


        $this->init();
    }

    /**
     * Destructor
     */
    public function __destruct()
    {
        $this->close();
    }

    /**
     * isConnected.
     */
    public function isConnected()
    {
        return !empty($this->connectionId);
    }

    /**
     * init.
     * calls:
     * connect()
     * login()
     * setPassiveMode()
     * [changeDirectory() optional]
     */
    public function init()
    {
        $this->connectionId = $this->connect();

        if($this->connectionId)
        {
            $this->setLoginSuccess($this->login());
            $this->setPassiveMode($this->passive);

            if(empty($this->directory) === false)
            {
                $this->changeDirectory($this->directory);
            }
        }
    }

    /**
     * FTP-connect
     *
     * @return mixed resource
     */
    public function connect()
    {
        if(empty($this->host))
        {
            $this->error = 'FTP-Host nicht konfiguriert';
        }
        else
        {

            if(true !== $this->ssl)
            {
                $connectionId = @ftp_connect($this->host, $this->port);
            }
            else
            {
                $connectionId = @ftp_ssl_connect($this->host, $this->port);
            }

            if($connectionId)
            {
                $this->connectionId = $connectionId;
                return $connectionId;
            }
            else
            {
                $this->error = 'FTP-Verbindung zu ' . $this->host . ' fehlgeschlagen.';

            }
        }
        return false;
    }

    /**
     * FTP-login
     *
     * @return boolean
     */
    public function login()
    {
        $success = false;
        $login   = @ftp_login($this->connectionId, $this->user, $this->password);
        if($login)
        {
            $success = true;
        }
        else
        {
            $this->error = 'Login ' . $this->user . '@' . $this->host . ' fehlgeschlagen.';
        }

        return $success;
    }

    /**
     * close
     */
    public function close()
    {
        if(!empty($this->connectionId))
        {
            @ftp_close($this->connectionId);
            $this->connectionId = null;
        }
    }

    /**
     * FTP-changeDir
     *
     * @param string $directory
     * @return boolean
     */
    public function changeDirectory($directory)
    {
        $chDir = @ftp_chdir($this->connectionId, $directory);
        if($chDir)
        {
            return true;
        }
        else
        {
            echo $this->error = 'konnte nicht in Verzeichnis ' . $directory . ' wechseln.';

            return false;
        }
    }

    /**
     * set passive Mode
     *
     * @param boolean $passive
     */
    public function setPassiveMode($passive)
    {
        @ftp_pasv($this->connectionId, $passive);
    }

    /**
     * gets remote file
     *
     * @param string $localFile
     * @param string $remoteFile
     * @return boolean
     */
    public function getFile($localFile, $remoteFile)
    {
        $mode = FTP_BINARY;

        $success = false;
        $get     = @ftp_get($this->connectionId, $localFile, $remoteFile, $mode);
        if($get)
        {
            $success = true;
        }
        else
        {
            $this->error = 'Datei ' . $remoteFile . ' konnte nicht herunter geladen werden.';
        }

        return $success;
    }

    /**
     * puts remote file
     *
     * @param string $remoteFile
     * @param string $localFile
     * @param string $directory
     * @return bool
     */
    public function putFile($remoteFile, $localFile, $directory = null)
    {
        $success = false;

        if(is_null($directory) === false)
        {
            $this->createPath($directory, '');
        }

        if($this->connectionId && (ftp_put($this->connectionId, $remoteFile, $localFile, FTP_BINARY) === true))
        {
            $success = true;
        }
        else
        {
            $this->error = 'Datei ' . $remoteFile . ' konnte nicht hoch geladen werden.';
        }

        return $success;
    }

    /**
     * renames a file
     *
     * @param string $oldName
     * @param string $newName
     * @return boolean
     */
    public function renameFile($oldName, $newName)
    {
        if(($this->fileExists($oldName)) && (!$this->fileExists($newName)))
        {
            return @ftp_rename($this->connectionId, $oldName, $newName);
        }

        return false;
    }

    /**
     * gets remote filesize
     *
     * @param string $file
     * @return integer file size
     */
    public function getFileSize($file)
    {
        return @ftp_size($this->connectionId, $file);
    }

    /**
     * fileExists.
     *
     * @param string $file
     * @return boolean
     */
    public function fileExists($file)
    {
        return $this->getFileSize($file) != -1;
    }

    /**
     * gets remote mtime
     *
     * @param string $file
     * @return integer time
     */
    public function getFileMtime($file)
    {
        return @ftp_mdtm($this->connectionId, $file);
    }

    /**
     * deletes remote file
     *
     * @param string $file
     * @return boolean
     */
    public function deleteFile($file)
    {
        $success = true;

        if($this->fileExists($file) === true)
        {
            $success = @ftp_delete($this->connectionId, $file);

            if($success === false)
            {
                $this->error = 'Datei ' . $file . ' konnte nicht gelöscht werden.';
            }
        }

        return $success;
    }

    /**
     * returns current directory name
     *
     * @return string
     */
    public function getCurrentDir()
    {
        return ftp_pwd($this->connectionId);
    }

    /**
     * list remote directory
     *
     * @param string $dir
     * @return array directory list
     */
    public function listDir($dir)
    {
        $return = array();

        if($this->connectionId)
        {
            $return = @ftp_nlist($this->connectionId, $dir);
        }

        return $return;
    }

    /**
     * is dir
     *
     * @param string $resourcePath
     * @return bool
     */
    function isDir($resourcePath)
    {
        $isDirectory = false;
        $origin      = $this->getCurrentDir();

        if(@ftp_chdir($this->connectionId, basename($resourcePath)))
        {
            ftp_chdir($this->connectionId, $origin);

            $isDirectory = true;
        }

        return $isDirectory;
    }

    /**
     * makeDir.
     *
     * @param string $dir
     * @return boolean
     */
    public function makeDir($dir)
    {
        $mkDir   = @ftp_mkdir($this->connectionId, $dir);
        $success = false;

        if($mkDir)
        {
            $success = true;
        }
        else
        {
            $this->error = 'Verzeichnis ' . $dir . ' konnte nicht angelegt werden.';
        }

        return $success;
    }

    /**
     * createPath.
     *
     * @param string $path
     * @param string $root
     */
    public function createPath($path, $root)
    {
        if($this->getLoginSuccess() === true)
        {
            if(!empty($root))
            {
                $tmpPath = str_replace($root, '', $path);

                if(false != $tmpPath && strlen($tmpPath) > 0)
                {
                    $path = $tmpPath;
                }
                $this->changeDirectory($root);
            }
            $parts = explode(DIRECTORY_SEPARATOR, trim($path, DIRECTORY_SEPARATOR));

            foreach($parts as $directory)
            {
                $dirs = $this->listDir("");

                if(!in_array($directory, $dirs, true))
                {
                    $this->makeDir($directory);
                }
                $this->changeDirectory($directory);
            }
        }
    }

    /**
     * LoginSuccess
     *
     * @param boolean $loginSuccess
     * @return Ftp
     */
    private function setLoginSuccess($loginSuccess)
    {
        $this->loginSuccess = $loginSuccess;

        return $this;
    }

    /**
     * LoginSuccess
     *
     * @return boolean
     */
    public function getLoginSuccess()
    {
        return $this->loginSuccess;
    }

    /**
     * @param string $directory
     * @return array
     */
    public function getRawList($directory = '.')
    {
        return @ftp_rawlist($this->connectionId, $directory);
    }

    /**
     * @param string $cmd
     * @return array
     */
    public function ftpRaw($cmd)
    {
        return @ftp_raw($this->connectionId, $cmd);
    }

}