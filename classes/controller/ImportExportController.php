<?php

/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 01.08.18
 * Time: 11:40
 */

namespace bfox\multichannel\classes\controller;

use bfox\multichannel\classes\controller\export as Exports;
use bfox\multichannel\classes\controller\import\Orders;
use bfox\multichannel\classes\model as Models;
use bfox\multichannel\classes\util as Utils;
use \OxidEsales\Eshop as Oxid;


class ImportExportController
{
    private $filesCreated = [];
    private $exportSettings = [];
    private $languagesToExport;
    private $processingTime;
    private $shp = null;

    /** @var Utils\ScriptManager $_scriptManager */
    private $_scriptManager = null;


    public function __construct()
    {
        $oxidShp = oxNew(Oxid\Core\Request::class)->getRequestParameter("shp");

        if ($oxidShp !== null && $oxidShp > 0) {
            Utils\LogManager::getInstance()->debug("Export/Import with shp parameter predefined.");
            $this->shp = $oxidShp;
        }

        $hangingTime          = Utils\OxidRegistry::getOxidConfig(Utils\ConfigurationKeys::CONFIG_KEY_SL_CLEANUP_HANGING_IN_SECONDS);
        $removalTime          = Utils\OxidRegistry::getOxidConfig(Utils\ConfigurationKeys::CONFIG_KEY_SL_CLEANUP_REMOVAL_IN_DAYS);
        $this->_scriptManager = oxNew(Utils\ScriptManager::class, $hangingTime, $removalTime);
    }

    /**
     *  Settings
     *
     *  Most of the exportSettings are defined here. Additional settings are defined in
     * the Export class in the exportProductModel method.
     */

    public function collectOxidArticles()
    {
        $this->collectionExport(
            [
                "xmlTagObjectName" => "Product",
                "object"           => oxNew(Models\ProductModel::class),
                "splitFiles"       => true,
                "onlyUpdates"      => false,
                "onlyNew"          => false,
                "customExport"     => false,
                "fullExport"       => true,
                "storageKey"       => Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_STORAGE_FILENAME_PRODUCTS)
            ]
        );
    }

    public function collectOxidArticlesCreatedSince()
    {
        $this->collectionExport(
            [
                "xmlTagObjectName" => "Product",
                "object"           => oxNew(Models\ProductModel::class),
                "splitFiles"       => false,
                "onlyUpdates"      => false,
                "onlyNew"          => true,
                "customExport"     => false,
                "storageKey"       => Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_STORAGE_FILENAME_PRODUCTS)
            ]
        );
    }

    public function collectOxidArticlesCustomized()
    {
        $this->collectionExport(
            [
                "xmlTagObjectName" => "Product",
                "object"           => oxNew(Models\ProductModel::class),
                "splitFiles"       => true,
                "onlyUpdates"      => true,
                "onlyNew"          => false,
                "customExport"     => true,
                "storageKey"       => Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_STORAGE_FILENAME_PRODUCTS)
            ]
        );
    }

    public function collectOxidArticlesStockAndPrices()
    {
        $this->collectionExport(
            [
                "xmlTagObjectName"   => "ProductUpdate",
                "object"             => oxNew(Models\ProductModel::class),
                "splitFiles"         => true,
                "onlyUpdates"        => true,
                "onlyNew"            => false,
                "customExport"       => false,
                "storageKey"         => Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_STORAGE_FILENAME_PRODUCTS_UPDATE),
                "xmlStartingElement" => "Products",
            ]
        );
    }

    public function collectOxidCategories()
    {
        $this->collectionExport(
            [
                "xmlTagObjectName" => "Category",
                "object"           => oxNew(Models\CategoryModel::class),
                "splitFiles"       => false,
                "onlyUpdates"      => false,
                "fullExport"       => true,
                "storageKey"       => Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_STORAGE_FILENAME_CATEGORIES)
            ]
        );
    }

    public function collectOxidManufacturers()
    {
        $this->collectionExport(
            [
                "xmlTagObjectName" => "Manufacturer",
                "object"           => oxNew(Models\ManufacturerModel::class),
                "splitFiles"       => false,
                "onlyUpdates"      => false,
                "fullExport"       => true,
                "storageKey"       => Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_STORAGE_FILENAME_MANUFACTURERS)
            ]
        );
    }

    public function collectOxidOrdersStatus()
    {
        $this->collectionExport(
            [
                "xmlTagObjectName"   => "Order",
                "object"             => oxNew(Models\OrderStatusModel::class),
                "splitFiles"         => false,
                "onlyUpdates"        => false,
                "onlyStatus"         => true,
                "customList"         => oxNew(Models\OrderStatusListModel::class),
                "storageKey"         => Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_STORAGE_FILENAME_ORDERSSTATUS),
                "xmlStartingElement" => "Orders",
            ]
        );

    }

    public function collectOxidProductsDeletes()
    {
        $this->collectionExport(
            [
                "xmlTagObjectName"   => "ProductDelete",
                "object"             => oxNew(Models\ProductDeleteModel::class),
                "splitFiles"         => true,
                "onlyUpdates"        => false,
                "onlyNew"            => false,
                "customExport"       => false,
                "customList"         => oxNew(Models\ProductDeleteListModel::class),
                "storageKey"         => Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_STORAGE_FILENAME_PRODUCTS_DELETES),
                "xmlStartingElement" => "Products",
            ]
        );
    }

    public function importBrickfoxOrders()
    {
        $orderImportSettings = oxNew(ImportExportSettings::class,
            ImportExportSettings::SETTING_NAME_ORDERS,
            Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_STORAGE_FILENAME_ORDERS),
            ImportExportSettings::SETTING_NAME_NAMESPACE_IMPORT);

        $orderImportSettingsCopy = $orderImportSettings;

        $orderImportSettings->addExecutionDependency($orderImportSettingsCopy);

        $this->collectionImport(
            [
                "xmlTagObjectName"     => "Order",
                "object"               => oxNew(Models\OrderModel::class),
                "filenameBase"         => [Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_STORAGE_FILENAME_ORDERS)],
                "scriptLoggerSettings" => $orderImportSettings
            ]
        );
    }

    /** ############################################################################################################ */
    /** Export */

    /**
     * collectionExport.
     * @param array $exportSettings
     */
    private function collectionExport($exportSettings)
    {
        $this->languagesToExport = Utils\OxidRegistry::getUrlParameter('lang');

        $this->exportSettings = $exportSettings;
        $exportSubdirectories = Utils\FileManager::getInstance()->getValidExchangeSubdirectories($this->shp);

        // valid export subdirectory = oxshopid
        foreach ($exportSubdirectories as $oxShopId) {
            Utils\OxidRegistry::setActiveShopId($oxShopId);

            $this->handleExport();
        }

    }


    private function handleExport()
    {
        Utils\LogManager::getInstance()->debug('Start exporting ' . $this->exportSettings["xmlTagObjectName"]);

        $startPage = Utils\OxidRegistry::getUrlParameter('startPage');
        $pages     = Utils\OxidRegistry::getUrlParameter('pages');
        $limit     = Utils\OxidRegistry::getUrlParameter('limit');

        if (is_null($pages) || empty($pages)) {
            $pages = 10000;
        }

        if (is_null($limit) || empty($limit)) {
            $limit = 100;
        }

        if (!(is_null($startPage) || empty($startPage))) {
            $offset     = ($startPage - 1) * $limit;
            $fileNumber = $startPage - 1;
        } else {
            $offset     = 0;
            $fileNumber = 0;
        }

        $this->processingTime = date('Y-m-d H:i:s');
        $fileDate             = date("Ymd_His");

        Utils\LogManager::getInstance()->debug("Preparing to export " . $this->exportSettings["xmlTagObjectName"]);

        do {
            $collected = null;

            $listModel = $this->returnCollectListObject();

            /** @var Models\ListModel $collected */
            $collected = $this->collectList($offset, $limit, $listModel);
            $offset    += $limit;
            $fileNumber++;
            $pages--;
            gc_collect_cycles();
        } while ($pages >= 0 && $this->exportList($collected, $fileDate, $fileNumber) === true);

        if ($this->exportSettings["onlyUpdates"] === true) {
            $configKey = ($this->exportSettings["customExport"]) ? Utils\ConfigurationKeys::CONFIG_KEY_CUSTOM_EXPORT_LAST_CHANGES : Utils\ConfigurationKeys::CONFIG_KEY_LAST_RUN_PRODUCTS_UPDATE;

            Utils\OxidRegistry::saveModuleConfig($configKey, $this->processingTime, 'str');
        }

        $this->sendFlagForAutoconnect();
        $this->sendFilesInQueue();

        Utils\LogManager::getInstance()->debug('End exporting ' . $this->exportSettings["xmlTagObjectName"]);

    }


    private function returnCollectListObject()
    {
        if (in_array("customList", $this->exportSettings) === true) {
            return $this->exportSettings["customList"];
        }

        return null;
    }


    private function sendFilesInQueue()
    {
        Utils\LogManager::getInstance()->debug('Sending files to client FTP...');

        foreach ($this->filesCreated as $exportFile) {
            $fileSent = $this->sendXmlToFtp($exportFile);

            if ($fileSent === true) {
                unlink($exportFile);
            }
            array_pop($this->filesCreated);
        }
    }

    /**
     * reply for cUrl loop
     */
    private function sendFlagForAutoconnect()
    {
        if (count($this->filesCreated) > 0) {
            echo Utils\ConfigurationKeys::AUTOCONNECT_KEY_MORE_FILES;
        } else {
            echo Utils\ConfigurationKeys::AUTOCONNECT_KEY_END_LOOP;
        }
    }


    /**
     * @param $offset
     * @param $limit
     * @param $listModel
     * @return array
     */
    private function collectList($offset, $limit, $listModel = null)
    {
        if ($listModel === null) {
            $listModel = oxNew(Models\ListModel::class);
        }

        /** @var Exports\Collect $collector */
        $collector = new Exports\Collect($this->exportSettings, $listModel, $this->shp);

        if ($this->exportSettings["splitFiles"] === true) {
            $collector->setLimitsForList($offset, $limit);
        }

        $this->exportSettings["objectShortName"]        = $collector->getObjectShortName();
        $this->exportSettings["objectCounterAttribute"] = $collector->getObjectCounterAttribute();
        $this->exportSettings["classToExport"]          = $collector->getClassToExport();
        $this->exportSettings["thirdPartyStockConfig"]  = $this->getThirdPartyTagConfig();


        return $collector->getObjectsList()->getArray();
    }

    /**
     * @param $collectedData
     * @param $fileDate
     * @param $fileNumber
     * @return bool
     */
    private function exportList($collectedData, $fileDate, $fileNumber)
    {
        $result = false;

        $this->exportSettings["groupsToExport"] = Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_CUSTOM_EXPORT_GROUPS_TO_EXPORT);

        /** @var Exports\Export $exporter */
        $exporter = new Exports\Export($collectedData, $this->exportSettings, $fileDate, $fileNumber, $this->languagesToExport);
        $exporter->export();

        if (strlen($exporter->getFileCreated()) > 0) {
            array_push($this->filesCreated, $exporter->getFileCreated());

            if ($this->exportSettings["splitFiles"] === true) {
                $result = true;
            }
        }

        $collector = null;
        $exporter  = null;
        unset($collector);
        unset($exporter);


        return $result;
    }

    private function getThirdPartyTagConfig()
    {
        $config = [];

        $config["ThirdPartyStock"]   = Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_THIRD_PARTY_FLAG);
        $config["ThirdPartyMapFrom"] = Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_THIRD_PARTY_MAP_FROM);
        $config["ThirdPartyValue"]   = Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_THIRD_PARTY_MAP_AS);

        return $config;
    }


    /** ############################################################################################################ */
    /** Import */

    /**
     * collectionImport.
     *
     * @param array $importerSettings settings, see below
     * @return bool
     */
    private function collectionImport($importerSettings)
    {
        /** @var transfer\Ftp $ftp */
        $ftp = $this->connectToFtp();

        if ($ftp === false) {
            return false;
        }

        /*
        echo $ftp->getCurrentDir();
        print_r($ftp->listDir('Orders*.xml'));
        die();
        */
        try {
            if (is_array($fileList = $ftp->listDir('Orders*.xml'))) {
                asort($fileList);

                $exchangeDir = Utils\FileManager::getInstance()->getExchangeShopDirectoryLocation();

                foreach ($fileList as $file) {
                    $success = $this->getXmlFromFtp($ftp, $file, $exchangeDir . $file);
                    $ftp->deleteFile($file);
                }
            }
        } catch (\Exception $exception) {
            Utils\LogManager::getInstance()->debug("Error retrieving Orders files. " . $exception->getMessage());
        } finally {
            $ftp->close();
        }


        $exportSubdirectories = Utils\FileManager::getInstance()->getValidExchangeSubdirectories($this->shp);

        /*
        print_r($exportSubdirectories);
        die();
        */

        // valid export subdirectories
        foreach ($exportSubdirectories as $oxShopId) {
            Utils\OxidRegistry::setActiveShopId($oxShopId);

            $this->handleImport($importerSettings);
        }


        return true;
    }


    private function handleImport($importerSettings)
    {
        $aFiles = Utils\FileManager::getInstance()->getImportFiles($importerSettings["filenameBase"]);
        $iCount = 0;

        $scriptLoggerManager = $this->_scriptManager;
        /** @var ImportExportSettings $scriptLoggerSettings */
        $scriptLoggerSettings = $importerSettings["scriptLoggerSettings"];

        // throws an exception is case that script is already running
        $scriptLoggerManager->check($scriptLoggerSettings->getPreparedScriptLoggerDependencies());

        // insert script logger db entry - prevent a second execution
        $scriptLoggerManager->start($scriptLoggerSettings->getName());

        /*
        print_r($importerSettings["filenameBase"]);
        die();
        */
        do {
            foreach ($aFiles as $importXmlFile) {
                //process file
                $importOrder = oxNew(Orders::class, Utils\OxidRegistry::getActiveShopId(), $importXmlFile);

                //delete processed file
                Utils\FileManager::getInstance()->deleteImportFile($importXmlFile);
            }

            $aFiles = Utils\FileManager::getInstance()->getImportFiles($importerSettings["filenameBase"]);
            $iCount++;
        } while (count($aFiles) > 0 && $iCount < 50);

        // update script logger db entries - so scripts can be executed again
        $scriptLoggerManager->end();

    }

    /** ############################################################################################################ */
    /** Ftp */

    /**
     * @param bool $isExport
     * @return \bfox\multichannel\classes\controller\transfer\Ftp|bool
     * @throws \bfox\multichannel\classes\exception\ConfigurationException
     */
    public function connectToFtp($isExport = false)
    {
        $ftpHost = Utils\OxidRegistry::getModuleConfig('strbrickfoxFtpHost');
        $ftpUser = Utils\OxidRegistry::getModuleConfig('strbrickfoxFtpUsername');
        $ftpPass = Utils\OxidRegistry::getModuleConfig('strbrickfoxFtpPassword');
        $ftpPort = Utils\OxidRegistry::getModuleConfig('intbrickfoxFtpPort');

        if ($isExport === true) {
            $ftpRemoteFolder = Utils\OxidRegistry::getModuleConfig('strbrickfoxFtpFolder');
        } else {
            $ftpRemoteFolder = 'outgoing';
        }


        if (strlen($ftpHost) > 0 && strlen($ftpUser) > 0 && strlen($ftpPass) > 0) {
            /** @var transfer\Ftp $oFtp */
            $oFtp = oxNew(transfer\Ftp::class, $ftpHost, $ftpUser, $ftpPass, $ftpRemoteFolder, $ftpPort, true, false);

            if ($oFtp->isConnected() === true) {
                return $oFtp;
            }
        }

        $msg = "Connection to FTP failed, check access data!";
        Utils\LogManager::getInstance()->debug($msg);
        return false;
    }


    /**
     * @param $filePath
     * @return bool
     */
    private function sendXmlToFtp($filePath)
    {
        if (file_exists($filePath) === false) {
            $msg = "File does not exists! File name: " . $filePath;
            Utils\LogManager::getInstance()->debug($msg);
            return false;
        }

        /** @var transfer\Ftp $ftp */
        $ftp = $this->connectToFtp(true);

        if ($ftp === false) {
            return false;
        }

        $sent  = $ftp->putFile(basename($filePath), $filePath);
        $error = $ftp->error;
        if (strlen($error) > 0) {
            Utils\LogManager::getInstance()->debug($error);
        }

        if ($sent === true) {
            $msg = "File sent successfully to FTP. FileName: " . basename($filePath);
            Utils\LogManager::getInstance()->debug($msg);
            return true;
        }

        $msg = "Problem sending file to brickfox FTP, check access data. FileName: " . basename($filePath);
        Utils\LogManager::getInstance()->debug($msg);
        return false;
    }


    private function getXmlFromFtp($ftp, $remoteFile, $localFile)
    {
        /** @var transfer\Ftp $ftp */

        $received = $ftp->getFile($localFile, $remoteFile);
        $error    = $ftp->error;

        if ($error) {
            return false;
        }

        return $received;
    }

}