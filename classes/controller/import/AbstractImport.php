<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 24.08.18
 * Time: 13:01
 */

namespace bfox\multichannel\classes\controller\import;


abstract class AbstractImport
{
    
    protected abstract function preImport();

    protected abstract function mainImport();

    protected abstract function posImport();

    public function import()
    {
        $this->preImport();
        $this->mainImport();
        $this->posImport();
    }

}