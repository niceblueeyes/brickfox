<?php

/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 23.08.18
 * Time: 15:32
 */

namespace bfox\multichannel\classes\controller\import;

use bfox\multichannel\classes\exception\ConfigurationException;
use bfox\multichannel\classes\model as Models;
use bfox\multichannel\classes\util as Utils;
use DomDocument;
use OxidEsales\Eshop as Oxid;
use OxidEsales\Eshop\Core\Database\Adapter\DatabaseInterface;
use OxidEsales\Eshop\Core\Registry;
use SimpleXMLElement;


class Orders
{
    /**
     * @var string
     */
    private $_strShopOXID = null;

    /**
     * @var string
     */
    private $_sImportFile = null;

    /**
     * @var int
     */
    private $_iOrderCount = 0;

    /**
     * @var int
     */
    private $_iArticleCount = 0;

    /**
     * @var array
     */
    private $_reportData = [
        'Totals'  => 0,
        'Process' => 0
    ];

    /**
     * @var array
     */
    private $_aFailedOrders = [];

    /**
     * @var string
     */
    private $_sOrderStatus = '';

    private $_logManager;

    /**
     * @var string
     */
    protected $_ordernr = '';

    /**
     * @param $value
     */
    public function setOrderNr($value){
        $this->_ordernr = $value;
    }

    /**
     * @return string
     * @throws ConfigurationException
     */
    public function getSavedOrderNr(){
        return $this->_ordernr;
    }

    /**
     * @param string $strShopOXID
     * @param string $sFile
     * @param Utils\LogManager $logManager
     * @throws ConfigurationException
     */
    public function __construct($strShopOXID = null, $sFile = null, $logManager = null)
    {
        if (is_null($logManager)) {
            $this->_logManager = Utils\LogManager::getInstance();
        } else {
            $this->_logManager = $logManager;
        }

        if (!is_null($strShopOXID)) {
            $this->_strShopOXID = $strShopOXID;
        } else {
            $this->_reportData['Error'] = 'No Shop to work on!';

            return;
        }

        if (!$sFile) {
            $this->_reportData['Error'] = 'No file to import!';
        } else {
            $this->_sImportFile = $sFile;

            if (($xml = simplexml_load_file($this->_sImportFile)) == true) {
                // add total count to report
                $this->_reportData['Totals'] = (string)$xml->attributes()->count;

                foreach ($xml->xpath('Order') as $aOrderXML) {
                    if (!$sUserId = $this->_createUser($aOrderXML->BillingParty, (string)$aOrderXML->SalesChannelName)) {
                        $this->_aFailedOrders[(int)$aOrderXML->OrderId] = 'Could not save user';
                        continue;
                    }

                    if ($this->_checkOrderAlreadyImported((string)$aOrderXML->ExternOrderId, $this->_strShopOXID)) {
                        $this->_aFailedOrders[(int)$aOrderXML->OrderId] = 'Order already imported';
                        continue;
                    }

                    if (count($aOrderXML->xpath('OrderLines')) == 0) {
                        $this->_aFailedOrders[(int)$aOrderXML->OrderId] = 'Orderlines missing in XML';
                        continue;
                    }

                    if (($sOrderId = $this->_createOrder($aOrderXML, $sUserId)) != false) {
                        $this->_reportData['Process'] += 1;

                        // read order products
                        foreach ($aOrderXML->OrderLines->OrderLine as $aOrderedProduct) {
                            $this->_createOrderProduct($aOrderedProduct, $sOrderId);
                        }

                        // erstelle order articles xml Datei
                        $orderArticles = oxNew(\Bender\dre_xmlsync\models\orderarticles2xml::class);
                        $orderArticles->setOrderId($sOrderId);
                        $orderArticles->setOrderNr($this->getSavedOrderNr());
                        $orderArticles->setOrderArticleIds();
                        $orderArticles->setOrderArticles();
                        $orderArticles->setOrderArticleXmlArray();
                        $orderArticles->erstelleOrderArticlesXmlDatei();

                    } else {
                        $this->_aFailedOrders[(int)$aOrderXML->OrderId] = 'Order could not be saved';
                    }
                }
            } else {
                $this->_reportData['Error'] = 'Invalid XML in orderfile!';
            }
        }


        $this->_logManager->debug($this->_iOrderCount . ' orders with ' . $this->_iArticleCount . ' articles imported for shop-id');
        $this->_logManager->setLastRunDate(Utils\ConfigurationKeys::CONFIG_KEY_ORDERS_LAST_RUN);
    }

    /**
     * report array for xml export of report
     *
     * @return string
     */
    public function getReport()
    {
        $obj_dom  = new DomDocument ();
        $rootNode = $obj_dom->createElement('Orders');
        $obj_node = $obj_dom->createElement('Totals', $this->_reportData['Totals']);
        $rootNode->appendChild($obj_node);
        $obj_node = $obj_dom->createElement('Process', $this->_reportData['Process']);
        $rootNode->appendChild($obj_node);
        if (!empty($this->_reportData['Error'])) {
            $obj_node = $obj_dom->createElement('Error', $this->_reportData['Error']);
            $rootNode->appendChild($obj_node);
        }
        foreach ($this->_aFailedOrders as $orderId => $message) {
            $OrderNode = $obj_dom->createElement('Order');
            $obj_node  = $obj_dom->createElement('OrderId', $orderId);
            $OrderNode->appendChild($obj_node);
            $obj_node = $obj_dom->createElement('ErrorMessage', $message);
            $OrderNode->appendChild($obj_node);
            $rootNode->appendChild($OrderNode);
        }
        $obj_dom->appendChild($rootNode);
        $str_xml = $obj_dom->saveXML();
        unset ($obj_dom);

        simplexml_load_string($str_xml);

        return $str_xml;
    }

    /**
     * creates a ordered_articles entry in oxdb
     *
     * @param $ItemData
     * @param $sOrderId
     * @return mixed
     * @throws ConfigurationException
     */
    private function _createOrderProduct($ItemData, $sOrderId)
    {
        $oArticle = oxNew(Models\ProductModel::class);
        if (!$oArticle->load((string)$ItemData->ExternProductId)) {
            Utils\LogManager::getInstance()->debug("Could not Load Order Article. Check the ExternProductId: " . $ItemData->ExternProductId);
            return false;
        }

        $oOrderArticle = oxNew(Models\OrderArticleModel::class);
        $oOrderArticle->getConfig()->setShopId($this->_strShopOXID);

        $oOrderArticle->oxorderarticles__oxorderid->value     = $sOrderId;
        $oOrderArticle->oxorderarticles__oxamount->value      = (int)$ItemData->QuantityOrdered;
        $oOrderArticle->oxorderarticles__oxartid->value       = Utils\SystemManager::utf8ToLocal((string)$ItemData->ExternProductId);
        $oOrderArticle->oxorderarticles__oxartnum->value      = Utils\SystemManager::utf8ToLocal((string)$ItemData->ItemNumber);
        $oOrderArticle->oxorderarticles__oxtitle->value       = Utils\SystemManager::utf8ToLocal((string)$ItemData->ProductName);
        $oOrderArticle->oxorderarticles__oxselvariant->value  = Utils\SystemManager::utf8ToLocal((string)$ItemData->ProductsVariationsName);
        $oOrderArticle->oxorderarticles__oxnetprice->value    = (float)$ItemData->ProductsPriceTotalNetto;
        $oOrderArticle->oxorderarticles__oxbrutprice->value   = (float)$ItemData->ProductsPriceTotal;
        $oOrderArticle->oxorderarticles__oxvatprice->value    = (float)$ItemData->VatPriceTotal;
        $oOrderArticle->oxorderarticles__oxvat->value         = (float)$ItemData->TaxRate;
        $oOrderArticle->oxorderarticles__oxprice->value       = (float)$ItemData->ProductsPrice;
        $oOrderArticle->oxorderarticles__oxbprice->value      = (float)$ItemData->ProductsPrice;
        $oOrderArticle->oxorderarticles__oxnprice->value      = (float)$ItemData->ProductsPriceNetto;
        $oOrderArticle->oxorderarticles__oxordershopid->value = $this->_strShopOXID;

        if ($oOrderArticle->save()) {
            $this->_iArticleCount++;
            $this->_writeOrderArticleImport($oOrderArticle->getId(), (int)$ItemData->OrderLineId, (string)$ItemData->ExternProductId, $sOrderId, (int)$ItemData->QuantityOrdered);

            return $oOrderArticle->getId();
        }

        return false;
    }

    /**
     * creates a user entry in oxdb
     *
     * @param SimpleXMLElement
     * @param string
     * @return string on success, false otherwise
     */
    private function _createUser($ItemData, $sSalesChannelName)
    {
        try{
            $oUser = oxNew(Models\UserModel::class);
            $oUser->getConfig()->setShopId($this->_strShopOXID);

            // use e-mail address or generate unique username if no valid email is given
            $strUsername = (oxNew(Oxid\Core\MailValidator::class)->isValidEmail((string)$ItemData->EmailAddress) === true)
                ? (string)$ItemData->EmailAddress
                : $sSalesChannelName . '_' . Utils\SystemManager::generateUId() . '@' .
                str_replace('http://', '', $oUser->getConfig()->getConfigParam('sShopURL'));

            //$oUser->createUser();
            //$oUser = $oUser->configureUserBeforeCreation($oUser);
            //$oUser->load($oUser->getId());
            $oUser->oxuser__oxactive = new \OxidEsales\Eshop\Core\Field(1, \OxidEsales\Eshop\Core\Field::T_RAW);
            $oUser->oxuser__oxusername = new \OxidEsales\Eshop\Core\Field($strUsername, \OxidEsales\Eshop\Core\Field::T_RAW);
            //$oUser->changeUserData($oUser->oxuser__oxusername->value, $sPassword, $sPassword, $aInvAdress, $aDelAdress);
            $oUser->oxuser__oxsal = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$ItemData->Title), \OxidEsales\Eshop\Core\Field::T_RAW);

            /* Namen Hack */
            $pattern = "/^(.+?) ((?:(?:D[eu]|d[i]|L[ae]|v[ao]n|der|Ste?\.?|V[ao]n|d[er]) )*[^\s]+)$/";
            preg_match($pattern, $ItemData->Name, $matches);
            $oUser->oxuser__oxfname = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$matches[1]), \OxidEsales\Eshop\Core\Field::T_RAW);
            $oUser->oxuser__oxlname = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$matches[2]), \OxidEsales\Eshop\Core\Field::T_RAW);
            /* \ Namen Hack */

            $oUser->oxuser__oxcompany = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$ItemData->Company), \OxidEsales\Eshop\Core\Field::T_RAW);
            $oUser->oxuser__oxstreet = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$ItemData->Address), \OxidEsales\Eshop\Core\Field::T_RAW);
            $oUser->oxuser__oxstreetnr = new \OxidEsales\Eshop\Core\Field('', \OxidEsales\Eshop\Core\Field::T_RAW);
            $oUser->oxuser__oxaddinfo = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$ItemData->AddressAdd), \OxidEsales\Eshop\Core\Field::T_RAW);
            $oUser->oxuser__oxcity = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$ItemData->City), \OxidEsales\Eshop\Core\Field::T_RAW);
            $oUser->oxuser__oxzip = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$ItemData->PostalCode), \OxidEsales\Eshop\Core\Field::T_RAW);
            $oUser->oxuser__oxfon = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$ItemData->PhonePrivate), \OxidEsales\Eshop\Core\Field::T_RAW);
            $oUser->oxuser__oxcountryid = new \OxidEsales\Eshop\Core\Field($this->getCountryIdByIsoCode($ItemData->CountryIso), \OxidEsales\Eshop\Core\Field::T_RAW);
            $oUser->oxuser__bfox = new \OxidEsales\Eshop\Core\Field('ebay', \OxidEsales\Eshop\Core\Field::T_RAW);

        } catch (\OxidEsales\Eshop\Core\Exception\UserException $exception) {
            Registry::getUtilsView()->addErrorToDisplay($exception, false, true);
            #d($exception);
            #die();
            return false;
        } catch (\OxidEsales\Eshop\Core\Exception\InputException $exception) {
            Registry::getUtilsView()->addErrorToDisplay($exception, false, true);
            #d($exception);
            #die();
            return false;
        } catch (\OxidEsales\Eshop\Core\Exception\DatabaseConnectionException $exception) {
            Registry::getUtilsView()->addErrorToDisplay($exception, false, true);
            #d($exception);
            #die();
            return false;
        } catch (\OxidEsales\Eshop\Core\Exception\ConnectionException $exception) {
            Registry::getUtilsView()->addErrorToDisplay($exception, false, true);
            #d($exception);
            #die();
            return false;
        }

        if ($oUser->save()) {
            return $oUser->getId();
        } else {
            return false;
        }
    }

    /**
     * creates a order entry in oxdb
     *
     * @param SimpleXMLElement
     * @param string
     * @return mixed
     * @throws ConfigurationException
     */
    private function _createOrder($ItemData, $sUserId)
    {
        $oOrder = oxNew(Models\OrderModel::class);
        $oOrder->getConfig()->setShopId(2);

        if ($ItemData->Fba && (int)$ItemData->Fba != 0) {
            $this->_sOrderStatus =
                $oOrder->getConfig()->getShopConfVar(Utils\ConfigurationKeys::CONFIG_KEY_ORDERS_FOLDER_FBA, $this->_strShopOXID);
        } else {
            $this->_sOrderStatus =
                $oOrder->getConfig()->getShopConfVar(Utils\ConfigurationKeys::CONFIG_KEY_ORDERS_FOLDER_NEW, $this->_strShopOXID);
        }

        $oxOrderColumnName = Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_ORDERS_CUSTOMER_ORDER_NUMBER_COLUMN_NAME, 'module:multichannel', "");

        if(strlen($oxOrderColumnName) > 0) {
            $part1 = "oxorder__" . strtolower($oxOrderColumnName);
            $part2 = "value";

            $oOrder->$part1->$part2 = (string) $ItemData->CustomerId;
        }

        $oOrder->oxorder__oxtransstatus->value = 'OK';
        $oOrder->oxorder__oxfolder->value      = 'ORDERFOLDER_NEW';
        $oOrder->oxorder__oxuserid->value      = $sUserId;

        if(isset($ItemData->PaymentMethodsValues->transactionId)) {
            $oOrder->oxorder__oxtransid->value = (string)$ItemData->PaymentMethodsValues->transactionId;
        }

        $oOrder->oxorder__oxpaymentid->value   = (string)$ItemData->PaymentMethod;
        $oOrder->oxorder__oxpaymenttype->value = (string)$ItemData->PaymentMethod;
        $oOrder->oxorder__oxdeltype->value     = (string)$ItemData->ShippingMethod;
        $oOrder->oxorder__oxorderdate->value   = (string)$ItemData->OrderDate;
        $oOrder->oxorder__oxordernr->value     = $this->getOrderNr((string)$ItemData->ExternOrderId, (string)$ItemData->OrderId, $oOrder);

        if (strlen((string)$ItemData->CustomerId) > 0) {
            $oOrder->oxorder__oxremark->value = Utils\SystemManager::utf8ToLocal($ItemData->ExternOrderId) . ' | ' . (string)$ItemData->CustomerId;
        } else {
            $oOrder->oxorder__oxremark->value = Utils\SystemManager::utf8ToLocal($ItemData->ExternOrderId);
        }

        $oOrder->oxorder__oxtotalbrutsum->value  = (float)$ItemData->TotalAmountProducts;
        $oOrder->oxorder__oxtotalnetsum->value   = (float)$ItemData->TotalAmountProductsNetto;
        $oOrder->oxorder__oxtotalordersum->value = (float)$ItemData->TotalAmount;
        $oOrder->oxorder__oxdelcost->value       = (float)$ItemData->ShippingCost;
        $oOrder->oxorder__oxpaycost->value       = (float)$ItemData->PaymentCost;
        $oOrder->oxorder__oxpayvat->value        = (float)$ItemData->PaymentTax;
        $oOrder->oxorder__oxcurrency->value      = $oOrder->getConfig()->getActShopCurrencyObject()->name;
        $oOrder->oxorder__oxcurrate->value       = $oOrder->getConfig()->getActShopCurrencyObject()->rate;
        $oOrder->oxorder__oxdelvat->value        = (float)$ItemData->OrderLines->OrderLine->TaxRate;

        // billing address information
        $sBillCompany = Utils\SystemManager::utf8ToLocal((string)$ItemData->BillingParty->Company);
        $sBillCompany .= ' ' . Utils\SystemManager::utf8ToLocal((string)$ItemData->BillingParty->AddressAdd);
        $sBillCompany = trim($sBillCompany);

        $oOrder->oxorder__oxbillsal->value       = Utils\SystemManager::utf8ToLocal((string)$ItemData->BillingParty->Title);

        /* Namen Hack */
        $matches = null;
        $pattern = "/^(.+?) ((?:(?:D[eu]|d[i]|L[ae]|v[ao]n|der|Ste?\.?|V[ao]n|d[er]) )*[^\s]+)$/";
        preg_match($pattern, $ItemData->BillingParty->Name, $matches);
        $oOrder->oxorder__oxbillfname = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$matches[1]), \OxidEsales\Eshop\Core\Field::T_RAW);
        $oOrder->oxorder__oxbilllname = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$matches[2]), \OxidEsales\Eshop\Core\Field::T_RAW);

        /*
        $oOrder->oxorder__oxbillfname->value     = '';
        $oOrder->oxorder__oxbilllname->value     = Utils\SystemManager::utf8ToLocal((string)$ItemData->BillingParty->Name);
        */
        /* \ Namen Hack */
        $oOrder->oxorder__oxbillcompany->value   = $sBillCompany;
        $oOrder->oxorder__oxbillemail->value     = (oxNew(Oxid\Core\MailValidator::class)->isValidEmail((string)$ItemData->BillingParty->EmailAddress) === true)
            ? (string)$ItemData->BillingParty->EmailAddress : null;
        $oOrder->oxorder__oxbillstreet->value    = Utils\SystemManager::utf8ToLocal((string)$ItemData->BillingParty->Address);
        $oOrder->oxorder__oxbillstreetnr->value  = '';
        $oOrder->oxorder__oxbilladdinfo->value   = '';
        $oOrder->oxorder__oxbillcity->value      = Utils\SystemManager::utf8ToLocal((string)$ItemData->BillingParty->City);
        $oOrder->oxorder__oxbillcountryid->value = $this->getCountryIdByIsoCode($ItemData->BillingParty->CountryIso);
        $oOrder->oxorder__oxbillzip->value       = Utils\SystemManager::utf8ToLocal((string)$ItemData->BillingParty->PostalCode);
        $oOrder->oxorder__oxbillfon->value       = Utils\SystemManager::utf8ToLocal((string)$ItemData->BillingParty->PhonePrivat);

        // delivery address
        $sDelCompany = Utils\SystemManager::utf8ToLocal((string)$ItemData->DeliveryParty->Company);
        $sDelCompany .= ' ' . Utils\SystemManager::utf8ToLocal((string)$ItemData->DeliveryParty->AddressAdd);
        $sDelCompany = trim($sDelCompany);

        $oOrder->oxorder__oxdellsal->value      = Utils\SystemManager::utf8ToLocal((string)$ItemData->DeliveryParty->Title);

        /* names Hack */
        $matches = null;
        $pattern = "/^(.+?) ((?:(?:D[eu]|d[i]|L[ae]|v[ao]n|der|Ste?\.?|V[ao]n|d[er]) )*[^\s]+)$/";
        preg_match($pattern, $ItemData->DeliveryParty->Name, $matches);
        $oOrder->oxorder__oxdelfname = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$matches[1]), \OxidEsales\Eshop\Core\Field::T_RAW);
        $oOrder->oxorder__oxdellname = new \OxidEsales\Eshop\Core\Field(Utils\SystemManager::utf8ToLocal((string)$matches[2]), \OxidEsales\Eshop\Core\Field::T_RAW);
        /*
        $oOrder->oxorder__oxdelfname->value     = '';
        $oOrder->oxorder__oxdellname->value     = Utils\SystemManager::utf8ToLocal((string)$ItemData->DeliveryParty->Name);
        */
        /* /names Hack */

        $oOrder->oxorder__oxdelcompany->value   = $sDelCompany;
        $oOrder->oxorder__oxdellemail->value    = (oxNew(Oxid\Core\MailValidator::class)->isValidEmail((string)$ItemData->DeliveryParty->EmailAddress) === true)
            ? (string)$ItemData->DeliveryParty->EmailAddress : null;
        $oOrder->oxorder__oxdelstreet->value    = Utils\SystemManager::utf8ToLocal((string)$ItemData->DeliveryParty->Address);
        $oOrder->oxorder__oxdelstreetnr->value  = '';
        $oOrder->oxorder__oxdeladdinfo->value   = '';
        $oOrder->oxorder__oxdelcity->value      = Utils\SystemManager::utf8ToLocal((string)$ItemData->DeliveryParty->City);
        $oOrder->oxorder__oxdelcountryid->value = $this->getCountryIdByIsoCode($ItemData->DeliveryParty->CountryIso);
        $oOrder->oxorder__oxdelzip->value       = Utils\SystemManager::utf8ToLocal((string)$ItemData->DeliveryParty->PostalCode);
        $oOrder->oxorder__oxdelfon->value       = Utils\SystemManager::utf8ToLocal((string)$ItemData->DeliveryParty->PhonePrivat);

        $oOrder->oxorder__oxisnettomode->value = (int)Registry::getConfig()->getShopConfVar('blEnterNetPrice', Registry::getConfig()->getShopId());
        $oOrder->oxorder__oxshop->value         = Utils\SystemManager::utf8ToLocal((string)'ebay');

        if ($oOrder->save()) {
            $salesChannelId = $ItemData->SalesChannelId ? $ItemData->SalesChannelId : 0;
            $this->_iOrderCount++;
            $this->_writeOrderImport($oOrder->getId(), $ItemData->ExternOrderId, $ItemData->OrderId, $this->_strShopOXID, $salesChannelId);

            // erstelle OrderXml Datei für Filemaker
            $BNoOrder = oxNew(\Bender\dre_xmlsync\models\dre_order::class);
            $BNoOrder->setOrderArray($oOrder,'ebay');
            $BNoOrder->setCustNr();
            $BNoOrder->setBillCountry();
            $BNoOrder->setDeliveryCountry();
            $BNoOrder->setPayments();
            $BNoOrder->setVouchers();
            $BNoOrder->erstelleOrderXmlDatei();

            $this->setOrderNr($oOrder->oxorder__oxordernr->value);

            /*
            $oxEmail = oxNew(Oxid\Core\Email::class);
            $oxEmail->setBody('ORDER erstellt ' . $oOrder->oxorder__oxordernr->value . "<br>" . 'OXTRANSSTAUS: ' . $oOrder->oxorder__oxtransstatus->value . '<br> OXPAYMENT: ' . $oOrder->oxorder__oxpaymenttype->value . '<br><br> OXTOTALBRUTSUM:' . $oOrder->oxorder__oxtotalbrutsum->value);
            $oxEmail->setSubject('ORDER: ' . $oOrder->oxorder__oxordernr->value . ' über EBAY eingegangen');
            $oxEmail->setRecipient('order@bodynova.de', 'Bodynova GmbH');
            $oxEmail->setReplyTo('order@bodynova.de', 'Bodynova GmbH');
            $oxEmail->send();
            */
            return $oOrder->getId();
        }

        return false;
    }

    /**
     * @param $externOrderId
     * @param $bfOrderId
     * @param Models\OrderModel $objectOrder
     * @return string
     * @throws ConfigurationException
     */
    private function getOrderNr($externOrderId, $bfOrderId, Models\OrderModel $objectOrder)
    {
        $numberingOption = Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_ORDERS_ORDER_NUMBERING);

        switch ($numberingOption) {
            case 'externId':
                $oxordernr = (strlen($externOrderId) > 0 && strlen($externOrderId) < 10) ? $externOrderId : '';
                $oxordernr = (is_numeric($oxordernr) === false) ? '' : $oxordernr;
                break;

            case 'bfId':
                $oxordernr = (strlen($bfOrderId) > 0 && strlen($bfOrderId) < 10) ? $bfOrderId : '';
                break;

            case 'oxId':
            default:
                $oxordernr = $objectOrder->newOrderNumber();

        }

        if ($oxordernr === '') {
            $oxordernr = $objectOrder->newOrderNumber();
        }

        return $oxordernr;
    }

    /**
     * creates the entry in brickfox_import
     *
     * @param string $sOxOrderId
     * @param string $sExternOrderId
     * @param string $sOrderId
     * @param string $sShopId
     * @param int $iSaleschannelId
     * @internal param string $sStatus
     */
    private function _writeOrderImport($sOxOrderId, $sExternOrderId, $sOrderId, $sShopId, $iSaleschannelId)
    {
        $sQ = "insert into brickfox_import (id, oxorderid, externorderid, import_importdate, orderid, shopid, import_oxfolder, saleschannelid) ";
        $sQ .= "values ( '" . Utils\SystemManager::generateUId() . "', '" . $sOxOrderId . "', '" . $sExternOrderId . "', NOW(), '"
            . $sOrderId . "', '" . $sShopId . "', '" . $this->_sOrderStatus . "', " . $iSaleschannelId . " ) ";

        Oxid\Core\DatabaseProvider::getDb()->execute($sQ);
    }

    /**k
     * creates the entry in brickfox_import_articles
     *
     * @param int $iOrderlineId
     * @param string $sProductExternId
     * @param string $sOxorderid
     * @param int $iQuantity
     */
    private function _writeOrderArticleImport($oxOrderArticleId, $iOrderlineId, $sProductExternId, $sOxorderid, $iQuantity)
    {
        $sQ = "insert into brickfox_import_articles (id, import_id, orderline_id, extern_product_id, import_article_oxfolder, import_article_importdate, oxorderid, quantity_ordered) ";
        $sQ .= "values ( '" . Utils\SystemManager::generateUId() . "', '" . $oxOrderArticleId . "', " . $iOrderlineId . ", '"
            . $sProductExternId . "', " . " '', NOW(), '" . $sOxorderid . "', " . $iQuantity . " )";

        Oxid\Core\DatabaseProvider::getDb()->execute($sQ);
    }

    /**
     * checks if order with given already exists in oxdb
     *
     * @param string $externOrderId
     * @param int $iShopId
     * @return boolean
     */
    private function _checkOrderAlreadyImported($externOrderId, $iShopId)
    {
        $returnValue = [];
        $sQuery      = "select externorderid from brickfox_import where externorderid='" . $externOrderId . "' and shopid='" . $iShopId . "'";

        $oResult = Oxid\Core\DatabaseProvider::getDb()->select($sQuery);

        if ($oResult != false && $oResult->count() > 0) {
            while (!$oResult->EOF) {
                $row                                = $oResult->getFields();
                $returnValue[$row['externorderid']] = $row['externorderid'];
                $oResult->fetchRow();
            }
        }

        if (count($returnValue) > 0) {
            return true;
        }

        return false;
    }

    /**
     * @param string $sCountryIsoCode
     * @return string
     * @throws Oxid\Core\Exception\DatabaseConnectionException
     */
    private function getCountryIdByIsoCode($sCountryIsoCode)
    {
        return Oxid\Core\DatabaseProvider::getDb()->getOne("SELECT `oxid` FROM `oxcountry` WHERE `oxisoalpha2` = '$sCountryIsoCode'");
    }
}