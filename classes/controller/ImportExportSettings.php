<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 07.06.19
 * Time: 16:49
 */

namespace bfox\multichannel\classes\controller;

    use bfox\multichannel\classes\util as Utils;
    use bfox\multichannel\classes\exception as Exceptions;

class ImportExportSettings
{

    const SETTING_NAME_ORDERS               = 'ImportOrders';

    const SETTING_NAME_NAMESPACE_IMPORT     = 'import',
        SETTING_NAME_NAMESPACE_EXPORT       = 'export';

    const SETTING_NAMESPACE_IMPORT_CLASSES  = ['orders'];

    /**
     * name
     * @var string
     */
    private $name;

    /**
     * filename base
     * @var string
     */
    private $filenameBase;

    /**
     * action (import or export)
     * @var string
     */
    private $action;

    /**
     * script logger dependencies
     * @var array
     */
    private $scriptLoggerDependencies = array();

    /**
     * execution dependencies
     * @var array
     */
    private $executionDependencies = array();

    /**
     * TransferSettings constructor.
     * @param $name
     * @param $filenameBase
     * @param $action
     */
    public function __construct($name, $filenameBase, $action)
    {
        $this->setName($name);
        $this->setFilenameBase($filenameBase);
        $this->setAction($action);

        $this->addScriptLoggerDependency($this);
    }


    /**
     * @return ImportExportSettings
     * @throws Exceptions\ConfigurationException
     */
    public function getImportOrderStatusSetting()
    {
        return oxNew(ImportExportSettings::class,
            self::SETTING_NAME_ORDERS,
            Utils\OxidRegistry::getModuleConfig(Utils\ConfigurationKeys::CONFIG_KEY_STORAGE_FILENAME_ORDERSSTATUS),
            self::SETTING_NAME_NAMESPACE_IMPORT
        );
    }



    /**
     * getImportFilenameBases.
     *
     * @return array import filename bases
     */
    public function getImportFilenameBases()
    {
        return array(
            self::getImportOrderStatusSetting()->getFilenameBase(),
        );
    }

    /**
     * getClassName.
     *
     * @return string class name
     */
    public function getClassName()
    {
        return $this->name;
    }

    /**
     * getName.
     *
     * @return string name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * setName.
     *
     * @param string $name name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * getAction.
     *
     * @return string action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * setAction.
     *
     * @param string $action action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * getFilenameBase.
     *
     * @return string filename base
     */
    public function getFilenameBase()
    {
        return $this->filenameBase;
    }

    /**
     * setFilenameBase.
     *
     * @param string $filenameBase filename base
     */
    public function setFilenameBase($filenameBase)
    {
        $this->filenameBase = $filenameBase;
    }

    /**
     * getScriptLoggerDependencies.
     *
     * @return array script logger dependencies
     */
    public function getPreparedScriptLoggerDependencies()
    {
        $result = array();

        foreach($this->getScriptLoggerDependencies() as $importExportSetting)
        {
            $result[] = $importExportSetting->getName();
        }
        return $result;
    }

    /**
     * addScriptLoggerDependency.
     *
     * @param ImportExportSettings $scriptLoggerDependency script logger dependency
     */
    public function addScriptLoggerDependency(ImportExportSettings $scriptLoggerDependency)
    {
        $this->scriptLoggerDependencies[] = $scriptLoggerDependency;
    }

    /**
     * hasExecutionDependency.
     *
     * @param array $exchangeFiles exchange files
     * @return boolean has execution dependency
     */
    public function hasExecutionDependency($exchangeFiles)
    {
        $result = false;
        foreach($this->getExecutionDependencies() as $importExportSetting)
        {
            if(true === array_key_exists($importExportSetting->getFilenameBase() , $exchangeFiles))
            {
                $result = true;
                break;
            }
        }

        return $result;
    }

    /**
     * addExecutionDependency.
     *
     * @param ImportExportSettings $executionDependency execution dependencies
     */
    public function addExecutionDependency(ImportExportSettings $executionDependency)
    {
        $this->executionDependencies[] = $executionDependency;
    }

    /**
     * getScriptLoggerDependencies.
     *
     * @return array script logger dependencies
     */
    private function getScriptLoggerDependencies()
    {
        return $this->scriptLoggerDependencies;
    }

    /**
     * getExecutionDependencies.
     *
     * @return array execution dependencies
     */
    private function getExecutionDependencies()
    {
        return $this->executionDependencies;
    }

}