<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 23.08.18
 * Time: 12:12
 */

namespace bfox\multichannel\classes\util;


class LogManager
{

    /**
     * script status
     */
    const   LOGLEVEL_ERROR		= 'ERROR',
            LOGLEVEL_WARNING	= 'WARN',
            LOGLEVEL_DEBUG		= 'DEBUG';




    /**
     * singleton instance
     * @var LogManager
     */
    static private $instance = null;

    /**
     * log file name prefix
     * @var string
     */
    private $logFilenamePrefix = null;

    /**
     * log file name
     * @var string
     */
    private $logFilename = null;

    /**
     * log file handle
     * @var resource
     */
    private $logFileHandle = null;





    /**
     * Constructor.
     */
    private function __construct()
    {

        $this->initLogFilename();

        $this->removeOldLogFiles();
    }

    /**
     * Destructor.
     */
    public function __destruct()
    {
        if(false === is_null($this->logFileHandle))
        {
            fclose($this->logFileHandle);
        }
    }

    /**
     * getInstance.
     *
     * @return LogManager
     */
    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * error.
     *
     * @param string $logMessage log message
     */
    public function error($logMessage)
    {
        $this->log($logMessage, self::LOGLEVEL_ERROR);
    }

    /**
     * debug.
     *
     * @param string $logMessage log message
     */
    public function debug($logMessage)
    {
        $this->log($logMessage, self::LOGLEVEL_DEBUG);
    }

    /**
     * warn.
     *
     * @param string $logMessage log message
     */
    public function warn($logMessage)
    {
        $this->log($logMessage, self::LOGLEVEL_WARNING);
    }

    /**
     * getLogFileLocation.
     *
     * @return string log file location
     */
    public function getLogFileLocation()
    {
        return $this->getLogDirectory() . $this->logFilename;
    }





    /**
     * log.
     *
     * @param string $logMessage log message
     * @param string $logLevel log level
     */
    private function log($logMessage, $logLevel = self::LOGLEVEL_ERROR)
    {
        if(true === in_array($logLevel, $this->getLogLevel()))
        {
            if(true === is_null($this->logFileHandle))
            {
                $this->initLogFileHandle();
            }

            if(is_object($logMessage))
            {
                $logMessage = serialize($logMessage);
            }
            $memoryUsageInMegaByte	= round(memory_get_usage()/1048576,2);
            $logMessage	= sprintf("%s [%s](Memory usage: %s MB): %s%s", date('Y-m-d H:i:s'), $logLevel, $memoryUsageInMegaByte, $logMessage, PHP_EOL);
            fputs($this->logFileHandle, $logMessage);

            if (SystemManager::getInstance()->getDebugMode() === true)
            {
                $this->outputMessageWhenDebugMode($logMessage);
            }
        }
    }

    /**
     * initLogFilename.
     */
    private function initLogFilename()
    {
        $this->logFilenamePrefix	= OxidRegistry::getModuleConfig(ConfigurationKeys::CONFIG_KEY_LOGGING_FILENAME);
        $this->logFilename			= $this->logFilenamePrefix . '_' . date('Y') . '_' . date('m') . '_' . date('d') . '.log';
    }

    /**
     * removeOldLogFiles.
     */
    private function removeOldLogFiles()
    {
        $logDirectory = $this->getLogDirectory();
        if(true === is_dir($logDirectory))
        {
            $logFiles				= glob($logDirectory . '/*');
            $cleanupTimeInSeconds	= $this->getCleanupTimeInSeconds();
            foreach($logFiles as $logFile)
            {
                // only remove old log files
                if(0 === strpos(basename($logFile), $this->logFilenamePrefix)
                    && filectime($logFile) < (time() - $cleanupTimeInSeconds))
                {
                    unlink($logFile);
                }
            }
        }
    }

    /**
     * getCleanupTimeInSeconds.
     *
     * @return integer cleanup time in seconds
     */
    private function getCleanupTimeInSeconds()
    {
        return (OxidRegistry::getModuleConfig(ConfigurationKeys::CONFIG_KEY_LOGGING_CLEANUP_IN_DAYS) * 86400);
    }

    /**
     * getLogDirectory.
     *
     * @return string log directory
     */
    private function getLogDirectory()
    {
        return OxidRegistry::getLogsDir();
    }

    /**
     * getLogLevel.
     *
     * @return array log level
     */
    private function getLogLevel()
    {
        $logLevel = OxidRegistry::getModuleConfig(ConfigurationKeys::CONFIG_KEY_LOGGING_LEVEL);
        return explode(',', $logLevel);
    }

    /**
     * initLogFileHandle.
     */
    private function initLogFileHandle()
    {
        $this->logFileHandle = fopen($this->getLogFileLocation(), 'a+');
    }

    private function outputMessageWhenDebugMode($message)
    {
        echo "<h4>" . $message . "</h4>";
    }


    public function getLastRunDate($configKeyFieldName)
    {
        $lastRun = OxidRegistry::getModuleConfig($configKeyFieldName);

        if ($lastRun == "" || !$this->validateDate($lastRun, 'Y-m-d H:i:s')){
            $lastRun = date_create();
            date_add($lastRun,date_interval_create_from_date_string("-1 hour"));
            $lastRun = date_format($lastRun,'Y-m-d H:i:s');
        }
        return $lastRun;
    }


    private function validateDate($date, $format)
    {
        //Works on PHP 5.3 or higher
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }


    public function setLastRunDate($configKeyFieldName, $dateTime = null)
    {
        if ($dateTime === null){
            $dateTime = date('Y-m-d H:i:s');
        }

        OxidRegistry::saveModuleConfig($configKeyFieldName, $dateTime, 'str');
    }

}