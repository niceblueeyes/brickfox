<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 07.06.19
 * Time: 15:50
 */

namespace bfox\multichannel\classes\util;

use bfox\multichannel\classes\model as Models;
use bfox\multichannel\classes\exception as Exceptions;

class ScriptManager
{
    /*****************************************************************************
     *
     * Class properties
     *
     *****************************************************************************/


    /**
     * script logger model
     * @var Models\ScriptLoggerModel
     */
    private $scriptLoggerModel = null;

    private $_hangingCleanupTime;

    private $_removalCleanupTime;

    /*****************************************************************************
     *
     * Callable functions
     *
     *****************************************************************************/

    /**
     * Contructor.
     * @var $hangingCleanupTime int : Time in seconds to hold a Script logger running
     * @var $removalCleanupTime int : Number of days that logs will be maintained
     *
     */
    public function __construct($hangingCleanupTime = 1800, $removalCleanupTime = 90)
    {
        $this->_hangingCleanupTime = $hangingCleanupTime;
        $this->_removalCleanupTime = $removalCleanupTime;

        $this->initScriptLoggerModel();

        // cleanup hanging script database entries and remove old script logger database entries
        $this->cleanup();
    }


    /**
     * check.
     *
     * @param array $scriptDependencies script dependencies
     */
    public function check($scriptDependencies)
    {
        if(0 < count($scriptDependencies))
        {
            $this->getScriptLoggerModel()->loadByScriptDependencies($scriptDependencies);

            // a depending script is already running
            if(true === $this->getScriptLoggerModel()->isLoaded())
            {
                throw oxNew(Exceptions\ImportExportException::class,
                    'Script is already running!Script id:' . $this->getScriptLoggerModel()->getOxid(),
                    Exceptions\ImportExportException::SCRIPT_RUNNING);
            }
        }
    }

    /**
     * start.
     *
     * @param string $scriptName script name
     */
    public function start($scriptName)
    {
        $this->getScriptLoggerModel()->setScriptName($scriptName);
        $this->getScriptLoggerModel()->start();
    }

    /**
     * end.
     */
    public function end()
    {
        $this->getScriptLoggerModel()->end();
        $this->initScriptLoggerModel();
    }

    /*****************************************************************************
     *
     * helper functions
     *
     *****************************************************************************/

    /**
     * cleanup.
     */
    private function cleanup()
    {
        // fixing hanging database entries
        $cleanupTime = time() - $this->getHangingCleanupTimeInSeconds();
        $this->getScriptLoggerModel()->cleanupHangingRecords($cleanupTime);

        // removal old database entries
        $cleanupTime = time() - $this->getRemovalCleanupTimeInSeconds();
        $this->getScriptLoggerModel()->removeOldRecords($cleanupTime);
    }

    /**
     * getHangingCleanupTimeInSeconds.
     *
     * @return integer hanging cleanup time in seconds
     */
    private function getHangingCleanupTimeInSeconds()
    {
        return $this->_hangingCleanupTime;
    }

    /**
     * getRemovalCleanupTimeInSeconds.
     *
     * @return integer removal cleanup time in seconds
     */
    private function getRemovalCleanupTimeInSeconds()
    {
        return ($this->_removalCleanupTime * 86400);
    }

    /**
     * initScriptLoggerModel.
     */
    private function initScriptLoggerModel()
    {
        $this->setScriptLoggerModel(oxNew(Models\ScriptLoggerModel::class));
    }

    /**
     * getScriptLoggerModel.
     *
     * @return Models\ScriptLoggerModel script logger model
     */
    private function getScriptLoggerModel()
    {
        return $this->scriptLoggerModel;
    }

    /**
     * setScriptLoggerModel.
     *
     * @param Models\ScriptLoggerModel $scriptLoggerModel script logger model
     */
    private function setScriptLoggerModel($scriptLoggerModel)
    {
        $this->scriptLoggerModel = $scriptLoggerModel;
    }
}