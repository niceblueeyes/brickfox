<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 08.04.19
 * Time: 16:29
 */

namespace bfox\multichannel\classes\util;

use bfox\multichannel\classes\exception as Exceptions;
use OxidEsales\Eshop\Core\Registry;

class OxidRegistry
{
    /**
     * #####################
     *  STATIC METHODS
     * #####################
     */

    /**
     * @return integer default language id
     */
    public static function getDefaultLanguageId()
    {
        return Registry::getConfig()->getConfigParam('sDefaultLang');
    }

    /**
     * @return array default language id
     */
    public static function getOxidLanguageArray()
    {
        return Registry::getLang()->getLanguageArray();
    }

    public static function setBaseLanguage($languageOxidId)
    {
        return Registry::getLang()->setBaseLanguage($languageOxidId);
    }

    public static function getBaseLanguage()
    {
        return Registry::getLang()->getBaseLanguage();
    }

    /**
     * @return string shop directory
     */
    public static function getShopDirectory()
    {
        return Registry::getConfig()->getConfigParam('sShopDir');
    }

    /**
     * @return string shop url
     */
    public static function getShopURL()
    {
        return Registry::getConfig()->getConfigParam('sShopURL');
    }

    public static function getLogsDir()
    {
        return Registry::getConfig()->getLogsDir();
    }

    /**
     * @return int shopId
     */
    public static function getActiveShopId()
    {
        return Registry::getConfig()->getShopId();
    }

    /**
     * @param int $oxshopid
     *
     */
    public static function setActiveShopId($oxshopid)
    {
        Registry::getSession()->initNewSession();
        Registry::getConfig()->setShopId($oxshopid);
        Registry::getSession()->setVariable("shp", $oxshopid);
        Registry::getSession()->setVariable("actshop", $oxshopid);
        Registry::getSession()->setVariable('currentadminshop', $oxshopid);
    }

    public static function getOxidConfig($configKey)
    {
        $result = Registry::getConfig()->getConfigParam($configKey);

        if (true === is_null($result)) {
            throw oxNew(Exceptions\ConfigurationException::class, 'Invalid configuration key: ' . $configKey . ' for shop id: ' . self::getActiveShopId());
        }

        return $result;
    }

    /**
     * @param string $configKey configuration key
     * @param string $moduleName module name
     * @param string $defaultValue Preferably send here the default defined at the metadata file for this field
     * @param string $defaultValueType We MUST send here the type defined at the metadata file for this field
     * @return mixed configuration value
     * @throws Exceptions\ConfigurationException configuration exception
     */
    public static function getModuleConfig($configKey, $moduleName = 'module:multichannel', $defaultValue = null, $defaultValueType = null)
    {
        $result = Registry::getConfig()->getShopConfVar($configKey, self::getActiveShopId(), $moduleName);

        if (true === is_null($result)) {
            if ($defaultValue === null) {
                throw oxNew(Exceptions\ConfigurationException::class, 'Invalid configuration key: ' . $configKey . ' for shop id: ' . self::getActiveShopId());
            } else {
                self::saveModuleConfig($configKey, $defaultValue, $defaultValueType, $moduleName);
                $result = Registry::getConfig()->getShopConfVar($configKey, self::getActiveShopId(), $moduleName);
            }
        }

        return $result;
    }

    /**
     * @param string $configKey configuration key
     * @param mixed $configValue configuration value
     * @param string $configType config type
     * @param string $moduleName module name
     */
    public static function saveModuleConfig($configKey, $configValue, $configType, $moduleName = 'module:multichannel')
    {
        Registry::getConfig()->saveShopConfVar($configType, $configKey, $configValue, self::getActiveShopId(), $moduleName);
    }

    public static function getMultishopArticleFields()
    {
        return Registry::getConfig()->getMultishopArticleFields();
    }

    public static function getUrlParameter($parameterName)
    {
        return Registry::getConfig()->getRequestParameter($parameterName);
    }

    public static function getActShopCurrencyObject()
    {
        return Registry::getConfig()->getActShopCurrencyObject();
    }

    public static function getListActiveShopIds()
    {
        return Registry::getConfig()->getShopIds();
    }
}