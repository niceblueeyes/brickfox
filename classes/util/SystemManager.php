<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 14.08.18
 * Time: 13:09
 */

namespace bfox\multichannel\classes\util;



class SystemManager
{

    /**
     * singleton instance
     *
     * @var SystemManager
     */
    static private $instance = null;

    /** @var int */
    private $shopId = null;

    /** @var bool */
    private $debugMode = false;

    /**
     */
    private function __construct()
    {
        $this->shopId = $this->getActiveShopId();
    }

    /**
     * @return SystemManager
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function setDebugMode ($blDebugMode)
    {
        $this->debugMode = $blDebugMode;
    }

    public function getDebugMode ()
    {
        return $this->debugMode;
    }

    /**
     * @return string shop directory
     */
    public function getShopDirectory()
    {
        return OxidRegistry::getOxidConfig('sShopDir');
    }


    /**
     * @return int shopId
     */
    public function getActiveShopId()
    {
        return OxidRegistry::getActiveShopId();
    }

    /**
     * @param int $oxshopid
     */
    public function setActiveShopId($oxshopid)
    {
        OxidRegistry::setActiveShopId($oxshopid);
    }


    /**
     * @param string $units
     * @return string
     */
    public static function getMemoryUsage($units = 'MB')
    {
        $memoryPeak = memory_get_peak_usage(true);

        switch($units)
        {
            case 'GB':
                $memoryPeak = $memoryPeak / 1024 / 1024 / 1024;
                break;

            case 'MB':
                $memoryPeak = $memoryPeak / 1024 / 1024;
                break;

            case 'kB':
                $memoryPeak = $memoryPeak / 1024;
                break;

            case 'B':
            default:
                break;
        }

        return number_format($memoryPeak, 0, '', ' ') . ' ' . $units;
    }

    public static function printMemoryUsage($prefix = '')
    {
        echo $prefix . round(memory_get_usage(true)/1048576,2) . ' MB<br/>';
    }

    /**
     * @param string $value
     * @return string
     */
    public static function localToUtf8($value)
    {
        if(1 != self::getUtfMode())
        {
            $value = utf8_encode($value);
        }

        return $value;
    }

    /**
     * @param string $value
     * @return string
     */
    public static function utf8ToLocal($value)
    {
        if(1 != self::getUtfMode())
        {
            $value = utf8_decode($value);
        }

        return $value;
    }

    /**
     * @return mixed
     */
    public static function getUtfMode()
    {
        return OxidRegistry::getOxidConfig('iUtfMode');
    }

    /**
     * generates a unique id
     *
     * @return string
     */
    public static function generateUId()
    {
        return substr(md5(uniqid('', true) . '|' . microtime()), 0, 29);
    }


    /**
     * returns the path for the export files
     *
     * @return string
     */
    public static function getExportPath()
    {
        return self::getExchangePath() . ConfigurationKeys::CONFIG_KEY_EXPORT_ACTION_NAME. '/';
    }


    /**
     * returns the path for the export files
     *
     * @return string
     */
    public static function getImportPath()
    {
        return self::getExchangePath() . ConfigurationKeys::CONFIG_KEY_IMPORT_ACTION_NAME. '/';
    }



    /**
     * returns the path for operation with files with a trailing slash
     *
     * @return string
     */
    public function getExchangePath()
    {
        $exchangeDirectory = OxidRegistry::getModuleConfig(ConfigurationKeys::CONFIG_KEY_STORAGE_DIRECTORY);
        $exchangePath = rtrim(getShopBasePath(), '/') . $exchangeDirectory;

        return $exchangePath . '/';
    }

    /**
     * returns the path for operation with files, inside shop folder, with a trailing slash
     *
     * @return string
     */
    public function getShopExchangePath()
    {
        $shopFolder = '';
        $useShopExchange = OxidRegistry::getModuleConfig(ConfigurationKeys::CONFIG_KEY_STORAGE_DIRECTORY_INCLUDE_SHOP);

        if ($useShopExchange === true)
        {
            $shopFolder = $this->getActiveShopId() . '/';
        }

        return $this->getExchangePath() . $shopFolder;
    }


}