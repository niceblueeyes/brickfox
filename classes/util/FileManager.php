<?php
/**
 * Class FileManager
 */

namespace bfox\multichannel\classes\util;


class FileManager
{
    const FILENAME_DELIMITER = '_';


    /**
     * singleton instance
     * @var FileManager
     */
    static private $instance = null;



    /**
     * getInstance.
     * @return FileManager
     */
    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new self;
        }
        return self::$instance;
    }


    /**
     * generateFilename.
     *
     * @param string $filenameBase file name base
     * @return string full file name
     */
    public static function generateFilename($filenameBase)
    {
        return $filenameBase . '_' . date('Ymd_His') . '.xml';
    }

    /**
     * @param array $importFilenameBases
     * @return array
     * @throws \bfox\multichannel\classes\exception\ConfigurationException
     */
    public function getImportFiles($importFilenameBases)
    {
        $result           = array();
        $exchangeXMLFiles = $this->getExchangeXMLFiles();


        if (true === is_array($exchangeXMLFiles))
        {
            foreach ($exchangeXMLFiles as $exchangeXMLFile)
            {
                $exchangeXMLFilename = pathinfo($exchangeXMLFile, PATHINFO_FILENAME);
                list($exchangeXMLFilenameBase, $exchangeXMLFilenameCreation) = explode(self::FILENAME_DELIMITER, $exchangeXMLFilename, 2);

                foreach ($importFilenameBases as $importFilenameBase)
                {
                    if ($exchangeXMLFilenameBase == $importFilenameBase)
                    {
                        $useFile = true;

                        // more than one files found, so compare dates and use oldest
                        if (true === isset($result[$importFilenameBase]))
                        {
                            $exchangeXMLFileCreationDate = $this->getTimestamp($exchangeXMLFilenameCreation);
                            $exchangeXMLFilenameCompare  = pathinfo($result[$importFilenameBase], PATHINFO_FILENAME);
                            list(, $exchangeXMLFilenameCreationCompare) = explode(self::FILENAME_DELIMITER, $exchangeXMLFilenameCompare, 2);
                            $exchangeXMLFileCreationDateCompare = $this->getTimestamp($exchangeXMLFilenameCreationCompare);

                            $useFile = $exchangeXMLFileCreationDate < $exchangeXMLFileCreationDateCompare ? true : false;
                        }

                        if (true === $useFile)
                        {
                            $result[$importFilenameBase] = $exchangeXMLFile;
                        }

                        break;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param string $filename
     * @return string
     * @throws \bfox\multichannel\classes\exception\ConfigurationException
     */
    public function createExportFile($filename)
    {
        $fileLocation = $this->getExchangeDirectoryLocation() . $filename;
        $fileHandle   = fopen($fileLocation, 'w');

        fclose($fileHandle);

        return $fileLocation;
    }

    /**
     * @param string $oldFilename
     * @param string $newFilename
     * @throws \bfox\multichannel\classes\exception\ConfigurationException
     */
    public function renameExportFile($oldFilename, $newFilename)
    {
        $oldFileLocation = $this->getExchangeDirectoryLocation() . $oldFilename;
        $newFileLocation = $this->getExchangeDirectoryLocation() . $newFilename;

        if (true === file_exists($oldFileLocation)) {
            rename($oldFileLocation, $newFileLocation);
        }
    }

    /**
     * @param string $fileLocation file location
     */
    public function deleteImportFile($fileLocation)
    {
        if (true === file_exists($fileLocation)) {
            unlink($fileLocation);
        }
    }

    /**
     * @return array
     * @throws \bfox\multichannel\classes\exception\ConfigurationException
     */
    private function getExchangeXMLFiles()
    {
        $exchangeDirectoryLocation = $this->getExchangeShopDirectoryLocation();

        return glob($exchangeDirectoryLocation . '*.xml');
    }

    /**
     * @return string
     * @throws \bfox\multichannel\classes\exception\ConfigurationException
     */
    private function getExchangeDirectoryLocation()
    {
        $exchangeDirectory = $this->getExchangeDirectory();

        if(DIRECTORY_SEPARATOR == substr($exchangeDirectory, 0, 1))
        {
            $exchangeDirectory =  substr($exchangeDirectory, 1);
        }

        // oxid will deliver a shop dir path with a trailing "/"
        $exchangeDirectoryLocation  = OxidRegistry::getShopDirectory() . $exchangeDirectory;

        if(DIRECTORY_SEPARATOR != substr($exchangeDirectoryLocation, -1))
        {
            $exchangeDirectoryLocation .= DIRECTORY_SEPARATOR;
        }

        return $exchangeDirectoryLocation;
    }


    /**
     * getExchangeShopDirectoryLocation.
     * @return string exchange shop directory location
     */
    public function getExchangeShopDirectoryLocation()
    {
        $exchangeShopDirectoryLocation  = $this->getExchangeDirectoryLocation();
        $exchangeShopDirectory          = $this->getExchangeShopDirectory();

        if(false === is_null($exchangeShopDirectory) && 0 < strlen($exchangeShopDirectory))
        {
            $exchangeShopDirectoryLocation .=  $exchangeShopDirectory . DIRECTORY_SEPARATOR;
        }

        return $exchangeShopDirectoryLocation;
    }

    /**
     * @return int
     */
    private function getExchangeShopDirectory()
    {
        return OxidRegistry::getActiveShopId();
    }

    /**
     * @param int $shp Shop id parameter shp from oxid, when is set it will have preference
     * @return array
     * @throws \bfox\multichannel\classes\exception\ConfigurationException
     */
    public function getValidExchangeSubdirectories($shp = null)
    {
        $validExchangeSubdirectories = [];

        $changeDefault = OxidRegistry::getModuleConfig(ConfigurationKeys::CONFIG_KEY_LOOP_SHOPS_OVERRIDE_DEFAULT);

        if ($shp === null) {
            if ($changeDefault === false) {
                $oxShopIdList = OxidRegistry::getListActiveShopIds();

                $exchangeShopDirectoryLocation = $this->getExchangeDirectoryLocation();

                foreach ($oxShopIdList as $oxShopId) {
                    if (true === file_exists($exchangeShopDirectoryLocation)) {
                        $validExchangeSubdirectories[$oxShopId] = $oxShopId;
                    }
                }
            } else {
                $aShops = OxidRegistry::getModuleConfig(ConfigurationKeys::CONFIG_KEY_LOOP_SHOPS_ID_VALUES);
                $shopsIds = explode(',', $aShops[0]);

                foreach ($shopsIds as $oxShopId) {
                    $exchangeShopDirectoryLocation = $this->getExchangeDirectoryLocation() . $oxShopId;

                    if (true === file_exists($exchangeShopDirectoryLocation)) {
                        $validExchangeSubdirectories[$oxShopId] = $oxShopId;
                    }
                }
            }
        }
        else
        {
            array_push($validExchangeSubdirectories, $shp);
        }

        return $validExchangeSubdirectories;
    }

    /**
     * getExchangeDirectory.
     *
     * @return string exchange directory
     */
    private function getExchangeDirectory()
    {
        return OxidRegistry::getModuleConfig(ConfigurationKeys::CONFIG_KEY_STORAGE_DIRECTORY);
    }


    /**
     * Pay attention:
     * Currently PHP version 5.2.6 is in use.
     * For that reason it's not possible to use:
     * DateTime::createFromFormat('Ymd_His', $timestampString)
     *
     * @param string $timestampString timestamp string
     * @return integer timestamp
     */
    private function getTimestamp($timestampString)
    {
        $ftime = strptime($timestampString, 'Ymd_His');

        return mktime($ftime['tm_hour'], $ftime['tm_min'], $ftime['tm_sec'], 1, $ftime['tm_yday'] + 1, $ftime['tm_year'] + 1900);
    }
}
