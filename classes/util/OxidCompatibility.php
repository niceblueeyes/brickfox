<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 24.08.17
 * Time: 12:27
 *
 * Class to provide control for the functionalities and compatibilities to groups of oxid versions
 * to check for more look at https://oxidforge.org/en/source-code-documentation-overview
 */


namespace bfox\multichannel\classes\util;

use bfox\multichannel\wrapper\Core\OxidRegistry as Registry;

class OxidCompatibility
{


    const OXID_EE_VALUE = 'EE',
        OXID_PE_VALUE = 'PE',
        OXID_CE_VALUE = 'CE';


    /**
     *   Class Properties
     */

    private $supportGetInstance     = false;

    private $supportShops           = false;

    private $versionDescription;


    /**
     *   Class Methods
     */
    public function __construct()
    {

        $config = oxNew(Registry::class);

        $version    = $config->getConfig()->getVersion ();
        $edition    = $config->getConfig()->getEdition ();

        $this->versionDescription = $version . $edition;

        /**
         * All checks for EE
         */
        if ($edition === self::OXID_EE_VALUE){

            $this->supportShops = true;

            if (version_compare($version, '5.0.0', '<'))
            {
                $this->supportGetInstance = true;
            }
        }

        /**
         * All checks for CE and PE
         */
        if ($edition === self::OXID_PE_VALUE || $edition === self::OXID_CE_VALUE){

            if (version_compare($version, '4.7.0', '<'))
            {
                $this->supportGetInstance = true;
            }
        }

    }


    /**
     *   Class getters and setters
     */

    public function getVersionDescription(){
        return $this->versionDescription;
    }

    public function getSupportShops(){
        return $this->supportShops;
    }

    public function getSupportGetInstance(){
        return $this->supportGetInstance;
    }

}