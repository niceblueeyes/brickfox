<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 23.08.18
 * Time: 11:40
 */

namespace bfox\multichannel\classes\util;


class ConfigurationKeys
{
    const
        //logging
        CONFIG_KEY_LOGGING_FILENAME									= 'sLogFilename',
        CONFIG_KEY_LOGGING_CLEANUP_IN_DAYS							= 'sLogCleanupInDays',
        CONFIG_KEY_LOGGING_LEVEL									= 'sLogLevel',

        //products export
        CONFIG_KEY_LAST_RUN_PRODUCTS_UPDATE                         = 'strLastProductsUpdate',

        //third party stock
        CONFIG_KEY_THIRD_PARTY_FLAG                                 = "blnThirdPartyMap",
        CONFIG_KEY_THIRD_PARTY_MAP_FROM                             = "strThirdPartyMapFrom",
        CONFIG_KEY_THIRD_PARTY_MAP_AS                               = "slcThirdPartyMapAs",

        //operations
        CONFIG_KEY_EXPORT_ACTION_NAME                               = "export",
        CONFIG_KEY_IMPORT_ACTION_NAME                               = "import",

        //custom products export
        CONFIG_KEY_CUSTOM_EXPORT_GROUPS_TO_EXPORT                    = 'aGroupsToExport',
        CONFIG_KEY_CUSTOM_EXPORT_TABLES_TO_CHECK                     = 'aLastUpdateCheck',
        CONFIG_KEY_CUSTOM_EXPORT_LAST_CHANGES                        = 'strCustomExportLastChange',

        // shops
        CONFIG_KEY_BF_SHOP_ID_TO_SHOP_ID_MAPPING                    = 'aBfShopIdToShopIdMapping',
        CONFIG_KEY_LOOP_SHOPS_OVERRIDE_DEFAULT                      = 'blnOverrideShopsLoopDefault',
        CONFIG_KEY_LOOP_SHOPS_ID_VALUES                             = 'aShopsToLoop',

        // storage
        CONFIG_KEY_STORAGE_DIRECTORY								= 'strStorageDirectory',
        CONFIG_KEY_STORAGE_DIRECTORY_INCLUDE_SHOP					= 'boolStorageShopDirectory',
        CONFIG_KEY_STORAGE_FILENAME_ORDERS							= 'sStorageFilenameOrders',
        CONFIG_KEY_STORAGE_FILENAME_PRODUCTS_REPORTS				= 'sStorageFilenameProductsReports',
        CONFIG_KEY_STORAGE_FILENAME_ORDERSSTATUS					= 'sStorageFilenameOrdersStatus',
        CONFIG_KEY_STORAGE_FILENAME_PRODUCTS_DELETES        		= 'sStorageFilenameProductDelete',
        CONFIG_KEY_STORAGE_FILENAME_MANUFACTURERS					= 'sStorageFilenameManufacturers',
        CONFIG_KEY_STORAGE_FILENAME_CATEGORIES						= 'sStorageFilenameCategories',
        CONFIG_KEY_STORAGE_FILENAME_PRODUCTS						= 'sStorageFilenameProducts',
        CONFIG_KEY_STORAGE_FILENAME_PRODUCTS_UPDATE					= 'sStorageFilenameProductsUpdate',
        CONFIG_KEY_STORAGE_FILENAME_PRODUCTS_ASSIGNMENTS			= 'sStorageFilenameProductsAssign',
        CONFIG_KEY_STORAGE_FILENAME_PRODUCTSMAPPING					= 'sStorageFilenameProductsMap',
        CONFIG_KEY_STORAGE_FILENAME_CATEGORIESMAPPING				= 'sStorageFilenameCategoriesMap',
        CONFIG_KEY_STORAGE_FILENAME_MANUFACTURERSMAPPING			= 'sStorageFilenameManufacturersMap',

        // orders
        CONFIG_KEY_ORDERS_ORDER_NUMBERING                           = 'slcOrderNumberType',
        CONFIG_KEY_ORDERS_FOLDER_NEW								= 'sOrderfolderNew',
        CONFIG_KEY_ORDERS_FOLDER_SHIPPED							= 'sOrderfolderShipped',
        CONFIG_KEY_ORDERS_FOLDER_FBA								= 'sOrderfolderFba',
        CONFIG_KEY_ORDERS_ORDER_STATUS_TYPE                         = 'slcOrderStatusUpdateType',
        CONFIG_KEY_ORDERS_ORDER_STATUS_MAPPING                      = 'aaOrdersStatusFolderMapping',
        CONFIG_KEY_ORDERS_LAST_RUN								    = 'sOrderLastRun',
        CONFIG_KEY_ORDERS_CUSTOMER_ORDER_NUMBER_COLUMN_NAME         = 'sCustomerOrderNumberColumnName',

        // scriptLogger
        CONFIG_KEY_SL_CLEANUP_HANGING_IN_SECONDS					= 'sScriptLoggerCleanupDBHanging',
        CONFIG_KEY_SL_CLEANUP_REMOVAL_IN_DAYS						= 'sScriptLoggerCleanupDBRemoval',

        // tax
        CONFIG_KEY_TAX_REDUCED										= 'sTaxReduced',
        CONFIG_KEY_TAXCLASS_MAPPING									= 'aTaxClassMapping',

        // mailings
        CONFIG_KEY_MAILINGS_SHIPPING_CONFIRMATION					= 'bSendShippingConfirmationMail',

        // prices
        CONFIG_KEY_PRICE_GROSS_STORAGE_MAPPING                      = 'aPriceGrossStorageMapping',
        CONFIG_KEY_SPECIAL_PRICE_STORAGE_MAPPING                    = 'aSpecialPriceStorageMapping',

        // autoconnect
        AUTOCONNECT_KEY_HTACCESS                                    = 'Autoconnect: Using credentials.',
        AUTOCONNECT_KEY_MORE_FILES                                  = 'Autoconnect: Files to be sent!',
        AUTOCONNECT_KEY_END_LOOP                                    = 'Autoconnect: End Loop.',
        AUTOCONNECT_KEY_ERROR_CURL                                  = 'Autoconnect: cUrl returned no data.',
        AUTOCONNECT_KEY_ERROR_TEST                                  = 'Autoconnect: Error calling the test page. Check Credentials and Url.';




}