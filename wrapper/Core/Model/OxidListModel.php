<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 09.08.18
 * Time: 12:45
 */

namespace bfox\multichannel\wrapper\Core\Model;


class OxidListModel extends OxidListModel_parent
{

    private $customExportAdditionalTables = array(
                                                "oxartextends__oxid",
                                                "oxaccessoire2article__oxarticlenid",
                                                "oxobject2article__oxarticlenid",
                                                "oxobject2attribute__oxobjectid",
                                                "oxobject2category__oxobjectid",
                                                "oxobject2seodata__oxid",
                                                "oxprice2article__oxartid",
                                            );

    private $additionalClauses;

    /**
     * @param $whereField - only field name, no table name nor where
     * @param $whereClause - operand and value
     */
    public function setAdditionalClauses($whereField, $whereClause)
    {
        $viewName = $this->getBaseObject()->getViewName();
        $clauseSize = $this->additionalClauses;

        if (strlen($clauseSize) === 0)
        {
            $this->additionalClauses = str_ireplace("where", "",  $viewName . '.' . $whereField .  ' ' . $whereClause);
        }
        else
        {
            $this->additionalClauses .=  " and "  . $viewName . '.' . $whereField .  ' ' . $whereClause;
        }
    }

    // this was based in the original getList method
    public function getListWithClause($fullExport = false)
    {
        $addWhereOrAnd = $this->additionalClauses;
        $addWhereOrAnd = (strlen($addWhereOrAnd) == 0) ? " 1=1 " : $addWhereOrAnd;

        $oListObject = $this->getBaseObject();
        $sFieldList = $oListObject->getSelectFields();
        $sQ = "select $sFieldList from " . $oListObject->getViewName();
        if ($fullExport === false && $sActiveSnippet = $oListObject->getSqlActiveSnippet()) {
            $sQ .= " where $sActiveSnippet ";
            $sQ .= " and $addWhereOrAnd";
        }
        else{
            $sQ .= " where $addWhereOrAnd";
        }
        $this->selectString($sQ);

        return $this;
    }


    public function getArticleListByLastUpdate($lastUpdateDatetime, $aAdditionalTables)
    {
        /*
         * Here we check the last update time for the TABLE. This is updated automatically in oxid
         * so it is a good parameter. Currently we can check changes in oxarticles table (parent, in its variants)
         * but also in any other table that relates to oxarticles and have a oxtimestamp field.
         *
         * TODO: add check if view exists (for example oxfield2shop, Enterprise Edition only)
         */

        $oListObject = $this->getBaseObject();
        $viewName = $oListObject->getViewName();

        $sQ = $this->checkChangesArticlesTable($lastUpdateDatetime, $oListObject, $viewName);
        $sQ .= $this->addCheckOtherTables($lastUpdateDatetime, $oListObject, $viewName, $aAdditionalTables);

        $this->selectString($sQ);

        return $this;
    }

    private function addCheckOtherTables($lastUpdateDatetime, $oListObject, $viewName, $aAdditionalTables)
    {
        $sQ = "";

        foreach ($aAdditionalTables as $key => $value)
        {
            if (in_array($value, $this->customExportAdditionalTables))
            {
                $names = explode("__", $value);
                $sQ .= " UNION " . $this->checkChangesRelatedTables($names[0], $names[1], $lastUpdateDatetime, $oListObject, $viewName);
            }
        }
        return $sQ;
    }


    private function checkChangesArticlesTable($lastUpdateDatetime, $oListObject, $viewName)
    {
        //Last change in the Parents
        $sQ =  " SELECT {$viewName}.OXID FROM " . $viewName;
        $sQ .= ($sActiveSnippet = $oListObject->getSqlActiveSnippet()) ?  " WHERE $sActiveSnippet " : " WHERE 1 ";
        $sQ .= (strlen($this->additionalClauses) > 0) ? " AND $this->additionalClauses" : "";
        $sQ .= " AND {$viewName}.OXTIMESTAMP >= '{$lastUpdateDatetime}' ";

        $sQ .= " UNION ";

        //Last change in the Variants
        $sQ .= " SELECT OXVAR.OXPARENTID FROM oxarticles OXVAR INNER JOIN {$viewName} ON {$viewName}.OXID = OXVAR.OXPARENTID " ;
        $sQ .= ($sActiveSnippet = $oListObject->getSqlActiveSnippet()) ?  " WHERE $sActiveSnippet " : " WHERE 1 ";
        $sQ .= (strlen($this->additionalClauses) > 0) ? " AND $this->additionalClauses" : "";
        $sQ .= " AND OXVAR.OXTIMESTAMP >= '{$lastUpdateDatetime}' ";

        return $sQ;
    }


    private function checkChangesRelatedTables($tableName, $articleIdField, $lastUpdateDatetime, $oListObject, $viewName)
    {
        //Parents
        $sQ = " SELECT {$tableName}.{$articleIdField} FROM {$tableName} INNER JOIN {$viewName} ON {$viewName}.OXID = {$tableName}.{$articleIdField} " ;
        $sQ .= ($sActiveSnippet = $oListObject->getSqlActiveSnippet()) ?  " WHERE $sActiveSnippet " : " WHERE 1 ";
        $sQ .= (strlen($this->additionalClauses) > 0) ? " AND $this->additionalClauses" : "";
        $sQ .= " AND {$tableName}.OXTIMESTAMP >= '{$lastUpdateDatetime}'";

        $sQ .= " UNION ";

        //Variants
        $sQ .= " SELECT OXVAR.OXPARENTID FROM {$tableName} INNER JOIN oxarticles OXVAR ON {$tableName}.{$articleIdField} = OXVAR.OXID ";
        $sQ .= " INNER JOIN {$viewName} ON {$viewName}.OXID = OXVAR.OXPARENTID " ;
        $sQ .= ($sActiveSnippet = $oListObject->getSqlActiveSnippet()) ?  " WHERE $sActiveSnippet " : " WHERE 1 ";
        $sQ .= (strlen($this->additionalClauses) > 0) ? " AND $this->additionalClauses" : "";
        $sQ .= " AND {$tableName}.OXTIMESTAMP >= '{$lastUpdateDatetime}'";

        return $sQ;
    }

}