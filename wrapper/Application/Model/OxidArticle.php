<?php
/**
 * Created by PhpStorm.
 * User: doellerer
 * Date: 15.08.18
 * Time: 10:29
 */

namespace bfox\multichannel\wrapper\Application\Model;

class OxidArticle extends OxidArticle_parent
{
    /**
     * @return array
     */
    public function getExtendedPictureGallery()
    {
        $pictureGallery = $this->getPictureGallery();
        $normalSizeImages = $pictureGallery["Pics"];

        $masterSizeImages = $this->convertToMasterUrls($normalSizeImages);

        $pictureGallery["Master"] = $masterSizeImages;

        return $pictureGallery;
    }

    /**
     * @param string[] $imageUrls
     * @return string[]
     */
    private function convertToMasterUrls($imageUrls)
    {
        $masterImageUrls = [];

        foreach ($imageUrls as $imageUrl)
        {
            if(strlen($imageUrl) === 0) continue;

            $url = str_replace("generated", "master", $imageUrl);

            $parts = explode("/", $url);

            $indexSize = count($parts) -2;

            if(array_key_exists($indexSize, $parts))
            {
                unset($parts[$indexSize]);

                $masterUrl = implode("/", $parts);

                $masterImageUrls[] = $masterUrl;
            }
        }

        return $masterImageUrls;
    }
}