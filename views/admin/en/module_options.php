<?php
/**
 * @package brickfoxConnect
 * @copyright Copyright (c) 2011-2013 brickfox GmbH (http://www.brickfox.de)
 */
$sLangName = "English";
$iLangNr   = 1;

$aLang = array(
    'charset'                                           => 'ISO-8859-15',

    // GROUPS
    'SHOP_MODULE_GROUP_bfox_multichannel_connection'        => 'Connection to Brickfox client',
    'SHOP_MODULE_GROUP_bfox_multichannel_products'          => 'Products settings',
    'SHOP_MODULE_GROUP_bfox_multichannel_customexport'      => 'Customized Products Export',
    'SHOP_MODULE_GROUP_bfox_multichannel_thirdpartystock'   => 'Third Party Stock',
    'SHOP_MODULE_GROUP_bfox_multichannel_attributes'        => 'Attributes settings',
    'SHOP_MODULE_GROUP_bfox_multichannel_orders'            => 'Orders settings',
    'SHOP_MODULE_GROUP_bfox_multichannel_logging'           => 'General Log',
    'SHOP_MODULE_GROUP_bfox_multichannel_storage'           => 'Storage',
    'SHOP_MODULE_GROUP_bfox_multichannel_scriptLogger'      => 'ScriptLogger',

    // connection
    'SHOP_MODULE_strbrickfoxFtpHost'            	    => 'FTP Host',
    'SHOP_MODULE_strbrickfoxFtpUsername'          	    => 'FTP Username',
    'SHOP_MODULE_strbrickfoxFtpPassword'          	    => 'FTP Password',
    'SHOP_MODULE_intbrickfoxFtpPort'           	        => 'FTP Port',
    'SHOP_MODULE_strbrickfoxFtpFolder'           	    => 'Remote Folder',

    // products
    'SHOP_MODULE_aTaxClassMapping'			 	        => 'Tax Class Mapping',
    'SHOP_MODULE_aArticleAdditionalWhere'			 	=> 'Additional Clauses',
    'HELP_SHOP_MODULE_aArticleAdditionalWhere'          => 'In the format FieldName => full condition',
    'SHOP_MODULE_slctImageType'			 	            => 'Select Image Size for Export<br>',
    'SHOP_MODULE_slctImageType_Icons'			 	    => 'Icon size',
    'SHOP_MODULE_slctImageType_Pics'			 	    => 'Normal size',
    'SHOP_MODULE_slctImageType_ZoomPics'			 	=> 'Zoom pictures (when available)',
    'SHOP_MODULE_slctImageType_Master'			 	=> 'Master image',
    'HELP_SHOP_MODULE_slctImageType'			 	    => 'If Zoom pictures is selected, export will occur depending on availability. <br>
                                                            Example: if Zoom is selected, it will export Zoom Images where available, 
                                                            otherwise export normal ',
    'SHOP_MODULE_strLastProductsUpdate'			 	    => 'Last Products Update (do not change)',

    // custom products export
    'SHOP_MODULE_aGroupsToExport'			 	        => 'Data to Export. <br><br>
                                                             <i>Possible values</i>: Attributes, Categories, Images, Descriptions, Currencies, Options.',
    'SHOP_MODULE_aLastUpdateCheck'			 	        => 'Check last Update in these tables. <br><br>
                                                             <i>Possible values</i>: oxartextends__oxid, oxaccessoire2article__oxarticlenid, <br>
                                                             oxobject2article__oxarticlenid, oxobject2attribute__oxobjectid, <br>
                                                             oxobject2category__oxobjectid, oxobject2seodata__oxobjectid, oxprice2article__oxartid',

    'SHOP_MODULE_strCustomExportLastChange'             => 'Get changes from this date on.',

    // third party stock
    'SHOP_MODULE_blnThirdPartyMap'                      => 'Map a table field to Third Party Stock',
    'SHOP_MODULE_strThirdPartyMapFrom'                  => 'Field name (from oxarticles only).',
    'SHOP_MODULE_slcThirdPartyMapAs'                    => 'Choose the rule for output value',
    'SHOP_MODULE_slcThirdPartyMapAs_blnText'            => 'True or False (string)',
    'SHOP_MODULE_slcThirdPartyMapAs_blnNumber'          => '0 or 1 (bool)',
    'SHOP_MODULE_slcThirdPartyMapAs_fieldValue'         => 'Field Value (string)',

    // attributes
    'SHOP_MODULE_arrAttributesArticlesTable'			=> 'Fields from oxArticles to export as attributes',
    'HELP_SHOP_MODULE_arrAttributesArticlesTable'       => 'Array format, e.g. fieldTableName => outputAttributeName',

    // orders
    'SHOP_MODULE_slcOrderNumberType'                    => 'Options for Import Order Number',
    'SHOP_MODULE_slcOrderNumberType_externId'           => 'Use Extern Order Id (only if is numeric)',
    'SHOP_MODULE_slcOrderNumberType_bfId'               => 'Use Brickfox Order Id',
    'SHOP_MODULE_slcOrderNumberType_oxId'               => 'Oxid default',
    'SHOP_MODULE_sOrderfolderNew'                       => 'Order-Folder Incoming',
    'SHOP_MODULE_sOrderfolderShipped'                   => 'Order-Folder Shipped',
    'SHOP_MODULE_sOrderfolderFba' 	                    => 'Order-Folder FBA',
    'SHOP_MODULE_sCustomerOrderNumberColumnName'        => 'column-name for customer-id (oxorder table)',
    'SHOP_MODULE_sOrderLastRun'                         => 'Date of Last Order Import. Do not change!',
    'SHOP_MODULE_slcOrderStatusUpdateType'              => 'Orders Status type of updates',
    'SHOP_MODULE_slcOrderStatusUpdateType_excludeLines' => 'Do NOT check Folder in OrderLines',
    'SHOP_MODULE_slcOrderStatusUpdateType_includeLines' => 'Check Folder changes in OrderLines',
    'SHOP_MODULE_aaOrdersStatusFolderMapping'           => 'Mapping status from OXFOLDER => bf_status',

    'HELP_SHOP_MODULE_sOrderfolderNew'                  => 'OXFOLDER for new orders',
    'HELP_SHOP_MODULE_sOrderfolderShipped'              => 'OXFOLDER for shipped orders',
    'HELP_SHOP_MODULE_sOrderfolderFba'                  => 'OXFOLDER for orders fulfilled by Amazon (only if FBA is active)',

    // shops
    'SHOP_MODULE_GROUP_shops'				        => 'Shops settings',
    'SHOP_MODULE_aBfShopIdToShopIdMapping'		    => 'Brickfox shop id to Oxid shop id mapping',
    'SHOP_MODULE_blnOverrideShopsLoopDefault'		=> 'Use custom Shop id List to loop',
    'HELP_SHOP_MODULE_blnOverrideShopsLoopDefault'	=> 'Instead the default (list of all active shops in oxid), uses array below.',
    'SHOP_MODULE_aShopsToLoop'		                => 'Custom Exchange Shops',
    'HELP_SHOP_MODULE_aShopsToLoop'		            => 'Use format 1,2,4,6 ',

    // logging
    'SHOP_MODULE_sLogFilename'						    => 'Log filename prefix',
    'SHOP_MODULE_sLogLevel'							    => 'Log level',
    'SHOP_MODULE_sLogCleanupInDays'					    => 'Log cleanup time in days',

    // storage
    'SHOP_MODULE_strStorageDirectory'					    => 'Exchange Directory',
    'SHOP_MODULE_boolStorageShopDirectory'				=> 'Export/Import from specific shop directory (added at the end of Exchange Directory).',
    'SHOP_MODULE_sStorageFilenameOrders'			    => 'Import orders file name',
    'SHOP_MODULE_sStorageFilenameOrdersStatus'		    => 'Export orders status file name',
    'SHOP_MODULE_sStorageFilenameManufacturers'		    => 'Export manufacturers file name',
    'SHOP_MODULE_sStorageFilenameCategories'		    => 'Export categories file name',
    'SHOP_MODULE_sStorageFilenameProducts'			    => 'Export products file name',
    'SHOP_MODULE_sStorageFilenameProductsUpdate'	    => 'Export products update file name',
    'SHOP_MODULE_sStorageFilenameProductsAssign'	    => 'Export products assignments file name',
    'SHOP_MODULE_sStorageFilenameProductsMap'		    => 'Export products id mapping file name',
    'SHOP_MODULE_sStorageFilenameCategoriesMap'		    => 'Export categories id mapping file name',

    // scriptlogger
    'SHOP_MODULE_sScriptLoggerCleanupDBHanging'		    => 'ScriptLogger cleanup time in seconds for hanging processes',
    'SHOP_MODULE_sScriptLoggerCleanupDBRemoval'		    => 'ScriptLogger cleanup time in days for old database entries',

);
